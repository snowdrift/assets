# Economics of Software Freedom

Abstract:

There are four categories of economic goods: private goods, club goods, commons, and public goods. Free software programs are public goods, and they don't fit our market economy which relies on scarcity and exclusion.

This talk will lay out a new way to understand the economic dilemmas with public goods. It will discuss how the typical language and models of economics work against the goals of software freedom. With this foundation, we can start working on the challenge of how to build a new economy based on sharing and abundance.

---

## Intro

Hi everyone! I'm Aaron Wolf. I'm a free software and free culture evangelist.
In my day job, I'm an independent musician and music teacher
That's one of the things that got me interested in the economics of creative work.
I have no formal credentials in economics, but I've studied the issues for years,
particularly in relation to my work on Snowdrift.coop

I could say more about my personal stories,
but I'm cramming in a lot into this talk, so let's just get into it.

You probably already know a good bit about free software basics, funding challenges, and some basic economics.

But my goal here is to give you a better mental model — more helpful memes if you will — for thinking about and talking about these issues.

### Software freedom

So, as you likely know: we say that computer users have software freedom when they are legally and practically allowed to **use**, **study**, **change**, and **share** software however they wish.

And we emphasize "free as in freedom" because it's not about price.
In fact, it doesn't violate software freedom for people to *pay* for a copy of some software.

Also, nobody is required to share free software.
That means **free software is not *necessarily* publicly available**.
It's free software as long as everyone who has access also has all the freedoms.

Now, in *practice*, free software is nearly always public — anyone just needs internet access and an appropriate computer.

## Economic jargon

So, we can think of **Free Software** as a category of **Public Goods**.
Now, free software is *good* for the public rather than bad.
But here we're talking "Goods" like "goods and services".

One thing about standard economic jargon:
Besides being jargonny, it is biased against public goods.
So, today I'm not going to explain the jargon, I'm gonna *fix* it.

### Goods and services

If you look up definitions of "goods" it'll say "tangible" (like physical goods you can touch, though somtimes it's exanded to other senses), and you'll see stuff about ownership.
But here's a simpler summary which covers all we really need:

- "Goods" means anything that provides utility that you can enjoy separately from the time of production (or extraction)
- "Services" are actions that provide utility at the time of their performance

So, software is a good. It's a pattern of computer instructions you can save to computer memory and then use later.

Software *development* is a service. So is sharing a copy.
I am doing a service by giving this talk.
But the recording of it will be a good.

Not all services lead to goods, but most goods are the result of services.

So, there are two ways to think about software economically.
Fund the services, the development time and related work and the support and marketing.
Or treat the development as investment and charge for the goods.

Keep this in mind for later.

## Types of Goods

Look up types of goods in any economics textbook (or website), and you'll see the terms:
- Rivalrousness
- Excludability

Rivalrousness asks, "are different consumers *rivals* where they can't both get the full utility?"

So, if we have an apple, I can eat it, you can eat it, we can split it, but we can't both eat it.
So we're rivals.

Now, we could cut out the seeds, plant them, and maybe eventually both get imperfect "copies" of the apple.
So, rivalry could be seen as situations where copying is really hard and costly.
Because ease of copying is a core question for economics,
computers and the internet are a massive change to our economy.

Let's pause a moment and notice the political and social bias of the jargon.
Describing us as rivals is the opposite of describing us as partners.
It pushes an anti-social view of the world.
And also, not everything we use gets used *up*, so the word "consumer" is a problem.

### Scarcity

Instead of "rivalrousness", better to just talk about **Scarcity**
And for most contexts, the adjective is best, so **scarce**.

What matters economically is whether there's enough of something to go around.
So then we can think about supply and demand.
If demand be higher than supply, we have scarcity.

### Abundance

The opposite of scarcity is **abundance** (which is way better than the standard term: "non-rivalrousness").

Goods are abundant if there's no way demand can outstrip supply.
Economically, it doesn't matter if the abundance is truly infinite or just bountiful.

### Excludability

Excludability asks, "can people be excluded from accessing the goods?"
The problem with this term is: it doesn't matter economically if people *can* be excluded.
The only thing that matters is whether people *are* excluded.
The therm exclud*ability* implies that anything that *can* be locked down and restricted *should* be.

### Exclusive

So, let's just say **exclusive**.

And instead of "non-exclusive" (let alone "non-excludable"), we want a positive alternative.

"Free" isn't quite right, though it's close.
"Shared" is a good option.
Both of those words can describe the use of something *within* an exclusive group.
"Public" might be ideal, but that's already used here. More on that in a moment.

I really think the best term is **open**
(though that implies "closed" instead of exclusive, either way it works).

Of course, we have associations with terms like "shared source" and "open source".
So, we have to be careful using these words.

But this use really works.
"Open" means non-exclusive, unrestricted, available to everyone.

### Standard economics: Scarcity and Exclusion

Now, notice how strongly standard economics **presents the defaults as exclusion and scarcity**.
By defining positive alternatives, we can argue for **openness and abundance** as the default.

## Types of goods

Anyway, now that we have better terms, let's look at the four types of goods:

**[public-goods-grid.svg]**

**Private goods** are scarce and exclusive.
They fit the standard market assumptions.
You want things from the store, you pay for them.
Prices follow supply and demand, yadayada
The real world is not so simple, but I'm not gonna get into that here.
For the free software movement, the main relevant private goods are the computer hardware we use

**Common goods** are scarce but open, and that has problems.
Fish in the sea and trees in the forest can get overharvested.
People pollute our rivers and oceans and overheat the whole planet.
This is the **"tragedy of the commons"** and so on
We can try to police things and appeal to people to be socially responsible,
but each economic actor has a conflict-of-interest between getting value for themselves or leaving a good world for others.
There's no easy answers.
But Common goods aren't that relevant to free software.
The most applicable example would be open resources like software updates in the rare cases where the data is enough to risk hitting bandwidth limits. 

By the way, "commons" is a problem term because people also talk about "digital commons" or "Creative Commons" and that doesn't fit the economics definition of "commons".
I wish we didn't have this confusion, but I don't have a solution in this case.

Anyway, **Club goods** are abundant but exclusive.
Like proprietary software.
Legal or technical obstacles block access and freedoms.
You join the club by accepting the ads, surveillance, other terms and restrictions, and/or paying the membership fees.
These artificial restrictions might violate our intuitions,
and we could go on about all the complaints we have about proprietary software.
But, club goods can certainly be profitable.
Like the Free Software Song says: "hoarders can get piles of money"
Once a club gets large enough to cover its core costs, further growth is nearly pure profit.

But Free Software is **Public goods** — abundant *and* open
And this just has no way to fit in our normal market economy.
Anyone can enjoy the benefits without paying or contributing.
And unlike Common goods, everyone knows that their use of software takes nothing away from anyone else.

## Public Goods dilemmas

*Some* public goods are trivial to produce, so there's no economic problem.
(Except maybe when someone who makes competing club goods does stuff to sabotage the public goods like spreading FUD or worse)

But for public goods that are *costly* to produce and maintain, how do we support that work?

Using public-goods without contributing is called **"free-riding"**.
And it brings up two issues:
The main one is: can we *somehow* get enough support to have nice public goods at all?
But if we can do that, then we can get into asking how to spread the burden fairly.

In most cases, we're stuck with the first problem.
Public goods struggle with nowhere near enough funding.
It's just not clear how to get a critical mass of support. 
We have a **coordination problem**.

## Not all-or-nothing

Now, keep in mind that these categories of goods are on a **continuum**.

Goods can be more or less open as well as being more or less abundant.
We might have freedom to copy and share something but not to adapt it.

We don't call it "free software" unless we have *all* the freedoms,
but economically, we can recognize that it's not all-or-nothing.
One aspect of a product can fit the public-goods definition while another aspect is different.

A physical example of public-goods would be a bicycle path if there's no risk of overcrowding.
It has to be built and maintained, but that has nothing to do with the number of users.
So, it's open and abundant.

But with roads for cars, every vehicle adds congestion, noise, pollution, and wear-and-tear that reduces the value for others.
Road access is slightly restricted with license rules and such.
So, roads are a mostly but not perfectly open and have *some* aspects of scarcity.
They're kind of halfway between commons and public goods. 

## Recognizing features in the world

So, just in general, throughout your day, start asking yourself what sort of goods you see in the world.

Scarce or abundant? Exclusive or open?
You could ask in terms of freedoms even:

- Access
- Use
- Study
- Modify
- Share

Like, Public broadcasting has features of both public goods and club goods.
*Access* to National Public Radio is open and abundant, so it's a public good in that sense.
But the rights to *adapt* and to *publish* are exclusive club-goods.
Individual radio stations pay to get publishing rights.

## FLO

Public goods. by their nature have no access fee. They're gratis, free of charge.
So, we'll never get away from the misunderstanding of "free software" meaning gratis
since that interpretation nearly always holds up. 

Of course, we can clarify the freedom focus by using the term Libre.

But "open" also makes sense, and "open source" is a widespread term used by free software projects.

So the term I use in general is "FLO" which is the acronym for Free/Libre/Open.
I use that to refer to free software, but I also talk about FLO music, FLO art, FLO writings, and so on.

FLO works are public goods but not all public goods are FLO.
Like a public park isn't FLO.

## Funding public goods

Anyway, how can we fund public goods?
They have costs in R&D, maintenance, support, promotion and so on.
Those are all *services*.
So, public goods have to stick to service-based business models instead of a goods-based ones.

Again, goods and services are often intertwined.
We can talk about which features of a project are goods and which are services.

### Services

Free software projects can offer private services.
There's this trend of free software developers complaining about entitled users,
but that's not actually part of software freedom.
There's no conflict in charging for scarce time and energy to support users.
Some free software is funded that way.

Unfortunately, it still rarely competes with the funding levels of proprietary club-goods.
Adding service customers means doing more service.
With club-goods, adding customers can be all profit with near-zero added costs.

#### Side-note: SaaSS

Incidentally, the most effective sort of proprietary software business now is **SaaSS**: Service as a Software Substitute.
    [footnote: SaaS original term is clear here, SaaSS just works to express this approach as a deviation from how things should be]
That's where a business is set up as a service instead of selling goods,
but they don't provide much or any *custom* service or support.
They develop software that could be a Club good,
but they do not sell access to it as a *good* for customers to have and use whenever.
Instead, they keep the software private to themselves
and then charge customers for the *service* of accessing the software over the internet.
That gives the company even more control,
and it forces customers to continue paying forever if they want to keep using the software.

Free software projects may also offer hosting services,
but users are free to manage their own software or hire a competing service provider,
and that limits the price that companies can charge
(though appealing to the value of supporting the free software project does help a lot).

### Monopoly economics

To put this another way, proprietary software can be an economic **monopoly**.
Monopoly means single seller.
It's just a matter of scope how to consider what counts as a monopoly.
Apple has a monopoly on iOS devices, but it shares with Google their *duopoly* on mainstream, well-supported mobile devices.

Proprietary software monopolies are enforced by the "trade secrecy" of source code being unavailable and by the legal terms of copyright and patent which are government-enforced monopolies.

Monopolies don't *have* to extract maximum profits or mistreat customers,
but they have the power to do so.
Customers only have the choice to take-it-or-leave-it.
Government-enforced monopolies can be so profitable that the prospect of getting monopoly status is the primary consideration for VC investors when considering startups.
They don't want to fund just functional businesses, they want to fund monopolies that will capture massive profit.

    [IF TIME: mention stories from my startup thing in Ann Arbor, the question about data mobility as not good but business threat, pointed out that nobody leaves gmail, story about Quikly ("urgency marketing", "black friday every day", mixing social media with brand sponsors with penny auctions), how this was all in like handful of visits to such places]

For more about the dominance of monopolization in our economy overall, check out BIG by Matt Stoller.
Side-note: I wish his work itself were FLO.
My policy about dealing with proprietary resources is that when I recommend or use anything proprietary, I aim to be explicit about it, to recognize the compromise. I think that's the right balance instead of being a purist or ignoring the issues.

Anyway, without monopoly enforcement, competitors can easily cut away at profit margins.
And free software and free software services explicitly forgo such monopoly power.

So, how can we get public goods funded?

### Taxes

The largest funding source for public goods comes from taxes.
That solves the freerider problem.
We simply mandate that citizens pay-in regardless of how much they use the public goods.
Of course, that brings up all sorts of debates about state power, bureaucracy…
I'm not gonna get into that here.
But taxes pay for roads, libraries, parks, scientific research, and some software… 

Governments could potentially fund much more free software.
The obvious starting point is through government and public school use of free software,
and even if they just freeride, that's far better than using (and funding) proprietary software,
but these public institutions could also fund free software development and maintenance.
Beyond that, we *could*, in principle, get more government funding of free software — even the sort that isn't used by government itself.

Incidentally, some people who are most skeptical of taxes care about free-market ideals.
And we should note that FLO opens the maximum potential for competition and cooperation —
for a vibrant free market. Proprietary software terms are inherently anti-competitive.
So, from a free market perspective, FLO is the way to go.
And while free-market supporters might oppose government-backed FLO projects,
they should be opposed to the government enforcement of proprietary software monopolies.

### Donations

Today, much of free software relies on voluntary donations and volunteer time.

Donations are most effective when well-coordinated.
Businesses that use free software often coordinate through business leagues
(like 501(c)(6) in the U.S.) such as the Linux Foundation.
Unfortunately, those are usually businesses that produce proprietary Club-goods for end-users.
The free software they fund is upstream from their main products,
and the freedoms are not passed on.
Still, the free software they support is available for other projects that make free software for end-users.

I don't know of any, but there could be organized efforts to coordinate businesses which simply use free software.
For example, independent retailers could coordinate to fund high quality point-of-sale software.

The reasons such businesses keep using proprietary software are:

- lock-in, having sunk cost in proprietary software
- inadequate features in free software options — which is due to lack funding 

The same sort of coordination problem also happens with the general public.
If we could work together more, we could directly fund free software that serves the public interest.

### Snowdrift.coop

The dilemma is: if I donate all I can while everyone else still freerides,
it will be a huge burden for me but won't make enough difference.

I'm willing to chip in, I just want everyone else to help too. 

So, for nearly a decade, on and off, I've been working (with a team of other volunteers) to develop the concept of **crowdmatching** as a more-effective way to coordinate voluntary support of public goods.

The basic idea is to give *more* as others join the crowd and give with us.
So, a pledge is like "I'll give $1/month to some project I like for every 1,000 other patrons who give with me".

Getting a fully working crowdmatching system is the main focus of **Snowdrift.coop**,
and I wish I could say more about our progress, but that's another talk.
Our progress has been a lot harder and slower than we'd wish.
But you can check it out, and you can contact me later with any questions or if you're interest in helping.
I'd love to hear from you.

The problem is: we face the same funding dilemma that we're trying to fix.

A large enough and coordinated crowd could fund robust public public goods.
So, our work is to help build that coordination.

## Public goods investment

Most start-ups rely on investors to get going,
but there's no basis for public goods ever having profits to give as a return to investors
because there's no reason for the puclic to give projects any more funding than needed.

Some free software gets funded through large private grants,
and that's great as far as it goes, though it's certainly not democratic.

Side-note: businesses with no outside investors are often said to be "bootstrapping".
People have lost connection with the sarcastic meaning of this phrase.
The whole original point was that you *can't* pull yourself up by your own bootstraps —
It's a sarcastic idea used *mock* the myth of the self-made man.

Anyway, it's extremely hard to get going from nothing without economic support.

## Market competition from proprietary software

Today, the main obstacles to practical software freedom are:

- computers running proprietary systems that block the running of free software
    - or at least default to proprietary options
- dominance of proprietary software

Why do people accept proprietary software?

- features (including stability and reliable maintenence)
- network effects (schools, businesses, friends etc)
- advertising
- ignorance about free software options (or of why they should care about software freedom)

We're in this situation because proprietary software companies are well-funded.
They can hire professional teams to make advanced software and market it.

If we only had free software, everyone would accept it even if it were no better and no more funded than it is today.

To promote software freedom, we need the world to choose it over proprietary software.

We can:

- produce superior quality free software
- advertise (spread the word) about free software
- undermine proprietary software
    - block ads for it
    - defund it somehow
    - get proprietary projects to switch to being free
    - discourage developers from working for proprietary companies
    - advocate for the removal of legally-enforced proprietary monopolies
        - the ideal to flip to *enforcing* software freedom could be
            - abolish copyright and patent laws, mandate source release for published works, prohibit DRM

To accomplish any of these things, we need qualified people putting in time and energy.
Somehow, we need that work economically supported — whether directly or indirectly.

### Economic efficiency of public goods

Ironically (given the economic challenges), public goods are far more economical than club goods.

Public goods allow the maximum number of people to enjoy the utility.

FLO products enable creative synthesis.
With FLO, when someone has a new idea, they can contribute to an appropriate existing project
instead of making yet another competing product.
FLO also enables us to combine many sources into a superior synthesis.
We can fix mistakes we find in a Wikipedia article or bugs we find in a program.

In a basic utility sense, FLO projects are maximally economic.
They enable the greatest wealth and the greatest progress.

Free software avoids the economically-degrading harms of proprietary software.
We can be free from walled-gardens and user-hostile anti-features.

## A new open-abundant economy

So, FLO public goods are actually *exceptionally* economically productive and effective.
They just don't get funded well because of the **coordination problems**.

Proprietary club-goods just don't have the same problems getting funding.

We won't solve free software funding by just asking individuals to independently change their choices.

It also isn't just a matter of the structure of businesses.

Worker co-ops avoid the worst patterns of capitalist software companies,
and they are far less likely to abuse their customers,
but if they stick to a club-goods business model with proprietary software behind a paywall,
that doesn't bring us software freedom.

And it's not really about money. Money is just an imperfect tool we use to organize the economy.

To move to a FLO world, we need a paradigm shift in our economic organization as a society.

**We need to move from our current economy based on scarcity and exclusion
to a new economy designed for open abundance.**

Thank you for engaging with me on these important issues.
I look forward to getting questions, suggestions, and further conversations.

You can reach me at aaron@snowdrift.coop and @wolftune@social.coop and chat at Snowdrift rooms on IRC and Matrix.
