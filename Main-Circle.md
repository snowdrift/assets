# Main Snowdrift.coop Circle

This is the top Circle for the whole Snowdrift.coop team, accountable to the
Board of Directors.

## Circle-Wide Governance

### Main Strategies

* Maintain an overall vision and [plan](https://wiki.snowdrift.coop/planning)
* Assure that all roles are filled adequately

### Main Policies

*Note: we're not settled on strict Sociocracy (with some Holacracy) principles
in our governance. Those basic rules will be adapted and clarified here (with
links as appropriate) as we get comfortable with our governance procedures and
policies.*

#### Permission to impact domains

If you want to take an action that impacts the domain of a role you do not hold,
communicate with the appropriate team member in advance. If they do not respond
in 72 hours and there's a timely reason for the impact, you may take the action.
It may be reverted or adjusted later. As practical, check with higher-level
roles when unsure about the impact and unable to reach the impacted role
holders.

#### Asynchronous governance

Although governance changes should be made in real-time meetings, we allow the
processing of tensions and proposals via the team mailing list (planned to move
to community.snowdrift.coop when fully ready). With approval of the General
Manager, such changes may be adopted after 72 hours if there are no valid
objections.

All proposals accepted in Governance meetings must also be aired on the discuss
mailing list to give a chance for others to object who were not at the meeting.
If no valid objection is raised within 72 hours, the change will be accepted.


## Roles

### Human Resources

- Bringing on new partners
- Resolving conflicts and concerns amongst partners
- Looking after Partners' wellness (i.e. not overworked, not feeling useless,..)

### Legal

- Defining and reviewing Bylaws and related legal documents
- Communicating with lawyers and related
- Coordinating with Treasurer on taxes
- Updating, reviewing, handling any necessary legal enforcement

### Security

- Creating guidelines on security issues
- Creating security documentation
- Identifying vulnerabilities and prioritizing security issues
- Avoiding money fraud
- Managing patrons' financial info
- Managing patrons' identity data
- Mitigating unauthorized access to servers
- Securing backups
- Defining access control schemes
- Mitigating unauthorized account usage
  - on Snowdrift.coop, git.s…, contacts.s…, community.s… and any other systems

### Sysadmin

- Domain stuff (registration, certs, etc)
- Web server maintenance
- Mail server maintenance
- Deploying and updating applications
- Keeping IRC bot running

### Treasurer

- Accounting for all income and expenses
- Writing checks / submitting payments as needed for:
    - basic upkeep costs
    - contract work
    - travel
    - promotion
    - etc.
- Updating [public accounting](https://wiki.snowdrift.coop/operations/accounting) regularly
- Filing taxes etc. (in coordination with other officers and board)

#### Treasurer Policies

**Spending**

If the General Manager (with Board approval) provides a budget to any role, that
role may use the budget as they see fit but must report expenses to the
Treasurer.

Otherwise, any partner may apply to spend money by these steps:

1. Check in with Treasurer to ensure that enough money exists.
2. In a tactical meeting, pass a proposal for spending.
3. Have it approved by the Board
    * The rep to the Board will bring the proposal to the Board after step 2.
