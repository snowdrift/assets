# Mechanism Brain Dump
## Shared Definitions
- **Pledge-Goal**: Amount of support a project receives, expressed in patrons or money.
- **Pledge-Amount**: Amount a patron pays when a **Pledge-Goal** is hit.

## Mechanisms
### "Percentage of goal"
1. Projects set a **Pledge-Goal** they aim for.
2. Patrons name a **Pledge-Amount** they are willing to pledge.
3. Each month, **Pledge-Amounts** get paid out in proportion to how much of the **Pledge-Goal** has been reached.
4. For crowds growing beyond the **Pledge-Goal**, **Pledge-Amounts** are paid out at 100% of their value.

### "Percentage of goal - share the burden"
1. Projects set a **Pledge-Goal** they aim for.
2. Patrons name a **Pledge-Amount** they are willing to pledge.
3. Each month, **Pledge-Amounts** get paid out in proportion to how much of the **Pledge-Goal** has been reached.
4. For crowds growing beyond the **Pledge-Goal**, payouts across the crowd are reduced from **Pledge-Amounts** proportionally so that the crowd's total payouts equal the **Pledge-Goal**.

### "Percentage of goal - Opt-in past goal"
1. Projects set a **Pledge-Goal** they aim for.
2. Patrons name a **Pledge-Amount** they are willing to pledge.
3. Each month, **Pledge-Amounts** get paid out in proportion to how much of the **Pledge-Goal** has been reached.
4. For crowds growing beyond the **Pledge-Goal**, patrons are offered an opt-in to give beyond 100% of their **Pledge-Amount**.
    - Option: the opt-in applies toward a new stretch-goal which serves as a new opt-in point.
        - Variant A: opt-in matches only the others who have similarly opted in.
        - Variant B: continued giving is proportional to the degree the crowd has passed the **Pledge-Goal** — including others who do not opt-in to extra giving.

### "Patron Only"
1. Projects may suggest desired **Pledge-Goals**.
2. Projects may limit the range of possible **Pledge-Amounts** (min/max, fixed steps ...)
3. Patrons pledge to pay a chosen **Pledge-Amount** to a project.
4. Patrons set a **Pledge-Goal** (suggested or own) to their pledge:
5. **Pledge-Amounts** only get payed out to the degree that their **Pledge-Goal** was hit.

### "Critical Mass"
- **Basepay**: Patrons select how much to give monthly to **sustain** a project *as-is*, like Patreon.
- **Upgrade Plans**: An upgrade plan is a potential future **status** of the project where it sustains more **recurring** costs, because it has more income. The upgrade plan says exactly what monthly income must be reached, the minimum period a pledger must commit to, and the implementation plan.
- **Pledging**: Patrons review the proposed upgrade plans, and for each they would like to see realized, answer "how much more $/month would I give if this came true today?" and enter this hypothetical paybump.
- **Critical Mass**: When the current income (sum of everyone's basepay) plus the potential new income (sum of the crowd's pledged paybumps), surpasses the plan's goal amount, you've reached critical mass! The pledgers have their basepay *bumped up* by their pledged amount. If the plan had a minimum commitment period, they cannot reduce their basepay below this new amount for that time. They can always increase it. The project now has exactly the right amount of income to implement the plan *immediately*.

### "Stacked Goals"
1. Projects choose desired **Pledge-Goals**.
    - Note: While technically possible to use patrons, this mechanism makes more sense with a money-goal.
2. Patrons pledge to pay a chosen **Pledge-Amount** to a project, at a chosen **Pledge-Goal**.
3. For each **Pledge-Goal**, *starting at the lowest*, Patrons who pledged towards a higher **Pledge-Goal** put part of their **Pledge-Amount** toward this lower goal, in proportion to the size of the goals.
4. **Pledge-Amounts** only get payed out to the degree that their **Pledge-Goal** was hit.
5. For any **Pledge-Goal** that is passed, payouts across the crowd are reduced from **Pledge-Amounts** proportionally so that the crowd's total payouts equal the **Pledge-Goal**.
    - For higher goals, the difference is returned to the total remaining pledge-amount.

#### Example
Calculations at different points in time, starting from nothing.

1. The project sets two goals. A = $100 and B = $400 (aka $300 *additional* on top of A).
    - Note: A is 25% of the way to B.
2. Alex pledges $10 towards A, making it $10/$100=10% reached.
    - Alex — 10% of $10 ⇒ $1.00
3. Sam pledges $80 towards B ⇒ becomes $20 toward A (now $30/$100=30% reached) and $60 *additional* toward B (now $60/$300=20% reached).
    - Alex — 30% of $10 ⇒ $3.33
    - Sam — $6.67 + $12 = $18.67
        - A: 30% of $20 ⇒ $6.67
        - B: 20% of $60 = $12
4. Max pledges $70 toward A so it is 100% reached. Payouts this month are:
    - Max — $70 * 100% ⇒ $70
    - Alex — $10 * 100% ⇒ $10
    - Sam — $20 + $12 = $60
        - A: 100% of $20 ⇒ $20
        - B: 20% of $60 = $12
5. Jesse pledges $25 toward A, so A is at $125/100, or 5/4 of the goal. Therefore, each pledge is reduced to 4/5 (80%). Payouts this month are:
    - Jesse — 80% of $25 ⇒ $20
    - Max — 80% of $70 ⇒ $56
    - Alex — 80% of $10 ⇒ $8
    - Sam — $16 + $13.65 ⇒ $29.65. From:
        - A: 80% of $20 ⇒ $16
        - Updated pledge: $80 - $16 = $64 *additional* toward B (now $64/$300=21.33% reached)
        - B: 21.33% of $64 ⇒ $13.65

Note: Jesse's spread-the-burden at goal A lets Sam (automatically) shift some of the matching up towards a higher goal, while still reducing the burden for everyone (including Sam).

## Reference
These links describe mechanism proposals which are already captured above (therefore, there is no need to re-read these links).

- Patron-only
    - https://community.snowdrift.coop/t/mechanism-proposal-patron-only/1653
    - https://community.snowdrift.coop/t/patron-based-proposal-for-mechanism-1-1-instead-of-based-goals/1614
- Percentage of goal
    - https://community.snowdrift.coop/t/options-for-next-step-for-new-approach-to-crowdmatching/1608/
    - https://gitlab.com/snowdrift/mechanism/-/blob/master/proposal_percentage-of-dollar-goal.md
    - https://gitlab.com/snowdrift/mechanism/-/blob/master/proposal_percentage-of-dollar-goal-lighten-the-load.md
- Critical mass
    - https://community.snowdrift.coop/t/a-critical-mass-mechanism/1652
    - (Rationale) https://community.snowdrift.coop/t/are-patrons-okay-with-half-assed-funding/1696
- Stacked Goals
    - https://community.snowdrift.coop/t/mechanism-proposal-stacked-goals/1664

## See Also
### Not yet summarized mechanisms
#### "Goal-scaling"
- https://community.snowdrift.coop/t/idea-automatically-adjusted-project-goal/1617
- https://community.snowdrift.coop/t/building-consensus-on-crowdmatching-options-for-a-single-goal/1630
- https://community.snowdrift.coop/t/how-can-we-deduce-a-sensible-maximum-pledge/1676

#### "Preshold"
All-or-nothing threshold + share-the-burden + secret pledging

- https://community.snowdrift.coop/t/problems-with-crowdmatching-proposing-an-alternative/474/
- https://community.snowdrift.coop/t/feedback-on-a-proposed-preshold-protocol-an-alternative-to-crowdmatching/560/
- https://idiomdrottning.org/preshold

#### "Many-hands"
This is a form of share-the-burden that starts immediately with no threshold or goal.

> The scaling factor for each donation decrease with headcount N but the scaling factor is kept bigger than 1/N. It’s X/N where X is a number higher than 1. That way the sum will always go up even as each hand’s work is lightening.

- https://community.snowdrift.coop/t/mechanism-re-proposal-preshold/1735/18?u=wolftune

### Feature Suggestions
- https://community.snowdrift.coop/t/crowdmatching-implementations-charging-up-front/1116
- https://community.snowdrift.coop/t/idea-projects-have-multiple-goals/1619

### Terminology Discussions
- https://community.snowdrift.coop/t/a-valid-comparison/1677
- https://community.snowdrift.coop/t/defining-crowdmatching-terms-and-scope/1655
- https://community.snowdrift.coop/t/goal-adjustment-terminology-framing/1633
- https://community.snowdrift.coop/t/confusion-about-crowdmatching-terms-topic/1656
- https://community.snowdrift.coop/t/mechanism-proposal-definitions/1732
- https://community.snowdrift.coop/t/defining-controlled-comparisons/1674
- https://wiki.snowdrift.coop/about/terminology

Possible terms:

- **Pledge-Value**: The amount a patron is set to give at the current state of a crowd

### Framing Discussions
- https://community.snowdrift.coop/t/framing-of-simplest-patron-vs-based-goal/1645
- https://community.snowdrift.coop/t/how-to-frame-situation-when-a-project-has-a-low-number-of-patrons/1301
- https://community.snowdrift.coop/t/potential-re-framing-match-rate-vs-of-goal/1672
- https://community.snowdrift.coop/t/mechanism-quiz-which-one-is-it/1646
- https://community.snowdrift.coop/t/feedback-wanted-re-pledge-suspension-auto-drop-etc/470
- https://community.snowdrift.coop/t/re-framing-mvp-alpha-toward-project-goal-direction/1657
- https://community.snowdrift.coop/t/how-can-we-make-our-solution-not-work-for-exclusive-club-goods/1624
- (Meta) https://community.snowdrift.coop/t/direct-vs-abstract-replies-in-mechanism-discussion/1671

### Decision-making Discussions
- https://community.snowdrift.coop/t/proposal-decision-process-for-new-mechanism/1647
- https://gitlab.com/snowdrift/governance/-/blob/master/meeting-notes/2020/2020-08-28-crowdmatching-update-proposal.md

### Keyword searches
- https://community.snowdrift.coop/search?q=mechanism
- https://community.snowdrift.coop/tag/crowdmatching
