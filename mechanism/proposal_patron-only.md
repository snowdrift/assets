# proposal "Patron-Only"

1. Projects may suggest desired **Pledge-Goals**.
1. Projects may limit the range of possible **Pledge-Amounts** (min/max, fixed steps ...).
1. Patrons pledge to pay a chosen **Pledge-Amount** to a project.
1. Patrons set a **Pledge-Goal** (suggested or own) to their pledge:
1. **Pledge-Amounts** only get payed out to the degree that their **Pledge-Goal** was hit.


---



* ***Pledge-Goal**:
         Amount of support a project receives, expressed in patrons or payout.*
* ***Pledge-Amount**:
         Amount a patron pays when the Pledge-Goal is hit.*
