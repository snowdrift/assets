# Changelog for Snowdrift.coop legal docs

This log describes changes to Snowdrift.coop legal documents

The format is adapted from [Keep a Changelog](http://keepachangelog.com/en/1.0.0/),
and we adhere to [Semantic Versioning](http://semver.org/spec/v2.0.0.html), where:
- PATCH versions clarify wording without changing the meaning.
- MINOR versions add additional rights or protections for users.
- MAJOR versions are everything else.

## 2019-05-17 link updates

Changed git.snowdrift.coop/sd links to gitlab.com/snowdrift (no content changes, no new document version)

## 2018-09-07 Overhaul of Terms

### Terms of Service v0.4

Previously, we had Terms adapted from Wikimedia. We adapted our new terms from the May 25, 2018 version of the GitHub Terms of Service.
Thus, this changelog describes the changes from GitHub Terms to Snowdrift.coop Terms. We also added several new accessory documents, also based on analogous ones from GitHub.

#### Added

- Note that our FLO licenses consitute permission otherwise disallowed under the Terms
- In the short summaries of a couple sections, added non-binding sentences pointing out the parts we only have for legal reasons and hope never to use.
- Volunteer section adapted from [DreamWidth ToS](https://www.dreamwidth.org/legal/tos)
- Added statement prohibiting opening new Account after we terminate an Account
- New section on crowdmatching, pledging, and the rest of the Snowdrift.coop funding system
- New Projects section
- Added Mimi & Eunice comics for political commentary, levity, and to break up the monotony of legalese.

#### Changed

- Various rephrasings, tweaks, and order changes
- Replaced references to GitHub with references to Snowdrift.coop
- Use "payment services" as example of 3rd party terms that may apply (instead of "sign in with GitHub").
- Forbid "bigoted" instead of "discriminatory" content
- Replaced reference to GitHub Bug Bounty program with a section (C4) giving permission for security research (using language from Wikimedia ToU)
- "customers" -> "Users"; "throttling" -> "technical measures" (to reduce excessive bandwidth usage); and clarified that suspending an account is a last resort
- Consolidated licensing into a single section (D), appropriate to our situation and based on previous version of our Terms (0.3).
- "User Generated Content" -> "User Submitted Content"
- Consolidated account cancelation sections into one cancelation policy.
- Removed "Please read this section carefully" sentences and bolded the short versions instead.
- Organizations and legal entities may not create accounts (as currently planned, human will have accounts and can then *represent* organizations/projects)
- Changed "Intellectual Property Notice" to "Copyright Licensing…" and moved the whole section up above the "User-Submitted Content" section

#### Removed

(these reference the original GitHub numbers, so they no longer relate to the updated numbering)
- A7. A part about collaboration on multiple projects across organizations
- B3. Sections permitting and pertaining to "machine accounts"
- B, B3. References to paid accounts
- B4. We do not currently offer 2FA
- B5. Government affiliated references and "business cloud" stuff
- section E — we have no private repositories
- section H — we have no API at this time (but may add terms when we offer API(s)
- section I — we have no open-ended service comparable to GitHub Pages
- Section J — Snowdrift.coop doesn't have any third party applications
- S2. Snowdrift.coop cannot assign or delegate this contract; we don't plan on being bought out.
- 38. " We will resolve disputes in favor of protecting our Users as a whole."


### Legal Requests v0.1

- Added our own simplified legal-requests.md file (based on GitHub's) that the terms link to

### DMCA policies v0.1

The following notes are based on changes from the GitHub DMCA Policy docs:
- overall made more generic (as a guide to DMCA overall) along with some adjustments to mention Snowdrift.coop
- encouraged copyright owners to reach out informally before filing a DMCA takedown notice.
- removed section A6 from the DMCA Policy that was specific to GitHub repos and lack of access control for specific files within repos
- removed section B about repo forks
- removed some of the more-reading links and details about contacting representatives (we overall take a clear stance on free culture etc. and do not need to highlight it further here besides the links we're retaining)
- removed section about anti-circumvention in notice stuff (more about code in GitHub repos)
