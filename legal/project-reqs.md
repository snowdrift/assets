# Snowdrift.coop Project Requirements

At this time, we list Projects at Snowdrift.coop only after a thorough review. Upon receiving an application, We assess a project's fit for Crowdmatching, alignment with our mission and values, and legal status.

To apply for listing, you must:

- verify your legal right (as applicant) to represent the Project
- provide an adequate description of the Project including summaries of history, status, and future goals
- agree to publish all work funded through Snowdrift.coop under free/libre/open (FLO) licensing and publication standards (described in detail below), meeting the economic definition of "public goods"
- agree to use donations strictly to further Project goals
- provide transparent accounting, including reporting of outside income sources
- provide all the information necessary to validate the Project's legal status (which must itself meet the requirements of a third-party payment service that we support)
- expressly represent and warrant that funding of the Project does not contravene any applicable U.S. Laws or Sanctions

## Required licenses and FLO definitions

**Free/Libre/Open (FLO) terms mean unrestricted freedoms to use, modify, and share**. A few precise FLO definitions compete for recognition as the standard in different fields.[^free-v-open] Thankfully, the most accepted definitions are practically equivalent. We do not want to create yet another competing standard, so we defer to the options listed below.[^why-flo]

[^free-v-open]: The different definitions are generally between "freedom"-focus versus "open"-focus, which involves the same terminology debates surrounding the term [Free/Libre/Open](https://wiki.snowdrift.coop/about/free-libre-open).

[^why-flo]: For more on why we focus exclusively on FLO project, see the article [Why Only Free/Libre/Open Projects?](https://wiki.snowdrift.coop/about/why-flo).

- Software projects must meet either of these definitions:
    - [Free Software Definition](https://www.gnu.org/philosophy/free-sw.html)
    - [Open Source Definition](https://opensource.org/osd-annotated)

- Educational, research, journalism, and artistic works (including music, visual art, video, and so on) must meet either of these definitions:
    - [Definition of Free Cultural Works](https://freedomdefined.org/Definition)
    - [Open Definition](https://opendefinition.org/od)

- Online services must meet the [Open Software Service Definition](https://opendefinition.org/ossd)
and should otherwise abide by the [Franklin Street Statement on Freedom and Network Services](https://wiki.p2pfoundation.net/Franklin_Street_Statement_on_Freedom_and_Network_Services).[^online-services]

[^online-services]: Overall, online services should relate in a fundamental way to networking and communication and not merely substitute for products that would be equally effective when run locally and offline. The GNU.org article [*Who does that server really serve?*](https://www.gnu.org/philosophy/who-does-that-server-really-serve.html) is useful for further consideration. That article describes the problems with Service as a Software Substitute (SaaSS).

- Projects producing *non-rivalrous* works around hardware products (such as plans, software, designs, and related resources) must meet either:[^hardware]
    - The FSF's [Respects-Your-Freedom](https://www.fsf.org/resources/hw/endorsement/criteria) hardware criteria
    - The [Open Source Hardware Definition](https://www.oshwa.org/definition/)

[^hardware]: The manufacturing and selling of rivalrous hardware products themselves is fine but is independent of the public-goods focus at Snowdrift.coop. Funding from Crowdmatching here should not be used directly for such hardware manufacturing.

### License options

Each definition above includes connected lists of approved licenses.[^licenses]

[^licenses]: For general perspectives on license choice within the approved lists, see our separate discussion of [FLO license options](https://wiki.snowdrift.coop/about/licenses).

We will also consider projects that use recognized [Copyfree licenses](http://copyfree.org/standard/licenses/) that have not been reviewed by the organizations above (as long as the definitions above are still otherwise met).

## Patent non-aggression

Any project which involves any [patentable subject matter](https://en.wikipedia.org/wiki/Patentable_subject_matter) must agree to never use patent claims to restrict the freedoms otherwise included in the FLO definitions above.[^patents]

[^patents]: We would like to require something like the [Defensive Patent License](https://defensivepatentlicense.org), but it has some burdensome acceptance procedures which sound like there must be a process for each acceptance of a license from an entity. So it does not appear to serve the generalized need we have here. We otherwise encourage joining the [Open Invention Network](https://www.openinventionnetwork.com), but it too is not a complete solution.

Although not required, we also *suggest* that such projects use a license that includes explicit patent grant such as:

- Version 3 (or later) of the [GNU](https://www.gnu.org/licenses/) GPL, AGPL, or LGPL
- The [Copyfree Open Innovation License (COIL)](http://coil.apotheon.org/)
- The [Apache License v2](https://www.apache.org/licenses/LICENSE-2.0.html)
- The [Mozilla Public License](https://www.mozilla.org/MPL/2.0)

## Product release requirements

- Products must entirely meet the definitions above
    - We do not accept projects which release only limited versions of their work under the FLO definitions while producing a separate proprietary version.[^permissive]
    - Although we strongly discourage it, project teams may make fully-distinct, *independent* products with different names that do not meet our requirements. But such products shall not be promoted, funded, or otherwise supported through Snowdrift.coop.
    - A project itself may meet these requirements even if typically used in a proprietary context (such as an otherwise FLO software program that runs on a proprietary operating system).
- Publish in [standard non-proprietary formats](https://wiki.snowdrift.coop/market-research/flo-formats).
- Provide unobstructed public access to anything fully published.
    - Do not force users to do anything extra (such as registering or providing contact information) in order to download the product.
    - For high-bandwidth items, limits on direct access are acceptable *if* alternative access is provided via BitTorrent or other unrestricted mirrors.
- Documentation and support materials must themselves meet the project requirements.

[^permissive]: Of course, permissively-licensed products may lead to proprietary derivatives. We do not exclude projects merely because someone else has made a proprietary derivative. However, the project team itself must not actively promote the use of proprietary derivatives.

## Transparency

- Meet our [transparency requirements](https://wiki.snowdrift.coop/project-reqs/transparency)  (not yet finalized as of 2018-09-25) including reporting status on the items from the [Project Honor Code](https://wiki.snowdrift.coop/project-reqs/honor-projects).
