---
title: Creators' License Affirmation and Statement of Intent
toc: false
categories: legal
...

I/We, the undersigned (“the creator(s)”), do hereby affirm that the creative
work(s) attached to this statement (“the work(s)”) is/are entirely the product
of our own creative endeavors and that no other individual(s) or organization(s)
have any claim to copyright interest in the work(s).

In addition, the creator(s) hereby state my/our intent to license the work(s)
under either the:

* [Creative Commons-Attribution-ShareAlike 4.0 license](https://creativecommons.org/licenses/by-sa/4.0/) (“CC-BY-SA”)
* [Creative Commons-Attribution 4.0 license](http://creativecommons.org/licenses/by/4.0/) “CC-BY”
* [CC0](http://creativecommons.org/publicdomain/zero/1.0/) public domain release.

I/We, the creator(s), affirm my/our intention to make the work(s) available to
all at no cost, to be freely duplicated, shared, and used in the production of
other creative works as per the terms of the selected license.

If using CC-BY, I/we also affirm that these permissions are predicated on
licensees meeting the requirement to attribute my/our work(s) in the manner in
which I/we wish to be identified; and if using CC-BY-SA, the creator(s) further
affirm(s) the requirement for licensees to make any derivative works available
under the same CC-BY-SA license.

If using CC0, the creator(s) affirm that the work is dedicated to the public
domain by waiving all rights to the work worldwide under copyright law.

The creator(s) request that users attribute us in the following manner:





The creator(s) request to be notified of use that infringes on the terms of this
license in the following manner:






I/we, the undersigned, freely make this affirmation and statement in order to
further the goals of a more free, libre, and open world.



