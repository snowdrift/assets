# Snowdrift.coop DMCA Notice Archives

This directory contains the complete archive of any form DMCA takedown notices
or counter notices received by Snowdrift.coop (with appropriate redactions for legal and privacy concerns).
