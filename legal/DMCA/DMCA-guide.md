# DMCA Guide

*Version 0.1*

Welcome to Snowdrift.coop's (adapted from [GitHub's](https://help.github.com/articles/dmca-takedown-policy/)) Guide to the Digital Millennium Copyright Act (commonly known as the "DMCA") — specifically the processes around alleged copyright infringement within user-generated content at online platforms. This guide does not cover other aspects of the statute.

As with all legal matters, it is always best to consult with a professional about your specific questions or situation. We strongly encourage you to do so before taking any action that might impact your rights. This guide isn't legal advice and shouldn't be taken as such.

### DMCA safe-harbor protections

The DMCA includes [copyright liability safe harbor](https://www.copyright.gov/title17/92chap5.html#512) for internet service providers hosting user-generated content — so long as a service provider follows the DMCA's notice-and-takedown rules for any alleged copyright infringements.

### DMCA Notices In a Nutshell

The DMCA provides two procedures for handling alleged copyright infringements by users of online services:

- DMCA [takedown notices](https://gitlab.com/snowdrift/legal/blob/master/DMCA/DMCA-takedowns.md) are used by copyright owners to ask a service to take down content they believe to be infringing.

    Because U.S. copyright law requires no registration of copyrights, you create copyrighted content every time you produce any copyrightable work — even simple works like emails or plain text replies on a forum. If someone else is using your copyrighted content in an unauthorized manner, you can send a host service a DMCA takedown notice to request that the infringing content be changed or removed.

- DMCA [counter notices](https://gitlab.com/snowdrift/legal/blob/master/DMCA/DMCA-counter-notices.md) can be used to correct mistakes or misidentification.

    Maybe the person sending the takedown notice does not hold the copyright or did not realize that you have a license or made some other mistake in their takedown notice. Since a host usually cannot know if there has been a mistake, the DMCA counter notice allows you to let us know and ask that we put the content back up.

The DMCA notice and takedown process should be used only for complaints about copyright infringement and not for unrelated concerns such as trademark infringement, libel, or other complaints. Notices sent through our DMCA process must identify copyrighted work or works that are allegedly being infringed.

### A. Process overview

When a copyright owner sees a case of infringement, they can send the host service a takedown notice. If it's written correctly, the service passes the notice along to the appropriate user. To dispute the complaint, the user can send the service a counter-notice.

The service exercises little discretion in this process other than determining whether the notices meet the minimum requirements of the DMCA. It is up to the parties (and their lawyers) to evaluate the merit of their claims, bearing in mind that notices must be made under penalty of perjury.

Here are the steps in more detail:

1. **Copyright Owner Investigates.** A copyright owner should always conduct an initial investigation to confirm both (a) that they own the copyright to an original work and (b) that the content on Snowdrift.coop is unauthorized and infringing. This includes confirming that the use is not protected as [fair use](https://www.lumendatabase.org/topics/22). A particular use may be fair if it only uses a small amount of copyrighted content, uses that content in a transformative way, uses it for educational purposes, or some combination of the above. Each use case is different and must be considered separately.

    **Example:** An author finds some of their writings in a forum post on Snowdrift.coop. The use would be fully-licensed if the original work was published under CC BY-SA (or compatible) terms and those terms followed (including appropriate attribution and so on). If the use is only a small quotation in a transformative context, it would be fair use. But an unlicensed use of a substantial portion of a copyrighted work could justify sending in a take-down notice.

2. **Copyright Owner Sends A Notice.** After conducting an investigation, a copyright owner prepares and sends a takedown notice to the service. Assuming the takedown notice is sufficiently detailed according to the statutory requirements (as explained in our [DMCA takedown-notice guide](https://gitlab.com/snowdrift/legal/blob/master/DMCA/DMCA-takedowns.md)), we will [post the notice](#d-transparency) to our [public repository](https://gitlab.com/snowdrift/legal/tree/master/DMCA/notice-archives/) and pass the link along to the appropriate user.

3. **Service Asks User to Make Changes.** We will contact the user who posted the alleged infringing content and give them approximately 24 hours to delete or modify the content specified in the notice. We'll notify the copyright owner if and when we give the user a chance to make changes.

4. **User Notifies Service of Changes.** If the user chooses to make the specified changes, they *must* tell us so within the approximately 24-hour window. If they don't, we will act to hide the allegedly infringing content. If the user notifies us that they made changes, we will verify that the changes have been made and then notify the copyright owner.

5. **Copyright Owner Revises or Retracts the Notice.** If the user makes changes, the copyright owner must review them and renew or revise their takedown notice if the changes are insufficient. We will not take any further action unless the copyright owner contacts us to either renew the original takedown notice or submit a revised one. If the copyright owner is satisfied with the changes, they may either submit a formal retraction or else do nothing. We will interpret silence longer than two weeks as an implied retraction of the takedown notice.

6. **User May Send A Counter Notice.** We encourage users who have had content disabled to consult with a lawyer about their options. If a user believes that their use was legal (for whatever reasons, ranging from mistakes in the takedown notice to misindentification of the work to cases of fair use), they may send us a *counter notice*. As with the original takedown notice, we will make sure that the counter notice is sufficiently detailed (as explained in our [DMCA counter-notice guide](https://gitlab.com/snowdrift/legal/blob/master/DMCA/DMCA-counter-notices.md)). If it is, we will [post it](#d-transparency) to our [public repository](https://gitlab.com/snowdrift/legal/tree/master/DMCA/notice-archives/) and pass the notice back to the copyright owner by sending them the link.

7. **Copyright Owner May File a Legal Action.** If a copyright owner wishes to keep the content disabled after receiving a counter notice, they will need to initiate a legal action (i.e. a lawsuit) seeking a court order to restrain the user from engaging in infringing activity relating to the content on Snowdrift.coop. If the copyright owner does *not* give Snowdrift.coop notice within 10-14 days, by sending a copy of a valid legal complaint filed in a court of competent jurisdiction, we will reenable the disabled content.

### C. What If I Inadvertently Missed the Window to Make Changes?

We recognize that there are many valid reasons that you may not be able to make changes within the approximate 24-hour window we provide before your posted content gets hidden. If you respond to let us know that you would have liked to make the changes, but somehow missed the first opportunity, we will re-enable the post one additional time for approximately 24 hours to allow you to make the changes.

### D. Transparency

When we receive any DMCA-related legal notices (including original notices, counter notices, or retractions), we post redacted copies at <https://gitlab.com/snowdrift/legal/tree/master/DMCA/notice-archives/>. We will not publicly publish your personal contact information; we will remove personal information (except for usernames in URLs) before publishing notices. We will not, however, redact any other information from your notice unless you specifically ask us to. When we remove content, we will post a link to the related notice in its place.

Please also note that, although we will not publicly publish unredacted notices, we may provide a complete unredacted copy of any notices we receive directly to any party whose rights would be affected by it.

### E. Repeated Infringement

US law requires us to disable the accounts of repeat copyright infringers. What constitutes a repeat offender is not defined by law.
We currently define a repeat offender as anyone who has received five valid notifications of copyright infringement.
We will not count as such any cases where the alleged user has (A) filed a counter-notification; or (B) indicated that they do not wish to file a counter-notification but that they do not accept the allegation of copyright infringement.
We reserve the right to alter this definition in the future, at our sole discretion.
We also reserve the right to terminate the accounts of those who, in our opinion, misuse or abuse the DMCA notification process against other users.

### F. Submitting Notices

If you are ready to submit a notice or a counter notice:
- [How to Submit a DMCA Takedown Notice](https://gitlab.com/snowdrift/legal/blob/master/DMCA/DMCA-takedowns.md)
- [How to Submit a DMCA Counter Notice](https://gitlab.com/snowdrift/legal/blob/master/DMCA/DMCA-counter-notices.md)

### Learn More and Speak Up

The DMCA is a controversial law that many argue is unjust and harmful in specific ways. Here are a few links to scholarly articles and blog posts we have found with some opinions and proposals for reform:

- [Unintended Consequences: Twelve Years Under the DMCA](https://www.eff.org/wp/unintended-consequences-under-dmca) (Electronic Frontier Foundation)
- [Statutory Damages in Copyright Law: A Remedy in Need of Reform](https://papers.ssrn.com/sol3/papers.cfm?abstract_id=1375604) (William & Mary Law Review)
- [If We're Going to Change DMCA's 'Notice & Takedown,' Let's Focus on How Widely It's Abused](https://www.techdirt.com/articles/20140314/11350426579/if-were-going-to-change-dmcas-notice-takedown-lets-focus-how-widely-its-abused.shtml) (TechDirt)
- [Fair Use Doctrine and the Digital Millennium Copyright Act: Does Fair Use Exist on the Internet Under the DMCA?](https://digitalcommons.law.scu.edu/lawreview/vol42/iss1/6/) (Santa Clara Law Review)
