# Guide to Submitting a DMCA Takedown Notice

*Version 0.1*

This guide describes the information that Snowdrift.coop needs in order to process a DMCA takedown request. It is consistent with the form suggested by the DMCA statute (which can be found at the [U.S. Copyright Office website](https://www.copyright.gov). If you have more general questions about what the DMCA is or how Snowdrift.coop processes DMCA takedown requests, please review our general [DMCA Guide](https://gitlab.com/snowdrift/legal/blob/master/DMCA/DMCA-guide.md).

As with all legal matters, it is always best to consult with a professional about your specific questions or situation. We strongly encourage you to do so before taking any action that might impact your rights. This guide isn't legal advice and shouldn't be taken as such.

### Before You Start

***Tell the Truth.*** The DMCA requires that you swear to the facts in your copyright complaint *under penalty of perjury*. It is a federal crime to intentionally lie in a sworn declaration. (*See* [U.S. Code, Title 18, Section 1621](https://www.gpo.gov/fdsys/pkg/USCODE-2011-title18/html/USCODE-2011-title18-partI-chap79-sec1621.htm).) Submitting false information could also result in civil liability — that is, you could get sued for money damages. The DMCA itself [provides for damages](https://en.wikipedia.org/wiki/Online_Copyright_Infringement_Liability_Limitation_Act#§_512(f)_Misrepresentations) against any person who knowingly materially misrepresents that material or activity is infringing.

***Investigate.*** Filing a DMCA complaint is a serious legal allegation. We ask that you conduct a thorough investigation and consult with an attorney before submitting a takedown to make sure that the use isn't actually permissible.

***Ask Nicely First.*** Though not strictly required, a great first step before sending us a takedown notice is to try contacting the user directly. One way to get in touch: send them a private message at the [Snowdrift.coop community forum](https://community.snowdrift.coop).

***Send In The Correct Request.*** We can only accept DMCA takedown notices for works that are protected by copyright, and that identify a specific copyrightable work. We have other measures to address other concerns such as those related to conduct, security, trademarks, and so on.

***Consider Fair Use.*** A particular use may be [fair-use](https://www.lumendatabase.org/topics/22) if it only uses a small amount of copyrighted content, uses that content in a transformative way, uses it for educational purposes, or some combination of the above. Each use case is different and must be considered separately, so it is important to speak to a legal professional about your proposed complaint.

***No Bots.*** You should have a trained professional evaluate the facts of every takedown notice you send. If you are outsourcing your efforts to a third party, make sure you know how they operate, and make sure they are not using automated bots to submit complaints in bulk. These complaints are often invalid and processing them results in needlessly taking down projects!

***Matters of Copyright Are Hard.*** It can be very difficult to determine whether or not a particular work is protected by copyright. For example, facts (including data) are generally not copyrightable. Words and short phrases are generally not copyrightable. URLs and domain names are generally not copyrightable. Since you can only use the DMCA process to target content that is protected by copyright, you should speak with a lawyer if you have questions about whether or not your content is protectable. 

***You May Receive a Counter Notice.*** Any user affected by your takedown notice may decide to submit a [counter notice](https://gitlab.com/snowdrift/legal/blob/master/DMCA/DMCA-counter-notices.md). If they do, we will re-enable their content within 10-14 days unless you notify us that you have initiated a legal action seeking to restrain the user from engaging in infringing activity relating to the content on Snowdrift.coop.

***Your Complaint Will Be Published.*** As noted in our [DMCA Guide](https://gitlab.com/snowdrift/legal/blob/master/DMCA/DMCA-guide.md#d-transparency), after redacting personal information, we publish all complete and actionable takedown notices at our [public repository](https://gitlab.com/snowdrift/legal/tree/master/DMCA/notice-archives/).

***Snowdrift.coop Isn't The Judge.***
Snowdrift.coop exercises little discretion in the process other than determining whether the notices meet the minimum requirements of the DMCA. It is up to the parties (and their lawyers) to evaluate the merit of their claims, bearing in mind that notices must be made under penalty of perjury.

### Your Complaint Must ...

1. **Include the following statement: "I have read and understand Snowdrift.coop's Guide to Filing a DMCA Notice."** We won't refuse to process an otherwise complete complaint if you don't include this statement. But we'll know that you haven't read these guidelines and may ask you to go back and do so.

2. **Identify the copyrighted work you believe has been infringed.** This information is important because it helps the affected user evaluate your claim and give them the ability to compare your work to theirs. The specificity of your identification will depend on the nature of the work you believe has been infringed. If you have published your work, you might be able to just link back to a web page where it lives. If it is proprietary and not published, you might describe it and explain that it is proprietary. If you have registered it with the Copyright Office, you should include the registration number. If you are alleging that the hosted content is a direct, literal copy of your work, you can also just explain that fact.

3. **Identify the material that you allege is infringing the copyrighted work listed in item #2, above.** It is important to be as specific as possible in your identification. This identification needs to be reasonably sufficient to permit Snowdrift.coop to locate the material. At a minimum, this means that you should include the URL to the material allegedly infringing your copyright. If you allege that less than the whole page infringes, specify exactly where you are alleging infringement. If you allege that all of the content at a URL infringes, please be explicit about that as well. If you have investigated and analyzed that the allegedly infringing content has duplicated in secondary locations (such as within quotes in context of replies in a discussion topic) that you believe also count as infringement, please explicitly identify each allegedly infringing location. Please also confirm that you have investigated each individual case and that your sworn statements apply to all the cases you have identified.

4. **Explain what the affected user would need to do in order to remedy the infringement.** Again, specificity is important. When we pass your complaint along to the user, this will tell them what they need to do in order to avoid having the rest of their content disabled. Does the user just need to add a statement of attribution? Do they need to delete certain lines within their code, or entire files? Of course, we understand that in some cases, all of a user's content may be alleged to infringe and there's nothing they could do short of deleting it all. If that's the case, please make that clear as well.

5. **Provide your contact information.** Include your email address, name, telephone number and physical address.

6. **Provide contact information, if you know it, for the alleged infringer.** Usually this will be satisfied by providing the Snowdrift.coop username associated with the allegedly infringing content. But there may be cases where you have additional knowledge about the alleged infringer. If so, please share that information with us.

7. **Include the following statement: "I have a good faith belief that use of the copyrighted materials described above on the infringing web pages is not authorized by the copyright owner, or its agent, or the law. I have taken fair use into consideration."**

8. **Also include the following statement: "I swear, under penalty of perjury, that the information in this notification is accurate and that I am the copyright owner, or am authorized to act on behalf of the owner, of an exclusive right that is allegedly infringed."**

9. **Include the signature of the copyright holder or the copyright holder's designated agent.** Signatures may be a physical signature or a digital signature in a recognized industry-standard format such as PGP. Unsigned notifications will not be processed.

### How to Submit Your Complaint

Send an email notification to <copyright@snowdrift.coop>. You may include an attachment if you like, but please also include a plain-text version of your letter in the body of your message.

If you must send your notice by physical mail, you can do that too, but it will take *substantially* longer for us to receive and respond to it. Notices we receive via plain-text email have a much faster turnaround than PDF attachments or physical mail. If you still wish to mail us your notice, our physical address is:

```
Snowdrift.coop
1021 Nottingham Rd.
Grosse Pointe Park, MI 48230
```
