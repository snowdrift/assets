---
title: Board of Directors
categories: legal
...

#Board of Directors: Role and Resources

This is a page-in-progress for information for board members and others curious
about the role, purpose, and process of the governing Board of Snowdrift.coop.

## Status of the Board

Our initial [Articles of Incorporation](articles.md) have been approved but
almost certainly need amending once we've finalized our overall structure and
Bylaws. However, that work will be assisted through the first incarnation of our
Board of Directors. The initial Board will handle that and the adoption of the
first official Bylaws (we have a [Bylaws draft](bylaws) but it needs to be
redone by our lawyer). The Bylaws will then need to be ratified at the first
general membership meeting, at which time the first elected Board will be
formed. The basic structure, duties, and functioning of the Board will be laid
out in the Bylaws.

## What the Board does

### Expectations, duties, and responsibilities of Board members

#### Ethical obligations

**Avoidance of conflicts of interest:** Directors are expected to disclose any
real, potential, or perceived conflict of interest (CoI) to the rest of the
Board as soon as they become aware of them. A CoI is not necessarily a breach of
trust or an offense against the cooperative but willful failure to disclose a
CoI is.

**Proper use of confidential information:** Information subject to
confidentiality or privacy concerns is on a need-to-know basis only. Beyond
need-to-know, a director may not disclose private or confidential information
obtained because of their position as director until the time appointed by the
board for the information to be made available to all members. Directors are
also prohibited from using confidential information for business or other
advantage, or providing that information to others for advantage.

## Electing the Board

The initial Board will be appointed by the founders.

The Bylaws will describe the election of later Boards, but generally the Board
is expected to have two types of positions: elected, voting Representatives and
non-voting Officers appointed by the Board. If we go with multi-stakeholder
approach, then representatives will be further divided into At-Large
Representatives elected by the general membership and Member Class
Representatives elected by and from each of the three member classes. Officers
include the President (the only Officer elected by the general membership),
Treasurer, Secretary, and one or more Vice Presidents.

## Induction process

Newly elected Representatives and Presidents and newly appointed Officers are
inducted at the annual meeting.

## Training for new (and continuing) Directors

* How to handle cashflow:
    * Ensure that all moneys owed are paid on a timely basis to avoid service
      charges, interest penalties, and or delinquent notices. Where applicable,
      bills are to be paid, if cash flow and credit limits allow, in time to
      receive any discounts for early payment.

## Resources on servant leadership

## Meeting process and resources for reaching consensus

The default decision-making process of the Board as specified in the Bylaws is
consensus. By consensus, the Board may adopt Standing Rules that allow for other
decision-making processes in specific circumstances.

The Bylaws should also provide for a fallback voting process for use in cases of
irreconciliable deadlock or emergency issues that must be resolved immediately.

## Records

For legal compliance and internal transparency, minutes for all Board meetings
and for meetings of all Board-chartered committees are official corporate
records and must be submitted to the Secretary and posted on the website except
where prohibited by privacy and/or confidentiality concerns.

* Minutes of Board meetings must include *at minimum*:
    * Date, place, and time of meeting.
    * A record of the people who attended the meeting and any directors who were absent.
    * A brief statement of all matters pertaining to the business of the cooperative during the meeting.
    * All defined problems, resolutions, and votes by the board.
    * If a roll call vote is taken each individual response shall be recorded.
    * Highlights of the manager’s report, such as volume of sales, expenses, and earnings.
    * Signature of the board secretary and president.

## Board Code of Conduct

As a co-op director, I pledge to do my best for the co-op and will:

* Devote the time needed to fulfill the responsibilities of the position.
* Attend all regular and special board and committee meetings.
* Be prompt, attentive, and prepared for all board and committee meetings.
* Contribute to and encourage open, respectful, and thorough discussions by the board.
* To enhance board understanding and cohesiveness, attend and actively participate in the board’s
training sessions and educational programs.
* Disclose any personal or organizational conflict of interest in which I may be involved, and refrain
from discussing or voting on any issues related to that conflict.
* Refrain from becoming financially involved or associated with any business or agency that has
interests that are, or could be perceived to be, in conflict with the co-op’s interests.
* Be honest, helpful, diligent, and respectful in my dealings with the co-op, other directors, and the
co-op’s management, staff, and members.
* Work for continued and increased effectiveness in the co-op’s ability to serve its member-owners.
* Be a team player and agree to abide by actions of the board, even when I do not fully agree with each decision.
* Present the agreed-upon view of the board of directors, rather than my own, when I speak on
behalf of the co-op to employees, members, and the general public.
* Refrain from asking for special privileges as a board member.
* Work to ensure that the co-op is controlled in a democratic fashion and that all elections are
public, fair, and open to the participation of all members.
* Strive at all times to keep members informed of the co-op’s status and plans and of the board’s
work.
* Continually seek opportunities to learn more about the co-op and its operations and about my
responsibilities as a board member.

As a co-op director, I agree to abide by this Statement of Agreement in both letter and spirit.  
Signature: 
Date:
