# Crowdmatching transaction options

This page discusses issues about accounts and transactions. All of this is
tentative until we begin full operations.

In managing our pledge system, we have these main concerns:

* We want to allow very small pledges from many patrons to aggregate
* Unless we can avoid processing fees entirely, we must consolidate small
  donations into larger single charges.
    * We want to consolidate charges meant for multiple projects as well as
      charges across multiple crowdmatch events
* We want pledge reliability (backed by reliable sources of funds).
* We need a mechanism for patrons control some hard limit to their overall donations.

Some general principles:

* All third-party transaction fees should transparent and *added* to the patron charge rather than deducted from project receipts
* When a patron's total pledges become greater than available funds, their
  pledges will be adjusted until all active pledges fit the budget.
* Users can change their pledges at any time.

Using payment service(s):

* Some third-party payment processors (like Stripe) are specifically designed to handle marketplaces .
    * They register all our users, they charge patrons and pay projects, we never touch any of the money.
    * Our system specifies what the transactions directly between patrons and projects should be.
    * On a monthly basis, we tell the processor to make charges and payments of specific amounts
    * **Best option**: The payment service works with us to consolidate all charges and payments.
        * The total monthly charge for each patron is made as a single charge
        * The total monthly payment to each project is made as a single payment
        * The funding sources are pre-authorized so they are reliable
        * Thus, the total charges and total payouts are equal, and all handled by the payment processor
        * We believe Stripe can do this sort of consolidated charge option,
          although there may be *some* limitations such as the level of
          international support
    * Fallback option: We consolidate charges ourselves by designing our own
      calculation system to then pass to a payment service. I.e. multiple
      patrons donating to multiple projects are treated as though fewer patrons
      donate larger amounts to fewer projects, all adding up to the same total
      charges and payments.
    * **Alternative low/no-fee services:** To whatever extent we can use systems
      that have no fees, we can be more flexible, both in terms of charging
      sooner (lower threshold for arrears) and in terms of multiple charges per
      patron to go to many projects.

## Option A: "Wallet"

In general, the "wallet" approach means a sum of funds is available for each
patron, that gets used over time, and they must increase the available funds to
continue.

One approach without holding funds: get an *authorization* from patrons allowing
us to charge over time any amounts up to $X. Once that amount is approached, the
patron will have to authorize an additional amount in order to continue. We
*could* add to that an automatic reauthorization.

Effectively, patrons would get to say, "charge me whatever crowdmatching
calculates up until I've spent $X total over time, then check with me."

### Gift Cards?

We would like to offer gift cards as funds set aside for donation to
projects. In some respects, this is like giving donations in someone's
name, although it would give that someone the chance to determine something
about the direction the funds go.

How could we do gift cards without holding funds? We could have one charge
account do a "wallet" style authorization on behalf of another patron
potentially.

Open Collective started some form of gift-card option recently, and we could
check how they are handling that.

## Option B: Monthly recurring budget

Patrons authorize us to charge up to a certain amount each month which recurs
but does not roll-over. If at any time their pledges approach their limit, they
must adjust pledges or increase the budget limit.

**As of November 2018, all our UI/UX and public descriptions assume this option
only.**

## Background research etc.

### Why we will only charge in arrears

The optimal method for a "wallet" is to hold funds (somewhat like escrow).
Patrons deposit funds with us and we draw from the funds and transfer to
projects. But that has too many legal liabilities, so we don't plan to consider
that at this time.

If we accepted the liabilities and limitations, we *could* potentially take
actual funds instead of just authorization. But we would have to consider all
deposits as donations directly to Snowdrift.coop, not others' funds held in
escrow.  We would then describe the pledging mechanism merely as a vote on how
we then use the funds.** In order to avoid tax burden on held funds at annual
roll-over, we must either retain 501 tax exemption or determine a legally-sound
accounting method in which all held funds are are already allotted to go out as
expenses and thus not taxed as net income.

The primary values of actually holding funds would be reduced fees and
guarantees. We would know for sure that funds were available (no charges that
fail later) and pay only the one-time initial processing fee for deposits.

But overall, the limitations and liabilities far outweigh the benefits when
considering actually holding funds. Other platforms that have been set up to
hold funds have run into legal problems.

Whatever we do, we need to be sure we are not classified as a financial
institution or payment processor / money transmitter. The one thing we *cannot*
do is touch funds ourselves but claim that the funds are still the property of
the donors until given to projects. Either the funds are ours (which means we
address taxes on holdings either via 501 or via accounting that somehow
recognizes the funds as allotted and thus not subject to taxes at annual
roll-over) or we don't touch them, we just instruct third-parties about how to
process charges and payments.

## Legal liability concerns

* Probably this issue would only even arise if we held funds, but even still in
  a [2008 ruling](http://www.fincen.gov/statutes_regs/guidance/html/fin-2008-r011.html),
  the Bank Secrecy Act was determined not to apply to websites like Kiva that
  involve credit stored in user accounts which can be transmitted at any time to
  international microloan companies, so long as the website remains purely a
  clearinghouse for coordinating transactions.
    * Any transactions within the system have to be clearly tied to our mission
* The primary value a [payment-processor](payment-services) offers us is
  that we do not have to implement PCI compliance on our own infrastructure
  or have our corporate checking set up as a merchant account (as well as
  helping us with fraud detection).

## Who receives project funds?

We need to be sure that everything is legally clear with the recipients of funds
and their legal status. It will be easier with established legal entities.
Individuals are another question.

We need to confirm both with appropriate counsel (attorney, accountant) on our
side and via whatever payment services: what requirements are there for us to
accept a project to meet all compliance concerns?

We could potentially support the step of projects paying team members similarly
(or even in cooperation with) Open Collective, but that's not our primary
service focus.

## Misc notes and plans

The following is mostly older notes about considering the higher-liability
escrow options which we have no plans for now, but they may still be relevant in
some ways, particularly the note about Aaron Williamson.

* Aaron Williamson worked as the attorney for Gratipay when they dealt with the fallout from holding funds in escrow (which brought out concerns about running an illegal money transfer service (which came to light when their payment service, Balanced Payments, shut down and they investigated alternative options).
    * Aaron Wolf (Snowdrift.coop co-founder) worked with Aaron Williamson a
      little as part of 501 working group volunteering for OSI
    * Aaron Williamson helped Gratipay restructure toward charging in arrears
      and doing more adequate vetting of projects. He thus has experience and
      would be an optimal attorney for us for these purposes. He also has
      experience with FLO specifically.
    * Gratipay has since shut down and the fork, Liberapay, ran into similar problems
* BountySource uses a [version of escrow](https://github.com/bountysource/frontend/wiki/Salt-Frequently-Asked-Questions)
  for their "salt" subscription-donation service.  "There is a minimum billing
  charge of $5. If your contribution amount is less than $5, you will be charged
  $5 and the remaining portion will be applied as a credit to your account."
  Thus they are doing some level of "credit in your account". We don't know how
  legally solid that is.
*  This old [blog post at WePay](http://web.archive.org/web/20131221040800/http://blog.wepay.com/post/70311081952/marketplaces-and-money-transmitter-licenses)
  describes various implementations of money transmitter and online marketplace
  issues. Incidentally, WePay says they are exempt from Money Transmitter
  licensing, but we're not clear why (perhaps because they make recipients
  register in a particular manner and they then only process credit cards and do
  immediate transfers without any real time holding funds).  A complex
  state-based issue in California has people debating whether AirBnB is a money
  transmitter simply because they escrow funds for a short time before paying
  the renters of space.
    * In an [article about the debate](http://www.sfgate.com/technology/businessinsider/article/California-Is-Rethinking-The-Financial-4346126.php),
      it says "If a company entrusts a regulated bank with the funds it's moving,
      and the bank maintains control of the funds, the department typically won't
      classify it as a money transmitter." A [Business Insider
      article](http://www.businessinsider.com/california-money-transmitter-act-startups-2012-7)
      is relevant as well.  **AirBnB is now a licensed Money Transmitter**. It was
      debated before, but now they show up in the list of [licensed money
      transmitters](http://www.dbo.ca.gov/Licensees/money_transmitters/money_transmitters_directory.asp)
      for California. They do take in funds which they hold, then they pay others
      later. That's similar enough to what we propose. We should make sure to avoid
      this status ourselves either through a partner service or through classifying
      our transmissions differently (not considering the funds coming in as simply
      being the patrons' funds that we transmit).  A [Lawsuit against many parties
      for non-licensing of money transmitter
      status](http://thenextweb.com/insider/2013/05/07/zuckerberg-nemesis-aaron-greenspan-sues-facebook-again-plus-the-rest-of-silicon-valley/)
      is relevant and important but may be hyperbolic and have exaggerated claims.
      This is from someone frustrated with the law's burdens ruining his own startup
      trying to attack others who he sees as getting away with operating illegally.
      He certainly has at least *some* merit to his claims.
