---
title: Legal Description of the Snowdrift.coop System
categories: legal
...

Snowdrift.coop supports a wide variety of projects that produce non-rival public
goods freely available to anyone in the world under appropriate licenses. We
facilitate financial donations from the public to these projects and offer tools
for organizing and other needs. We also work to orient the projects toward
acting in ways that best serve the public interest.

Our key feature is "crowdmatching": a fundraising approach where each patron
donates a tiny base amount times the number of other patrons (everyone matches
each other in donating).

On a designated day each month, a crowdmatch-event takes place where the number
of active patrons determines the donation level to each project.

## Snowdrift.coop as allocation calculator

Snowdrift.coop's function is to determine allocation of funds from patrons to
their selected projects in a reliable and socially-reinforced fashion.
Snowdrift.coop is funded itself through donations specified to us (as one of the
projects), and we intend to charge no fees (but we will pass on any processing
fees.

## Types of projects

We intend to work with a range of projects with differing legal status. For a
501(c)(3) project, we'd like donors to benefit from tax-deductibility regardless
of using our service in facilitating their donation. We also want the ability to
support projects that do not have non-profit status, even though *all* the
donations we facilitate will be required to go toward serving our mission of
furthering the public commons. In other words, a for-profit entity may receive
donations through our system if they use those funds specifically to build the
non-rival public goods that we support.

## International concerns

We intend to operate internationally, both in supporting projects and accepting
donations. We want to operate internationally as soon as possible, but this
should not delay initial domestic operations.
