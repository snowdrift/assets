<!--
Previous week's meeting notes: https://wiki.snowdrift.coop/resources/board-meetings/
Meeting location: https://meet.jit.si/snowdrift
backup: Mumble server: snowdrift.sylphs.net port 8008 (mobile: half-duplex mode avoids echo)
See end of this pad for meeting-practices notes and reference
-->

Snowdrift.coop Board Meeting — January 23, 2021
================================================

1. Check-in
-------------
<!--

    Personal Check-In Round — Snowdrift-related thoughts are ok but not required.

    Where is your head at? — Air distracting thoughts & understand others state of mind.

    Add your name to attendees.

    Does anyone need to leave early?

    Assign facilitator

    Assign time keeper — use https://online-timers.com, start for each item, paste each url in the chat

    Assign note taker

    Assign someone to clean and save notes and update pad for next time

-->

- Attendees: Stephen, Salt, Lisha, Aaron, Erik

2. Review previous meeting
---------------------------------
<!-- Mark NEXT STEPs as [DONE] or [CAPTURED] <LINK> -->

### Approve previous meeting notes
https://gitlab.com/snowdrift/governance/-/blob/master/meeting-notes/2020/2020-12-05-board-meeting.md

- Approved: Stephen, Lisha, Aaron, Salt, Erik

### Feedback
- Aaron: thanks Stephen for leading/organizing/note-taking. Happy we're all engaged & making progress. My ideal would be that Deb was here & that would be a different process, but I don't want to take too much of her time. Process could be less wishy-washy/speculative, but I don't have specific suggestions for improvements even though I'm sure many ways to improve
- Lisha: Things seem to be moving forward, does feel slow to me, although I get we have a lot to run through. Would be awesome to have deb @ meeting, boom-boom-boom answer questions & we're done. But at least we're getting through this.
- Erik: Thanks for getting mtg more streamlined this time around. +1 lisha. My opinion differs from Aaron's tendency to show deference to everyone, sometimes we need people to be assertive and opinionated. Would be great to have someone on each area with a specific opinion/recommentation/proposal; could help move things along, & will make things less contentious move, more contentious would become obvious immediately.
- Micky: got a lot accomplished, long process & nobody is a pro at it, reachable goals; appreciate team being here
- Stephen: happy to just barely get through everything, really hard to facilitate and take notes all at once. This meeting was a bit more general and vague than I'd hoped, partly people didn't have enough solid recommendations/opinions. With more preparation, I think we can do better. Looking forward to future.
- Salt: being able to sit in without being facilitator was much nicer, tensions around being non-Board-member, but it was good that I felt I could speak up at times. Timeboxes were missing. 2 hour clarity up-front and planned break worked well. Thanks to all.

### 501(c) considerations
- NEXT STEP (Stephen): Schedule board mtg to discuss coop vs nonprofit situation, decide on goals/mission [DEFERRED]
- NEXT STEP (Aaron): Make sure we are clear on questions for Salt to pass along to his professor.
- NEXT STEP (Aaron, Stephen, Athan): update mission statement and vision statement 
    - What is this about / what is the update intended to accomplish?
    - There is https://gitlab.com/snowdrift/wiki/-/issues/20, which I don't know if it is a duplicate.
    - NEXT STEP (): Board discusses/updates/approves mission/vision statement
    - (Salt): Make checklist for this
    - (Stephen); schedule meeeting to discuss

- NEXT STEP (Stephen): Plan for bringing this up at next Board meeting [DEFERRED / maybe not needed]

### Legal/blyaws
- NEXT STEP (Stephen): Clarify^ w/ Deb what remains of our retainer
- NEXT STEP: bring up Sociocracy to Deb [DONE-ish]

3. Updates
-------------

### Metrics

(none yet)

### Treasurer's report

- Aaron: no change
2

### Open discussion <!-- Timebox: 5 minutes -->

- w: Any Q's on notes;
- e: What's the driver for renewed focus on c3? Seems like yes, it's some taxes, but that doesn't seem like the end of the world…
- w: There is a practical consideration: the day-to-day work of operating a crowdfunding platform is distinct from discussing & advocating public goods problems, advocacy — a lot of what we've produced so far.
- w: phyllis emphasized that we cannot budget funds in advance to avoid taxes — the only way to avoid paying that tax would be to have an actual invoice that came in that calendar year (e.g. work yet to do, but still planned/paid in advance).
- s: And even if we avoid liability as much as we can, someone will likely sue us at some point & we'll need to have funds set aside for this.
- w: We *could* just not keep any money and say "sure, you can sue us & put us out of business, but you're not going to get anything out of it." It's just a risk mitigation thing.
- w: Also, there is a bunch of work that we are doing that *does* fall under c3
- L: Have you considered doing everything as a B Corp? It doesn't get rid of tax liability, but does the signaling.
- w: Legally, state-level nonprofit + coop is similar
- L: If it's all a b-corp, then we don't have to worry about who recieves our $
- w: that's also the same if we're a *state*-level nonprofit, just not federal 501c3
- Salt: context — right now, the platform is not our main product; the wiki, our outreach & advocacy is.

- e: I've worked with many lawyers over my career; my experience is that there's always a risk / lawyers' *job* is to tell you about all the possible risk. We've been talking to many lawyers recently, and I feel it's weighted us away from concrete work and towards "org stuff" (smichel's paraphrase). Right now it's just a bunch of volunteers; I'm not sure there needs to be a legal structure at all yet. The reason to have a legal structure for the crowdfunding part is that you can't do it without it!
- e: I don't have a strong PoV for corp vs 501 in this situation, but biased towards our original idea, nonprofit co-op.
- sm: If we split, the crowdfunding platform, "crowdmatch.coop", would not be the 501c3, the think tank would be.
- w: We don't want to pioneer anything new legally.
- w: Phyllis' opinion was that opencollective — the closest thing to us — did it backwards (wrong part c3 vs c6)
- w: Crowdmatching could be tax-deductible if c3, but not a big deal due to small amounts
- w: 
- e: Does not having 501 prevent us from doing anything that we don't want to do if we don't have 3c?
- w: Allows us to pursue grants, e.g. behavioral economics study. Right now we have a debate about how our crowdmatching should go, would be nice to have more research to support it.
- Salt: This is not near-term; we're going to launch sooner than research.
- Salt: Concern is more about restructuring — how would we do it later if we do something different now?
- L: If your concern is getting grant money to write the software that funds the work, you can do that through Geeks Without Bounds.
- W: Yes, it's more about capacity — the OSI wants to not be a fiscal sponsor for us (or anyone) any more.
- L: We got a grant to develop distributed cooperation software, with a roadmap for other grants we're going for to continue development
- L: The team that's writing that software is made up of either employees or contractors of GWB. We get grant, pay devs, they're not employees of gorilla media foundation (to be disco media foundation in europe). It's just OSS, which happens to be used by gorilla media & other coops…
- Salt: We're also not just talking about developers.
- L: I don't just mean the people writing the code, I mean the whole team responsible for software.
- W: What would be the contrast between a c3 and making a distinction between the development (that any platform could use — the code & also codes of conduct, etc), as contractors of GWB… Is there anything missing if we don't have an org?
   - take 2: What if 501c3 stuff is happening under GWB, & snowdrift has team members
   - take 3: Can we achieve everything we need in the near future working with GWB & drawing the "line" between SD/CM.coop "this group of volunteers doing cool things on the internet" and "running crowdfunding platform, has own org"?
- E: There's a ton of other orgs, not just GWB. I think my rec is, incorporate as a coop, see what other options there are & how coop status limits us, first. Nothing limits us to work being done by coop members. Once people are working on that, *then* start exploring all the different options for that funding, and if we think it would be nice, consider creating one.
- w: I've felt similarly for a while, became more amenable when I started seeing the distinction between the two types of work.
- w: I don't want to found one, but the world ought to have a public goods think tank.
- sm: Even if we decide we're going to persue a split, we won't make both orgs right away; this is about planning so the migration is easier. To taht end, trademark is one of our main assets, how do we want to handle that?
- w: Want to prove to the world that "We can have public goods!" In a sense, I want it to be bigger than "we're just a fundraising platform" — but of course "the proof is in the pudding"
- E: Even if we split the branding, I think it belongs under the same structure.
- w: Part of the reason I've been against c3 is that our goals are tied in with cooperatives, and that coops are incompatible w/ how 501c3s are. You can have membership nonprofits, but once you're really a cooperative, the IRS is going to look unkindly on that, and I think coop matters more.
- Salt/wolftune: You could have a membership c3, which *operates* very much like a coop; and have a coop that does c3-type activities.
- E: If there is a time when folks in the coop want an org, nothing prevents people from spinning one up. One thing that OSS underestimates is how different OSS is from the typical corp world, you can have much more loosely-coupled legal structures, without ???. ???. Easy to have multiple orgs which are not subsidiaries, all pursuing similar goals. at sd: User groups, ???s,…
- E: I don't think we should constrain what the corp does & how bylaws are written, give away trademarks just yet.
- w paraphrases E: We have the option to partner with other orgs, even ones doing specific things for the coop
- E: the way wikimedia handles affiliations, they created an affiliation committee. They look at ideas for user groups, dedicated orgs, "wiki ???" partner with uni, they're a separate c3.??? able to use the wikimedia brand, wiki board can put their foot down and deny use of the brand, but in practice they're created without too much board input. We could have advocacy groups at many different levels (international, etc)
- sm:


for after break:
    - trademark ownership
    - liability issues, how does that relate to other orgs? e.g. EFF
    - moving forward w/ bylaws, team retreat

4. Agenda
------------

### Trademark ownership
- Salt: I think it's valuable to think about where our value is going to be — burning man is a similar org where most value is in trademark, transferring theirs was a fairly long process.
- Salt: When a project changes a license agreement, they need to talk to every developer who put in work under the old license. As a coop structure, we'd need to get consent from all of our patrons to make a change like that.
- W: Are you thinking about a brigade type scenario?
- Salt: Not necessarily. We think about this a lot, but asking that of every patron is a lot.
- W: Good to get legal structure clear / trademarks registered.
- Salt: There's the idea of having the company's trademark/value held in a more secure place
- W: I think that's not a huge risk if coop bylaws are set up correctly.
- E: trademark/copyright is a good distinction to make, trademark can be transferred by simple majority (unlike copyright, you need consent from everyone).
    - E: But even in copyright, wiki gnu free doc  license => cc-by-sa was able to transfer without getting 100% people, community referendum + notice was enough.
    - New org means new board, new legal structure, relationship with crowdmatch org, etc. Adds complexity and therefore potential for abuse. Besides hostile takeover, there's a risk of bad actions by the board. If we're going to have a separate org to hold assets, need convo about how to mitigate those risks.

### Liability
- W: ???? idea that we should be cautious in our operations
- W: I have this hope that we can call on people to help us if we're in danger, compared to 
- E: certian good practices, once we have $ in bank, we can have specific goals for how much runway/operating reserve, "war chest" is just that, eventually may become an endowment. ??
- L: +1, putting together a "war chest" will take time, we don't need to wait until we have that to do anything. As long as we're putting in best effort, ?? best practices, good memgber of wider community, if someone else came after us before we have those resources, EFF or crowdfunding legal fees may work for us. No matter what you do, at some point you'll be a young org that doesn't have those resources. This is something rich people worry about a lot, but when you're poor, just do it!
- W: Having some limited liability is important but yes, as long as the individuals aren't liable, if someone 
- E: In some ways it'd even be nice if a nasty corp tried to sue us out of existance — streissand effect => lots of attention & public support! Not something that lawyers etc talk about, that there's some risk we can mitigate by building community.
- W: My initial dismissal of c3 on financial concerns was, "We're not going to be taxed, because we won't have profit. There's so much work to be done, we'll just spend it all as soon as it comes in." If we're all clear that this is the plan.
- sm: if we made crowdmatch.coop also be a c3, it would allow us to do smaller charges & less fees
- L: the more money you're processing, the lower your rates go
- W: in conclusion, we should have an explanation for why, even though c3 would be nice if someone handed it to us, it seems like more of a distraction and cost than benefit right now. Are there any other reasons we haven't discussed? Or things for Salt to bring to prof?

### Moving forward

- we had this updated meeting with Deb and Aaron Williamson, and we have these example Bylaws from Deb from which we could adapt/draw-from.
- get rid of stuff that doesn't apply, compare/contrast our Bylaws bullets with stuff from her examples as a process for moving forward on our Bylaws
- wolftune: We've *told* deb that we are thinking through potential org split, are we ready to go ahead and tell her we've decided it does not affect our decision for the crowdfunding platform?
- E: Yes. [if] Mostly what we're looking to get out of deb is these bylaws, let's do that!
- sm: I want to be clear about the scope of what we're building so far.
- sm: Mission of org we're building: just crowdmatching or advocacy?
- E: Mission, think about how we support the creative output of humanity as public goods? Bigger than just ??. Can we frame that in one sentence in our mission? In practice that extends to building community & engaging with ?? with geeks without bounds and other partner orgs.

- W: we had a clear mission, brought it to lawyers, they brought up things, we considered, now back to clear.
- W: I want to clarify: fundraising platform is means to an end. If the end is humanity's potential creative output all as public goods, fundraising is not the mission in and of itself.
- E: Encourage folks to check out wikimedia mission/vision. Mission is very verbose, wouldn't recommentd that much, but it does speak to mission of networking with others.


5. Meeting evaluation / feedback / suggestions / appreciations round <!-- Timebox: final 5 minutes -->
--------------------------------------------------------------------------------------
- Salt: I didn't like not being attentive at the start. Liked 2-part split with open discussion at the beginning & then brain dump for clear agenda of 2nd part.
- E: Good discussion, reminds me of FOTPF board operation sometimes; open-ended conversation, then followed by specifics. Thanks for "train-of-thought-ing" verb.
- L: Went pretty well, towards the end (last 10-15) felt a little long, but otherwise smooth.
- sm: split worked nicely, toward the end seemed we extended conversation, hard to say things and take notes
- W: realized it would have been ideal if Athan (or someone else) were here to take notes. Like the practice of brain-dumping before break, helps both with picking up where left off & being able to let it go during break. Appreciate Erik & Lisha's experience, helping us learn from your mistakes & not have to make them ourselves




<!--

    Meeting adjourned!

    Clean up meeting notes, then add to wiki

    Prepare this pad for next meeting:

    Clear meetings date, attendee list

    Clear report / metrics (keeping the topic titles)

    Replace previous meeting eval notes with new and filter it down to essential points

    Clear the "last meeting next-steps review" section

    Move new NEXT STEPs to that review section and then clear out other notes areas

    Change the last-meeting-notes wiki link to this past one

    Clear authorship colors

-->

<!--
Meeting best-practices and tools

Etherpad use
- Use chat in etherpad (and add your name)
- Option: audio notifications on firefox via https://addons.mozilla.org/en-US/firefox/addon/notification-sound/

Agenda topics
- Each topic facilitated by topic lead with main facilitator help
- As needed, ping !folks on matrix to read over anything advance, ideally before the day of the meeting

NEXT STEPS
- each topic should capture NEXT STEPS (or clarify that there aren't any)
- should be clear and actionable
- assignee agrees to capture or complete by next week

Timeboxing
- timebox each topic, rounded to nearest 5min., settled during agenda confirmation
- format is: "Timebox: 10 minutes (until hh:mm)" (in an html comment so it doesn't appear in note archives)
- at topic beginning, convert the :mm to expected end time
- at timebox end, "thumb polls" may add 5 minutes at a time
- hand symbols
    - "^" approve, extend the timebox
    - "v" disagree, move onto the next topic
    - "." neutral

Discussion options
- open discussion
- call for a round ("pass the mic" style, facilitator makes sure no one is skipped)
- hand symbols
    - "o/" or "/" means you have something to say and puts you in the queue
    - "c/" or "?" means you have a clarifying question and jumps you to the top of the queue
    - "d" means thumbs up, encouragement, agreement, etc.
    - ">" means you understand someone's point, please move on
    - "d>" indicates feeling complete on an agenda item, ready to move on to

Notetaking
- "???" in notes means something missed, please help capturing what was said
- aim for shorthand / summary / key points (not transcript)
- don't type on the same line as another notetaker ( ok to do helpful edits after but not interfering with current typing)
-->
