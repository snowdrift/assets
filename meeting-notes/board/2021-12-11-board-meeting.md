<!--
Previous meeting notes: https://gitlab.com/snowdrift/governance/-/tree/master/meeting-notes/board
Meeting location:https://meet.jit.si/snowdrift
backup: Mumble server: snowdrift.sylphs.net port 8008 (mobile: half-duplex mode avoids echo)
See end of this pad for meeting-practices notes and reference
-->

Snowdrift.coop Board Meeting 2021-12-11
===================================
### Personal Check-In Round
- Attendees: Aaron, Erik, Stephen, Salt, Micky, Eli
    - Leaving early: 
- Facilitator: Salt
    - Facilitation links: <https://smichel.me/snowdrift/timer/>
- Note Taker: Stephen
    - Note-saver/pad-updater (if different): 


### Previous meeting appreciation
- Stephen: we could do better in our workflow and communication, but happy we're really making progress
- Salt: I wish things were progressing faster and more fluidly, but thanks everyone for being here and putting in the time
- Mickey: We are moving forward, we got things decided, moving at all is really good
- Erik: Seems we do have a clear plan toward resolution on the Articles, let's avoid continuing to come up with doubts when we re-read, let's avoid that, and know that we did look at things before, don't re-open old discussions
- Aaron: Yes, doubting mindset is a problem for sure, I have noticed it myself (was listening to a Buddhist talk on the topic). If we fall into doubt in an unhelpful way that isn't answering useful questions, we just get stuck. Let's keep moving. Thanks all


### Meeting updates/reminders

- Try for better timeboxing, even if it means topics get cut off. Be clear about who will track timeboxes.
- Structure could be better timing and clear boundaries during meeting, .
- Let's focus on satisficing instead of maximizing— "good enough" instead of crossing every t and dotting every i.
    - We should call each other out, maybe just talk about it as a tension.

### Minute of silence
https://www.online-timers.com/timer-1-minute

Updates
-----------
### Confirm approval of previous meeting minutes

### Metrics
(none yet)

### Treasurer's report

- Unchanged from last time

- update on BTC and Stephen…
- [x] Aaron: follow up with David on donation in USD
- [ ] Aaron: Handle accounting for lost BTC
- [x] Aaron & Stephen: Confirm that Stephen has access to the Snowdrift bank + paypal accounts

- [ ] Aaron: Check back in with David in March 2022

### Previous meeting NEXT STEPs


Agenda
----------

### Open Collective, FundOSS, broad market reflections and updates (Aaron) - 1 hour

#### Aaron presentation (questions & concerns) - 15m

- A: I did a bunch of research, some on the forum
- A: Please interrupt me to clarify anything/confusion.
- A: My goal: Share my perspectives, hear back enough from others that I can say "Yes, that's right".

The closest org to what we're trying to do is OpenCollective. We've done a huge amount of research into other orgs, e.g. Liberapay. The thing that makes OpenCollective the closest is the way they've *framed* things— in a cooperative manner, etc. Seems less like just a tech thing. And, they were thinking about challenges, not "just donate". 
They identified "legal challenges"— how to deal with injecting money into a project.
- It's an *important* challenge, something *we* never really knew how to deal with.
- Their software is all Open Source (vs many are proprietary)

In our review of other fundraising platforms generally, we pointed out about OC:
- They weren't doing any sort of matching
- They do take a cut of each payment, and so does the fiscal host (for the service of managing money)
- They are VC funded. So we don't know what their future is.

More recently:
- FundOSS:
    - Ethereum is a thing. Crypto with contracts.
    - Gitcoin was a crypto thing, "use money to do stuff". Started off with bounties.
        - They expanded, and added what they call quadratic funding / democratic funding.
        - Based on quadratic voting, where you pay more to get more votes (each additional vote costs 2x)
    - They identified: traditionally, small donors get very little say.
    - In their mechanism, everyone pays into the same pot, but if you're a small donor, you get more influence over which projects get which amount of the funding.
    - Erik: How do they distinguish different people in the crypto world?
        - A: Pretty sure they don't; if you made a bunch of wallets you'd get more say than one.
    - *Rhetorically*, OC sounds like us: "we don't want big money calling the shots, we want representation and democracy…"
    - At the time, GitCoin was funding exclusively other crypto projects, accomplishing nothing for real people, as far as I could tell.
    - FundOSS uses the same math. They got themselves accepted as a project on Open Source Collective, which is itself an OC fiscal sponsor (FundOSS is not a fiscal sponsor).
    - They ran one test pool: the "SustainOSS" (corporate folks, e.g. OpenSourceCollective, involved in Chromium development— people in our space but talking less about coops: "VC is great, but doesn't fund upstream…")
        - Started with real dollars, not just crypto, maybe?
        - Mostly crypto projects, but some "real world"— EFF, freedom of the press foundation, 
    - I misunderstood it at first as a variant of crowdmatching. It's not really— it's big pool with votes (no matching).
        - We're doing a sustaining thing with matching, not one-off donations.
- OpenCollective publicly saying, "We'd love to be a coop, it's too bad we're not. We owe $3m to our investors, we'd like to somehow pay them off & become a coop."

    - That's a big thing, to be addressing these issue **publicly.**

    - personal thoughts:

    - I don't know if they'll be able to find an out, but it opens up a bridge to talk to them

    - I believe the *people* there are aligned.



    - https://blog.opencollective.com/exit-to-community/

    - https://blog.opencollective.com/exit-to-community-part-2/

- main questions

    - how much time/energy do we want to spend talking to OC?

    - Realistically, we have a dilemma of time/capital (or lack thereof)

    - Most orgs seem to get VC/crypto funding.

    - Most cynically: Because the capital of those sources exists, they can fund *enough* dead-ends to keep us from succeeding.

    - On the other hand: they're actually funding these other things (handing money for projects), which it would be nice if we didn't have to handle :)

    - There are grants out there (we applied NlNet, Aaron has previously applied shuttleworth fellowship…)



Tie-in topics (maybe not to discuss today):
- web3
- distributed crypto
- DAOs


#### Clarify facts round - 10 minutes
- Micky: We were heads-down working on Drutopia, forgot about visibility; we recently asked people to star the project; in the OpenCollective literature, it says a project needs 100 stars.
    Aaron clarifies:
    - OpenCollective is a VC-funded website. It develops the software (also called OpenCollective?) for transparent budgeting that runs on the website.
    - Open*Source*Collective is a 501c6 org that is a fiscal sponsor on OpenCollective; you can get fiscal sponsorship from them by having the stars, etc.
- Stephen: no additional thoughts/questions
- Eli: Opinions:

    - Huge difference between what they're doing and what we are.

    - Heterogeneity is important (a core thing within Open Source)— good for evolution/robust-ness of environment.

    - Not close enough to close us down.

    - Their system is a different kind of popularity contest: All the orgs will get money, but prioritized by how the voters vote. It's a weird system. I might personally prefer to give my money to the smaller, less popular groups.

    - e.g. Everybody knows & will vote for the EFF. Smaller orgs won't have as much of a chance within that pool. With our system, something like GnuPrivacyGuard, which also needs funding, isn't competing with EFF, and the risk of giving a bunch of money to something you don't want, or won't work, is less.

   Aaron: Yes, they definitely feel "zero-sum", and not sustaining. Also, if you want a society built on trust, you need time to build trust— you can't do that in a trustless system where there's no opportunity to "defect". With crowdmatching, you have time at low funding levels to build trust.
- Erik: I don't fully understand the relationships between all the orgs (OC, OSC, FundOSS, GitCoin…). It's clear that there's a lot of money flowing to OSS projects through crypto. Great short term, but feels like a house of cards. VC funding remains an issue, not clear that "Exit to community" will succeed, feels premature for us to throw in the towel.
    - Good opportunity to evaluate our goals for 2022, set deadlines for doing something or wind down the platform, not keep this going indefinitely if we don't have the ability to get launched.
   Aaron: My perspective/reflection: they started not long after us (2014?)— the difference between where they are we are now is the $3MM VC funding they took.
- Salt: Thanks for laying this out, especially doing extra digging. I wonder if there's a moment where people working on similar things have a breakthrough (technical or public consciousness) that allows things to move forward, and then everyone's doing it.

    - That they are doing public outreach on this is an opportunity for us. Maybe an opportunity to connect with them and help shift the narrative.

    - I do think we're doing something different, it's why we keep coming back to public goods.

- Aaron: When I called this meeting, I was not sure to what degree they were making something like what we're doing. I'm now more confident that nobody else is doing what we're trying to do. So it's more a question, "How does their existence affect us?" It would be easier to pitch to the world that we're needed if they didn't exist. Positives of their publicity, more people talking about it

     - More energy & funding, maybe we can harness some of it. E.g. get snowdrift funded using OC, or convince OC to implement crowdmatching. Point is we should be open to not just saying "we have our path, it's not them ⇒ ignore them.

    - E: I'm not sure they're useful in transitional phase for snowdrift. Our incorporation would un-block recieving grants. The purpose of funding Snowdrift through Snowdrift is to have a test-case for crowdmatching. Opening an OC account seems like it would delay that. I don't think we should specifically single out OC just because there's crypto money flowing through them now.



#### Aaron asks other people questions

How much to balance time spent on: "blinders on, we know how the world works, we have work to do." vs "The world's changing, there's all this stuff."
- I feel like I got some useful perspective from recent research
- OC more than others, while not really doing what we want, are a good way to gague challenges through them.
- E: I think enumerating the differences is good. Like you did here: crypto, VC, different model. If those things change, it can change our position towards them, but for the moment I don't see OC as an exit strategy for Snowdrift.

I see more and more projects that we'd like to host on OC. They might be fiscally sponsored there, how would that work if they want to use Snowdrift?
- Eli: I think it's worth talking to OC. "Hi, we're in similar space, kind of competitors but kind of not. " It's good to have a relationship with them as "neighbors". I don't think it's a big deal for a project sponsored by OC to be on our platform. I'm sure there will be others with different fiscal sponsors. If there's any issues with fiscal sponsors, we'll have a line of communication to OC to work those issues out.
- Erik: We're more FLO than them (all libre stack). If we have these issues, they're good to have. Q is, "do we still want to get there? by when?"

In terms of launching Snowdrift, is there a way for us to take advantage of the fiscal sponsorship work they've done?
- Erik: After we've got the fundamental donation software sorted, figuring stuff like that out will be the next step.

Salt: We've struggled with how to handle money. We don't have any, but it's a challenge nonetheless (affecting decisions to seek money). We were looking for some guidance about that from the OSI…
- Aaron: It would be great if an aligned org with infrastructure would help us get going— a grant or accellerator program with not just money but also guidance/expertise. I've wished for years that people with resources/expertise/connections could help us accellerate.
- Salt: I wonder if this is some context for having a conversation with OpenCollective. Grants often seem not totally open— if you ask the right person for the right thing, etc.
- Aaron: I think that when we're at the point when a random person off the street can come to our website and *get it* (even if pledging is not live yet), it would help us apply for grants…


Stephen: [Not a clear question] Working with other orgs and coops? E.g. hcoop has people with technical skills
- Erik: If they have skills, and being an early project can be one incentive for them to help us, that could be good. I'm wary of piggy-backing on another org as a way to avoid getting this one set up.

### Articles restatement go ahead (Aaron) - 5 minutes
- A: Stephen and I talked with Deb, have articles 100% complete. If I didn't want to have this meeting, we could have just filed them and moved on to bylaws.

    - Salt: I'd like to read them over before filing

- A: We cleared out boilerplate relating to shares. I just need to deal with the technical process of signing— get things on the form with the right dates.
- A: I'll be the only signer (incorporator), but all of our names on the board of directors.
- A: In the restated articles, we're not doing anything 501. Partially, the articles state that the mission is for the benefit of the people funding the things— are we ready to "pull the plug" on 501?
- A: Deb and I are bartering for her time as a co-op consultant (the political questions); we'll still pay her normally for *legal* time, which I think will be much less.
https://docs.google.com/document/d/1o_v5r3F-Kzfo2TVt1uURBpD2UWAkL1JlnY_yPpAJLxs/edit

https://docs.google.com/document/d/1cRGUuNM-eK3Aqfk6Z5GTfKuVHzlb8vJr3M5UgdaN_L4/edit#heading=h.4193m6pj0qjw


- A: I have a friend in Michigan who agreed to be the registered agent.
- [ ] Aaron: Send pdfs to the Signal group today
- [ ] All: Approve them by monday
- [ ] Aaron: file in 2021

### Open discussion (Facilitator) - 5 minutes

Wrap-up
----------
### Check that all NEXT-STEPS have someone assigned (Facilitator)

- Next board meeting: discuss road to viability

### Reflection Round — short, concrete, & specific

- Aaron: Thanks for everyone for helping me get my head on straight. Especially to Salt for adjusting facilitation to be human and flexible, but still proactive. Stephen for notes. Everyone getting here on time.
- Erik: Feeling that facilitation is smoother / snowdrift team getting better at it. OpenCollective discussion could have been more time-bounded.
Cautiously optimistic that we'll get momentum going after incorporation.
- Stephen: Echo thanks, interesting thing Amazon does is have longer meeting and provide time to read at the beginning, might be thing to consider. Thanks all, looking forward to the next conversation.
- Salt: Thanks all for still being here. It's good to think about ways to get everyone more engaged, but also as long as everyone's here, it's ok… "too much time spent making the path perfectly clear vs allowing puddles to exist".
- Micky: Thanks to everyone for being here, good clarification on OC, thanks for everyone being present, looking forward to articles review.
- Eli: Thanks everyone for attending, Salt for keeping us on track. Thanks for everyone's feedback, looking forward to new year. Hope to get "Snowball rolling fast enough to achieve escape velocity."


Meeting Adjourned!

<!--

    Clean up meeting notes, then add to wiki

    Prepare this pad for next meeting:

    Clear meetings date, attendee list

    Clear report / metrics (keeping the topic titles)

    Replace previous meeting eval notes with new and filter it down to essential points

    Clear the "last meeting next-steps review" section

    Move new NEXT STEPs to that review section and then clear out other notes areas

    Change the last-meeting-notes wiki link to this past one

    Clear authorship colors

-->

<!--
Meeting best-practices and tools

Etherpad use
- Use chat in etherpad (and add your name)
- Option: audio notifications on firefox viahttps://addons.mozilla.org/en-US/firefox/addon/notification-sound/

Agenda topics
- Each topic facilitated by topic lead with main facilitator help
- As needed, ping !folks on matrix to read over anything advance, ideally before the day of the meeting

NEXT STEPS
- each topic should capture NEXT STEPS (or clarify that there aren't any)
- should be clear and actionable
- assignee agrees to capture or complete by next week

Timeboxing
- timebox each topic, rounded to nearest 5min., settled during agenda confirmation
- format is: "Timebox: 10 minutes (until hh:mm)" (in an html comment so it doesn't appear in note archives)
- at topic beginning, convert the :mm to expected end time
- at timebox end, "thumb polls" may add 5 minutes at a time
- hand symbols
- "^" approve, extend the timebox
- "v" disagree, move onto the next topic
- "." neutral

Discussion options
- open discussion
- call for a round ("pass the mic" style, facilitator makes sure no one is skipped)
- hand symbols
- "o/" or "/" means you have something to say and puts you in the queue
- "c/" or "?" means you have a clarifying question and jumps you to the top of the queue
- "d" means thumbs up, encouragement, agreement, etc.
- ">" means you understand someone's point, please move on
- "d>" indicates feeling complete on an agenda item, ready to move on to

Notetaking
- "???" in notes means something missed, please help capturing what was said
- aim for shorthand / summary / key points (not transcript)
- don't type on the same line as another notetaker ( ok to do helpful edits after but not interfering with current typing)
-->
