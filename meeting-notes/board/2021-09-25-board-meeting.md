Snowdrift.coop Board Meeting 2021-09-25
===================================
### Personal Check-In Round
- Attendees: Aaron, Erik, Stephen, Micky, Salt
    - Leaving early: 
- Facilitator: Aaron
    - Facilitation links: <https://smichel.me/snowdrift/timer/>
- Note Taker: Erik + Stephen
    - Note-saver/pad-updater (if different): 

### Meta: invite Deb to join?

- Stephen: I think it would be far more efficient to resolve outstanding questions with deb synchronously. Do we want to see if she can join this meeting?
- Aaron: Could also do it with just you and I later
- Stephen: I think it'd be better if we're all there.

### Previous meeting appreciation
- Stephen: I like the physical facilitator hat.
- Was off my expectations with late start and not full attendance.

### Meeting updates/reminders

- Try for better timeboxing, even if it means topics get cut off. Be clear about who will track timeboxes.
- Structure could be better timing and clear boundaries during meeting, .
- Let's focus on satisficing instead of maximizing— "good enough" instead of crossing every t and dotting every i.
    - We should call each other out, maybe just talk about it as a tension.

### Minute of silence
https://www.online-timers.com/timer-1-minute

Updates
-----------
### Metrics
(none yet)

### Treasurer's report

- Unchanged from last time

- Contacted phyllis from OSI as knowledgeable acountant, OSI and Python foundation avoiding touching BTC when possible ("please just give us dollars")
- We mentioned it being lost, she said it'd be fine/easy just to write it off, and David can donate separately if he wants to.
- [x] Aaron: follow up with David on donation in USD
- [ ] Aaron: Handle accounting for lost BTC
- [ ] Aaron & Stephen: Confirm that Stephen has access to the Snowdrift bank + paypal accounts

### Previous meeting NEXT STEPs
- [ ] Some BTC purchase agreement with David
- [ ] David sends funds into our bank account in exchange for BTC
- [ ] Update accounting/reporting to account for BTC
- [x] Stephen: import docs from forum thread into GDoc for collaborative review & annotation.

Agenda
----------

### Articles finalizing (Aaron) - 30 min
- Review of open issues & Deb's comments


What are the distinctions between cooperatives? Are there for-profit coops under this law? If not, all this discussion may be superfluous. Are there boundaries within the law we should be aware of, when considering language such as "other lawful activity"?

> MI Has two coop laws: the farmer coop law in the for-profit and the consumer coop in the nonprofit. It's the newer law from the 1970's. I don't think you'll have much trouble: you're making a consumer coop under the consumer coop law, I don't see any cause for concern.

> In the coop context, nonprofit means you're operating at cost for the benefit of the members. Same with worker coop or whatever. The purpose is not to benefit shareholders with profit, it's to provide a public good, operating at cost. It does not mean being a charitable institution. A nonprofit 501c3 or 4 could also organize under this statute, but I don't think it's a problem.

Is the term "patron" an appropriate term to use in the Articles for people funding public goods via the Snowdrift platform? 


Crowdmatching and fees? How will it technically work?

Can our membership agreement require yearly voting? Does article 8 refer only to amendment of the articles or any change to the membership agreement?



- Aaron: Finish process, submit updated articles to MI, only have our own imposed timeline for getting bylaws in place.
- Aaron: I propose that we draft bylaws to the point where we start using them, before we actually approve them. That way we don't need to have a sunset clause. We'll know we're comfortable at that point because we'll have been using them.

### Open discussion (Facilitator) - 5 minutes

Wrap-up
----------
### Check that all NEXT-STEPS have someone assigned (Facilitator)

### Reflection Round — short, concrete, & specific

- Stephen: we could do better in our workflow and communication, but happy we're really making progress
- Salt: I wish things were progressing faster and more fluidly, but thanks everyone for being here and putting in the time
- Mickey: We are moving forward, we got things decided, moving at all is really good
- Erik: Seems we do have a clear plan toward resolution on the Articles, let's avoid continuing to come up with doubts when we re-read, let's avoid that, and know that we did look at things before, don't re-open old discussions
- Aaron: Yes, doubting mindset is a problem for sure, I have noticed it myself (was listening to a Buddhist talk on the topic). If we fall into doubt in an unhelpful way that isn't answering useful questions, we just get stuck. Let's keep moving. Thanks all

---

Meeting Adjourned!
