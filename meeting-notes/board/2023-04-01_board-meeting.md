# 2023-04-01 Board Meeting

## Timebox

- Agree to how long this meeting should be / how long between bio breaks.

## Approve Previous Meeting Notes

There are 3 meetings whose minutes need approval.

- https://gitlab.com/snowdrift/governance/-/merge_requests/24
- https://gitlab.com/snowdrift/governance/-/merge_requests/25
- https://gitlab.com/snowdrift/governance/-/merge_requests/26

## Treasurer's report

We have 2 accounts, savings & checking. $5 in savings (required by bank, unused) and 87.66 in checking (1 cent of dividend per month)

Looked into multiple-person access, no way to do it with different credentials online. We can add another person in person, plan to add Salt so we have another person with the capacity to access the account, when it's convenient.

## Progress Update

- Ran credit card charges
- We made a decision to start charging credit cards without the site being completed, and without the IRS having updated our corporate info.
- Stripe is reserving taxes as though we are a c-corp already.
- Ran charge on Stephen's card. Charge came out of his account, the balance is now in Stripe, but not in Snowdrift CU account. Stephen will now send the money to the Snowdrift CU account.
- Micky: Is it worth looking into GNU Taler (now starting to be functional)? 
    - Stephen: Not right now, because the old codebase won't let us do that. Future code base will hopefully open that option.
- Aaron: Many people have pledged and their amount has reached a worthwhile level. 
    - Aaron will draft manual email to team member as first version, get feedback (role playing as patron). Emails will be handled manually because we have no email/charge integration on the old site.
    - We'll reach out to ask people to update cards, hopefully some engagement from that. We won't do any more publicity than that, just engaging with people already signed up.
    - We'll get some funds, $XXX
    - Will say something about what happens with pledges after this initial charge period.
- Stephen: I plan to personally cover $ for people who have expired credit cards & can't be charged
- Aaron: Hope to transition to new site before running additional charges.
- Stephen: We have progress w/ new site - front end and back end. It's slower than we would like. 
- Aaron: Michael and Connor have been the other team members consistently around, Michael around when needed, Connor actively working (volunteer basis obviously).
- Aaron: Still in touch with other volunteers and prior-active team members though little activity now

## Return to Bylaws

- A: We had a decent draft, never enough to adopt, need to decide on path forward
- M: We should have a bylaws-only meeting, soon.
    - Others: :thumbsup:
- A: I can prioritize this, maybe not as much in the next couple weeks
- A: How much legal involvement do we need?
- Sa: not much, unless we're hiring people
- A: As a coop, need elections and thus need bylaws for running them
- E: Fixed in another org by making initial members were board of directors. We put that in the bylaws.
- A: Good for doing that?
- SM: sure, initial board and team?
- A: Or we could do people as we charge them — that would also give us a time frame, where getting bylaws in place is a prereq to charging more members, I like the idea of being able to communicate that 
- E: I like the ide ao fbeing able to send them a link to existing bylaws before cahrging them.
    - If we do 1 by 1 (current collaborators) that's easier
- A: Seems like we can tie bylaws timeframe to charging people. Some time this summer?
- E: After first meeting, we'll have some idea of how many more meetings we need, can use that to set target date

- Come up with deliverables & timeline
  - w/ shorter milestones
 
 Decision: We are preparing the bylaws to be usable enough by team members & current board members as we charge them. Tentatively by July. First meeting in May.

## Board

### Recruitment

- A: Since Erik stepped down, might want additional perspectives. We also had some number in the bylaws draft (7), seems relevant.
- E: Could look on Linkedin, they have a "looking to join nonprofit board" section


- [ ] A: Compile latest bylaws stuff by the 5th of April

- Micky: Maybe make a pad to jot down what skills we're lookign for in a perfect candidate
    - [ ] SM: Create a pad (today)
    
- M: In the list of bullets, if we see someone suggest a skill we already have, put a note next to it (initials)

- Board continuation expectations going forward
  - Maybe this can be first bylaws milestone

### Formalize Meeting Planning

To be incorporated into the bylaws, but more practically about scheduling our next meeting(s).

- A: Tension around having timely meetings, especially when I'm not personally as available. I guess it's partially a secretary role. Not sure what best options are, just uncomfortable w/ status quo
- SA: If we want to have quarterly board meetings, announcements need to go out 1-2 weeks in advance, so we need a way of gathering availability.
- SM: Imporant to have a schedule. e.g. 1st day of month, secretary prompts for schedule updates; 1 week later proposes meeting time, 1 week later decides
- A: For now, are you SM OK being the scheduler?
- S: Yes, with and end date
- A: By end of July?
- S: OK.

### Communication tools/preferences/norms


- A: Generally we don't use Signal at Snowdrift, we have each other's numbers but it's more like casual texting people
- A: We use Matrix for free form and forum for long-form. Using the forum for the board hasn't been reliable.
- A: I'd like to propose making a Snowdrift Board matrix room
- A: ROUND: thoughts/opinions on different tooling, what you'd use?
    - M: I'd need a primer on Matrix, I've used Discourse. I like BBB for live meetings.
        - [ ] Salt and Micky walk through
    - E: [Missed this in notes]
    - S: Valuable to have different communication channels. Email is hard for me. Like the idea of forum but check it irregularly because I want to engage fully there. Signal is like a call, don't want to be pinged frequently. Matrix is my go-to. We should do some infrastructure work if we create a matrix board channel. Our matrix rooms are V6, current is V10. Upgrading is a bit of a hassle because people don't get automatically invited.
    - SM: Very similar to what Salt said.
    

- [ ] S: Make a Board channel on Matrix

- We have Signal group— maybe not the best place for less important messages
- Might be missing casual interactions that we'd get on e.g. Matrix

## Future meeting topics

### Groom meeting topics

- List topics we know we need to get to in other meetings soon
- Clarify enough to know what they are
