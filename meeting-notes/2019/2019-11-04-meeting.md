<!-- Previous week's meeting notes: https://wiki.snowdrift.coop/resources/meetings/2019/ -->
<!-- Meeting location: https://meet.jit.si/snowdrift -->

# Meeting — November 4, 2019

Attendees: msiep, wolftune, salt, alignwaivers, photm, smichel

<!-- Check-in round -->

## Metrics

Discourse (over past week): <!-- stats from https://community.snowdrift.coop/admin?period=weekly -->

- Signups: 0 -> 1
- New Topics: 1 -> 2
- Posts: 11 -> 16

## Reminders on building new best-practice habits

### Sociocracy stuff
- Drivers, describing driver statements: https://patterns.sociocracy30.org/describe-organizational-drivers.html
    - Current situation + effect + what's needed + impact of doing it
    - forum discussion of our use: https://community.snowdrift.coop/t/sociocracy-3-0-drivers/1309

### Use of GitLab kanban boards
- Use the boards to pick your tasks, dragging or marking To Do and Doing appropriately
- https://community.snowdrift.coop/t/using-the-kanban-boards-in-gitlab-plus-wolftune-check-in/1263

## Carry-overs

```
## Salt met author of unfinished book on patronage funding at Communications conference
- NEXT STEP (salt): follow up with conference contacts

## Issue grooming / dumping etc
- <https://gitlab.com/groups/snowdrift/-/issues?milestone_title=LFNW+2019>
- NEXT STEP (smchel17): Delete or close the LFNW 2019 milestone
- NEXT STEP (smichel + wolftune + alignwaivers): schedule grooming session
- NEXT STEP (msiep): groom design repo issues

## Team communication commitments etc
- NEXT STEPS (wolftune + alignwaivers): keep studying Sociocracy ideas around this, find current stuff we have, organize our statements https://gitlab.com/snowdrift/governance/issues/57

## Marking prominently that we're under-construction
- NEXT STEP (mray): respond to proposal https://gitlab.com/snowdrift/design/issues/111

## Nathan Schneider as Board recruit?
- Michael went on System 76 tour with him, welcome to use that reference as a prompt
- NEXT STEP (wolftune): reach out to Nathan

```

<!-- New Agenda Down Here  -->

## SeaGL
- NEXT STEP (athan + wolftune): get accommodations planned, see if anyone wants to come up with us
- we'll have a table
- Aryeah will be there and can help at the table
- Things to focus on: we are going through long transition to OSU, lots of volunteer opportunities
- Also discuss Sociocracy, things to get help with? Meeting note taker
- Display, demonstrate, and maybe collect, project interest forms
- Collect emails for mailinglist, signups, somehow to display current match for us
- Priority is on team building
- form for projects interested! Get some involved as much as we can
- https://wiki.snowdrift.coop/ has a good next-steps
- putting Snowdrift.coop reference in our separate talks etc
- wolftune: my talk is about Transformative Justice etc. will reference our Snowdrift.coop CoC and values

## CiviCRM
- OSUOSL is going forward with clean install of Drupal and CiviCRM
- after install, we'll have to do configuring
- then import of data
- after CiviCRM is working, various design tasks for survey forms / volunteer intake forms…

## Donation CRM, follow-up
- NEXT STEP (wolftune): make a spreadsheet for stuff-to-go-in-Civi as in the recent donation
- Use thoughts here: https://gitlab.com/snowdrift/outreach/issues/28

## Code development progress
- NEXT STEP (chreekat): run the backlog of crowdmatches https://gitlab.com/snowdrift/snowdrift/issues/169 (blocked by needing to build and deploy updated site)
- chreekat made progress on CI with GitLab, but got stuck on something with postgres
- once CI/CD is working, we just also need OSUOSL to run the main site server, then we can get everything updated
- photm: more clarity about the priorities and why they are (and whether they are valid) would be helpful
- photm: reading a few issues marked high priority, I had the feeling that they were not actually high priority. For example, upstreaming our postgresql fork
- wolftune: the broader issue seems to be issue grooming — everything should have driver statements in addition to just what needs doing. & we want to do more regular grooming.
- NEXT STEP (photm): ping chreekat (and others) at https://gitlab.com/snowdrift/snowdrift/issues/8 to check what the priority was, why was this a blocker? Was it related to getting the codebase updated to upstream dependencies?

## Athan met industry person, wastewater management FLO software something
- Ask about using mechanism at institutional level
- Encouraged forum participation
- Short answer, idea of different level of patronage for instutitions

<!-- Decide where to capture outstanding tasks -->

---

## meeting evaluation / feedback / suggestions round
- need note taker
- consent to the agenda would have been good
- people who have to leave early could then make sure the order will fit them
- maybe more clear rounds, discuss tensions, have more explicit proposals and objections/concerns



<!-- Closing round -->
<!-- Capture TODOs -->
<!-- Add meeting notes to wiki -->