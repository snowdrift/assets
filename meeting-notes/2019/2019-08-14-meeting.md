<!-- Previous week's meeting notes: https://wiki.snowdrift.coop/resources/meetings/2019/ -->
<!-- Meeting location: https://meet.jit.si/snowdrift -->

# Meeting — August 14, 2019

Attendees: wolftune, MSiep, salt, iko, aryeah

<!-- Check-in round -->

## Metrics

Discourse (over past week): <!-- stats from https://community.snowdrift.coop/admin?period=weekly -->

- Signups: 0 -> 0
- New Topics: 1 -> 1
- Posts: 19 -> 18

Sociocracy 3.0, wolftune @ page: 11 of 37

## Reminders on building new best-practice habits

### Sociocracy 3.0 stuff
- Drivers, describing driver statements: https://patterns.sociocracy30.org/describe-organizational-drivers.html
    - Current situation + effect + what's needed + impact of doing it
    - forum discussion of our use: https://community.snowdrift.coop/t/sociocracy-3-0-drivers/1309

### Use of GitLab kanban boards
- Use the boards to pick your tasks, dragging or marking To Do and Doing appropriately
- https://community.snowdrift.coop/t/using-the-kanban-boards-in-gitlab-plus-wolftune-check-in/1263

## Carry-overs

```
## New Project Forms
- NEXT STEP: (salt) discussing LibreOffice draft with Kodi and/or other closer project contacts
- NEXT STEP (msiep): incorporate links to project page mockups in LibreOffice stuff

## LFNW + beta milestone prioritizing/scheduling
-  <https://gitlab.com/groups/snowdrift/-/issues?milestone_title=LFNW+2019>
- NEXT STEP (smchel17): Delete or close the LFNW 2019 milestone

## Salt met author of unfinished book on patronage funding at Communications conference
- NEXT STEP (salt): follow up with conference contacts

## design progress, pace / how's h30
- NEXT STEP (wolftune): bug h30 (Henri) about documenting process of implementing mockups <https://gitlab.com/snowdrift/snowdrift/issues/158>
- NEXT STEP (all): recruiting front-end (haskell?) developers

## Coworking times
Set 1PM Pacific Fridays
NEXT STEP (wolftune): announce new coworking time

## IRC / forum activity
- need more activity, prompting etc. etc.
- mray: [longer, clear, well-said discussion about the need to just move forward in desperation toward whatever we can do, can't be perfect or always wait for someone else to do the right things etc]
- NEXT STEP (mray): post this and other questions on the forum

## Comparison page
- wolftune: emphasizing that a term like "mutual assurance" or reference to the "coordination problem" could be an important distinction, as in Snowdrift.coop and Kickstarter both check that while Patreon does not.
- NEXT STEPS (mray + msiep + ): continue work on this page, consider sharing with Kodi soon etc.


```

<!-- New Agenda Down Here  -->

## Cert renewal
- NEXT STEP (wolftune): renew through Globalsign ASAP
- update: submitted request, but we have a problem, see https://globalsign.ssllabs.com/analyze.html?d=snowdrift.coop
- what is going on? What do we need to do? So far no response from GlobalSign email
- iko tried the IP's and noted that one has a bad cert going to a different domain
- wolftune submitted the problem to OSU via Trello
- NEXT STEP (wolftune): follow up with OSU, continue cert renewal process as soon as they fix things on their end

## Discussion of fees / no-fees
- discussion of https://community.snowdrift.coop/t/questioning-how-snowdrift-coop-itself-should-be-funded/1349
- not much to add besides what is discussed in the forum
- fees are possible, but could be added via working co-op governance, we hope to not need it
- earliest projects need to understand the tenuousness of the stage of things

## Marking prominently that we're under-construction
- we used to mark "work in progress" on homepage
- not sure about best term or presentation
- prelaunch / beta something like that
- wolftune: I see no downsides at all, only benefits from making this clear
- msiep: besides disliking "under construction", don't see other problems, makes sense
- NEXT STEP (msiep): move forward the idea of marking some state-of-the-platform expressed more clearly (issue or forum post etc)



<!-- Decide where to capture outstanding tasks -->
<!-- Closing round -->
<!-- Capture TODOs -->
<!-- Add meeting notes to wiki -->