# Meeting 10

*December 21 2013, 9AM Pacific*  
*phone chat on freeconferencecall.com (605) 475-4000 pin 564511#  
and live-recording of minutes on [etherpad](https://snowdrift.etherpad.mozilla.org/1)*

## Background readings / updates / tasks

### Background readings / updates

Continue exploring the updated site, submit any bug or feature tickets or add discussion comments that arise from your exploration.

Further planning for the [fund drive](fund-drive)

### Assigned tasks from before

* Aaron: Knight prototype application plans (due by end of January), messaging, illustrations, coding. More coordination with Jim Hazard (maybe)? Other recruiting…
* David: permissions, moderation
* James: progress on Standing Rules, ed-status-quo
* Kim: Research crowdfunding expenses, work with Aaron to determine what tasks might be contracted.
* Mike: WebRTC still

## Agenda
* Check in with assigned tasks from previous meeting, agenda review
* Technical update
* Update: contracting with Joseph Thomas
* NCB status
* General topic: working out priorities for the next month and new year
    * Site moderation getting adequate for more public engagement
    * Tickets, tagging, other organizational parts of site
    * Fake money testing
    * Outstanding needs in prep for initial Board
    * Recruiting more need legal and financial advisors, potentially as steering committee members
    * Following up with Deb Olsen
    * Recruiting representatives from potential Snowdrift projects
* Funding discussion
    * Grant progress
        * Shuttleworth Fellowship (Aaron rejected, form letter; David still waiting…)
        * Ashoka Fellowship (no news)
        * Knight — will apply, figure out how to get more advice about submitting best application
   * Crowdfunding campaign, more finalizing

## Next steps assigned

* Joe, David, Aaron: prepare for fake money testing, project sign-up, simple newsfeed/blog, simple updates to IRC bot, look into @snowdrift.coop e-mail functioning more
* James: Graphic design consultation with sister, look over volunteer opportunities and come up with updated text
* Kim: Move notes for Standing Rules to wiki and work on improving them
* Mike: Review tickets, see what we can do

## Summary / minutes

* Review of progress over the year, discussion of priorities for the future
    * New intern Joe (David's younger brother, funded by David and his mother Kate, learning Haskell and working on stylistic issues)
    * Aaron has been rejected from Shuttleworth, David may still be in the running
    * May want to set up a working @snowdrift.coop email to avoid future work redoing email
* NCB status / Banking situation
    * Now have an account, Aaron is in the process of funding it
    * Will need to transition to Board control as Aaron and David are currently only signers on the account (although it is under the Snowdrift name)
    * Recordkeeping for account necessary even before transitioning to Board control
        * Done through the site?
        * How much do we make transparent? (tension between public verification and exploitability)
    * Need to get an account set up with Balance/Dwolla
        * May be necessary for funddrive purposes
* General topic: working out priorities for the next month and new year
    * Infographic has significantly reduced confusion about how we operate
    * Prioritize someone with graphic/usability design now that the backend is improved?
    * How to Help page may need some work
        * [Targeted messaging](targeted-messaging) improvement? Low priority, that was largely an exercise in how to speak to various groups
    * Site moderation getting adequate for more public engagement (moderate to high priority)
    * Tickets, tagging, other organizational parts of site (as needed for our own immediate purposes, medium to low priority)
        * Trying to mark high-priority tickets for work
        * May be able to close tickets with tags now (needs testing)
    * Archiving discussion threads? Need ability to close threads (moderate to low priority)
    * IRC moderation?
        * May need Aaron to get back on awolf to enable some moderation features
        * May need to designate times for users to be responsible/available for moderation
        * Rule for calling out rudeness in private
        * Do more with IRC bot? Welcome message with link to chat policy on site?
    * Fake money testing
        * How trivial is setting up fake money accounts?
        * Project accounts are set up but not displayed.
        * Accelerated test? Need program to move money. Started but not finished
        * Need to get projects listed, system for setup is not yet complete
    * Site may already be mobile-functional, people should check today
    * Newsfeed or blog for updates needed relatively high priority
    * Outstanding needs in prep for initial Board
        * Need to promote the site more in general, so we can...
        * Recruit more legal and financial advisors, potentially as steering committee members
        * Recruit co-op members in order to have an initial membership meeting
        * Aaron appoints a board from eligible candidates, board approves initial bylaws, members ratify bylaws at initial membership meeting
    * Following up with Deb Olsen
    * Recruiting representatives from potential Snowdrift projects
* Funding discussion
    * Before Board in order to fund legal counsel? Before site development for more effective fund drive? 
    * Grant progress
        * Shuttleworth Fellowship (Aaron rejected, form letter; David still waiting…)
        * Ashoka Fellowship (no news)
        * Knight — will apply, figure out how to get more advice about submitting best application
    * Crowdfunding campaign, more finalizing — will discuss more later
* Suggestion: focus on technical work while we have the labor (fake money (accelerated automatic transfers and project signups) over site navigation, which could be a good volunteer project), de-emphasis promotion beyond handling increasing interest on site and in IRC channel until after project signup is resolved, put legal issues on hold until we really need a Board to operate with real money, work on fund raising on back burner and talk to payment processors once we've verified our system works and then start incorporating their APIs.
