<!-- See end of this pad for meeting-practices notes and reference -->

![](https://pad.degrowth.net/uploads/upload_77445cab95fcc615d82149e0622dd4ab.svg)

Meeting — January 04, 2022
==========================
::: info
- **More information**
 <https://community.snowdrift.coop/t/weekly-all-hands-on-deck-check-in-meetings/1010>
- **Past weeks' notes**
   <https://gitlab.com/snowdrift/governance/-/tree/master/meeting-notes/2021>
- **Facilitation links** (timers and metrics)
   <https://smichel.me/snowdrift/timer/>
:::

### Check-in - 14 minutes
:::	success
- Informal time for socializing (updates on your week..)
- Pad prep (update metrics)
- Ask who would like time for post-meeting discussions.
- Confirm Agenda length & order (including open discussion).
:::

- Attendees: salt, smichel, wolftune, KrisK, MSiep, alignwaivers
    - Leaving early: 
- Facilitator: Salt
- Note Taker: smichel
    - Note-saver/pad-updater (if different):
    
### Minute of silence
<https://www.online-timers.com/timer-1-minute>
::: success
Get ready to be present in the meeting.
:::

### Centering Round
::: success
**What should others know about interacting with you today?**
- Are you present?
- What's distracting you?

::: warning
*Not for a full status update of everything that happened to you this week (unless it's affecting you today)*
:::

Previous Week Review
--------------------
::: success
Facilitator reads/summarizes
:::
### Meeting Feedback & Updates
- **Moved agenda confirmation to pre-meeting**
- Renamed early rounds to reflect their purpose
- Moved this section into previous week review
- Remove headers from next steps (they should stand on their own)

### Metrics

|                                        | Last week | This Week |
|----------------------------------------|-----------|-----------|
| **Meeting attendees**                  | 4         | 6         |
|| 
| **Snowdrift patrons**                  | 142       | 142       |
|| 
| **Matrix:** #snowdrift (lines / words) | .. / ...  | .. / ...  |
|| 
| **Discourse**                          |           |           |
|                                Signups | 0         | 0         |
|                             New Topics | 2         | 1         |
|                                  Posts | 3         | 3         |
|                                DAU/MAU | 26%       | 22%       |
|| 
| **Gitlab**                             |           |           |
|                   Git (commits/people) | 7 / 4     | 1 / 1     |
| Merge Requests (Created/Merged/Closed) | 2 / 1 / 0 | 0 / 0 / 0 |
|               Issues (Opened / Closed) | 0 / 0     | 0 / 0     |


### NEXT STEPs
::: success
Plan how to deal with each incomplete step. 
:::

#### Last Week

#### Earlier (Carried over)
- [ ] smichel: Get more info from hcoop about what would be helpful for them

- [x] ALL: read Aaron's [forum post](https://community.snowdrift.coop/t/thoughts-on-open-collective/977/9) on OpenCollective/FundOSS
- [x] compile list of where we're actually different from OpenCollective/FundOSS
    - <https://gitlab.com/snowdrift/snowdrift/-/issues/241>
- [x] ??? : reach out to OpenCollective — after more internal discussions (with Board?)
    - <https://gitlab.com/snowdrift/snowdrift/-/issues/687>
    
- [ ] ALL: What would it take for you to put in ~full time on Snowdrift for 6 months, dropping all other commitments which would interfere with that?
    - [ ] ?smichel?: Create team-category forum post about this

- [ ] ????: Try out process for moving past stalemate without discussing forever
    - <https://gitlab.com/snowdrift/snowdrift/-/issues/686>
    
- [ ] ???: brainstorm uses of money, then make a list with categorizations of budget categories
    - Create clearing-the-path forum post

- [ ] ALL: please consider how much we all care about being part of the decision-making process (for funds). E.g. would you want to vote on it, delegate the decisions, or something else?

- [ ] Salt: nail down what roles we're recruiting for
- [ ] wolftune, Salt, smichel: Cowork on description for a tech lead / advisor role
    - <https://gitlab.com/snowdrift/snowdrift/-/issues/688>

Agenda
------

### Moving past stalemates in decision-making (Salt) - 5 minutes
GOAL: Identify next steps / assignee for moving this forward
<https://gitlab.com/snowdrift/snowdrift/-/issues/686>

- salt: Smichel came up with a process, iko and I came up with a list. Not sure if there was an explicit decided process but iterating is a good idea
- smichel: It was originally MSieps idea, at some point (described int hte issue)
- wolftune: there should be a discussion and consensus about how to do that, and discussion often happens without framing
- salt: Question is, how weill we move past it? WHat's the next step?
- wolftune: we have the list. I think the next step / decision to make is:
- wolftune: What fallback mechanism are we using?
- smichel: the question is what item do we apply this to first?
- salt: we have 4 items
- smichel: if the majority wants something and the rest don't care, we can just fallback to that, no
- Salt: I htink this is related to the CommunityRule— having an explicit process is important
- wolftune: Everyone has to know at consensus time, if someone very objecting, they have to know there is a fallback rule and they may be overruled
- [ ] Salt: Write up the process, community-rule style
- smichel: I want to push back on this, we haven't even tried polling people to see if they care. 
- wolftune: We just need to have a fallback no matter what, because we can't allow lack of consensus to prevent moving forward etc
- smichel: I don't care enough to belabor the point here.
- Salt: Need to decide which 
- KrisK: Are these specific individuals assigned / with accountability for carrying forward to an action item or are these democratic decisions related to the whole organization and not individual accountability
- Salt: More the latter, things that would impact the organization.
    - That's why it's challenging— we want to have a process that's clear up front, but also want to keep us moving.

- [ ] smichel: Decide which item to try out
- [ ] wolftune: plan and run the process

### How much do you want to be part of the decision-making process for funds? (smichel) - 5-10 minutes
GOAL: Get people's thoughts (round)

- smichel: a month ago I asked people about this (how much they wanted to be involved, be able to veto or vote), thought we could do a round and see what people have to say. 


ROUND:
- wolftune: factor for how big of a decision? Little vs lots.
    - I don't care how much I'm involved if it's lots of little decisions
- Kris: From other consensus-based org I was part of (disaster relief), it's better to spread out your expenditures, invest them in different kinds of labor/resources than one big lump sum on one thing. Allows more flexibility.
  - Given democratic decision-making, maybe some participatory/proportional budgeting. E.g. $600 to spend with six people, each person votes for how to spend $100 of it.
  - Could have delegated votes
- align: I'd like input when I'm around, but don't want decisions to be delayed based on my response times. 
- Salt: I like being involved at the high level, budgeting stage. We have some money, vision on what it gets spent on. Day to day small amounts, don't need to be that involved. Like the idea of participatory budgeting.
- MSiep: What others said made sense to me. If it's a big thing I want to be involved, but for small things I don't want to hold anything up. Haven't had many examples of this so far so we could iterate on. 
    - people can have default preference, adjust w/ real world examples. I'd like to start by defaulting to being more involved
- smichel: I echo what others said, I think I'd like to be slightly more involved involved. Would like to be notified when there is money involved after the fact
    - Salt: You bring up the timing issue which is relevant, 24 - 48 hrs in advance of the money being spent, someone could jump in and object etc.
    - wolftune: this reminds me of something I proposed long ago, a transparent reporting of finances as part of project accountability. Connects to this
- Salt: this topic and the last one: it comes down to the issues we want to solve and we need to solve them for ourselves.
- Wolftune: we wanted to use this conversation in order to make the moves to request more money, is this correct? would like to make sure this is captured

### Piloting crowdmatching (MSiep) - 5-10 minutes

#### Context

A couple weeks ago, a few of us (Salt, athan, me) were talking about getting un-stuck. I realized I hadn't shared the thought of how much work we're putting in to creating infrastructure for a system we haven't tested in the real world to see if it works. Wouldn't it be great if we could pilot test the concept of crowdmatching without having to create this organization & software to make it work. Some of this you could manually do in a spreadsheet, for example.

One idea: find an org that already has a crowd of monthly donors, propose that they can join crowdmatching; present it as a pilot. Then you could invite people who are not yet donors to join. Would just need some messaging, and a landing page to opt in to crowdmatching, which could be done behind the scenes.

I first thought of QubesOS, and drafted an example email, which I posted on the team section of the forum

#### Discussion

- Kris: I think that's a great idea, I just want to propose a way that could be done very quickly (what prompted me to join this meeting in the first place)

    - I was looking at Kiva (micro-lending system) and loans in washington state, there are 4 active loans there. I have some money from pandemic assistance that I'd like to invest in something with a return. If I make loans on Kiva, even if there's not a cash payback, supporting something would be better than losing it to inflation
    - could make a Forum thread, kiva allows people to join "teams"
- Salt: this type of piloting has always been one of the initial ideas, we haven't gotten to it.
    - One tension: when Tanner was making graphs, we could visualize crowdmatching.. ??? ... we have a backend that works
    - where does snowdrift then fit in to it? Doing it all through a project website might not be the endgoal???
- wolftune: My view from the beginning is the proof of concept: get some behavioral psychologist do some studies etc. to validate this. I did inquire with Dan Arielle at some point but based on everything they know and previous research, the response was "yes, this sounds like a totally fantastic idea. Good luck!"
- smichel: it does start to get trickier with the spreadsheet approach. Since we have 143 patrons with just simple word of mouth, that number will increase greatly and become harder to manage on a spreadsheet
- Krisk: I want to commend the whole approach and work that's been done on the software platform. But right now you have a bunch of people who want to invest in the community. Doesn't have to be most organized or perfect, but with the people you have here right now, you could make a difference.
- Aaron: we've said, "it's more important that we succeed politically than it is to succeed technically." (if we fail technically, it's still a success overall)
    - When we talk to people about this, the most similar thing to what we're trying to do is a one-off donation on reddit, etc. That's already happened, here and there. I don't think there's doubt about whether the concept is viable & worth exploring. It's more about whether it will succeed at the scale we want it to happen at.
- MSiep: Let's say we knew the concept would work, there's the question of getting it off the ground: the thing we lack is a big community of people to introduce it to.
    - If you have an org that already *has* thousands of monthsly dononrs. Even if you know it's going to work, what you need is a compelling story in the real world that will get people to adopt the idea. 140 patrons who know what snowdrift is, isn't that.
- Salt: we have been planning on doing this, getting Qubes on that list is a good idea. If we can get someone on one of these larger orgs on board, I think we are into the idea of piloting the crowdmatching concept. I didn't realize this until more recently, crowdmatching inherently *slowly* builds trust over time (with donations)
- Wolftune: Has been in my mind the whole time. I'll recognize that as the need to emphasize it more. I love what MSiep drafted - what I like is the premise of open-collective which already has the resources, shifting the operations there etc.
    - In 2014 we setup an email prototype thing with test money, we got 1200 people to sign up really quick. If we **just** did this with snowdrift itself advocate to folks "help us test out this crowdmatching idea", I think we could get 5000 people in a month

- [ ] Continue discussing at the next meeting

### Aaron brain dump (smichel) - 5 minutes

GOAL: List TOPICS that Aaron wants to discuss

### Open Discussion (Facilitator) - 5 minutes


### Round for anyone who hasn't spoken yet (Facilitator)



Wrap-up - 5 minutes
-------------------
### Minute of Silence
https://www.online-timers.com/timer-1-minute
::: success
**Think about (and perhaps write out) your sign-out.** *Short, concrete, & specific:*
- How much will you be around this week?
- What will you focus on? (and does anything need capturing/scheduling?)
- What can others do to help you succeed?
- What meeting appreciations and/or suggestions do you have?
:::

### Sign-out round


- salt: could be fairly around today, thursday, and friday but have other things vying for time. Focus?? Not sure if Civi is or isn't done, OSUOSL has made some progress. Had some items on my docket before all the, roles and decisionmaking stuff. Think things went pretty well and appreciate new voices, time boxing could be done better
- smichel: Agenda in advance didn't work so well. Topics themselves went really well. Goals had good outcomes and it worked out fine. I have a job starting on the 18th, packing and moving (not all at once) but plan to spend some time on snowdrift.
- wolftune: When I was hearing dealing with backlog stuff and carryovers, it sounded like a really well organized and focused group, Salt did a good job of facilitating, flexible but on track. Questioning generally if/when we might want to do breakouts/1 on 1s, and hope people would use the forum more because getting out all of our thoughts in real time is difficult.
- KrisK: (had to leave)
- MSiep: My day job starts up again Thursday, I was almost affected by wildfire. Currently not back there yet, availability should still be normal.
- alignwaivers: Thought it was a great meeting overall, thanks for facilitation Salt and everyone's input and passion, appreciate feeling some momentum from this conversation.

### Meeting Adjourned!
::: success
- Check that all NEXT-STEPS have someone assigned
- Move to the "coffee shop"
:::

Post Meeting Discussion
-----------------------


### Next-steps capturing

<!--
Personal Scratch Pad
Salt
- computing for social good class project
    - https://pad.snowdrift.coop/p/role-development
- need to update slide deck, would be nice to have a short/long version ready to go for anyone
    - 5-min version (newer): https://seafile.test.snowdrift.coop/d/6861aa006f4e441da72a/?p=%2Fpromotional%2Fslide-deck&mode=list
    - longer version (old): https://seafile.test.snowdrift.coop/d/130a3bf5e9ab455bbc2e/?p=%2Fpromotional%2Fslide-deck-kit&mode=list
- work with athan on multiple versions of funding request but modified for researchers?
- civi... OSUOSL has been asking us for an update
    - CiviCRM 9.2.1 installation process test completed: https://pad.snowdrift.coop/p/civicrm-deploy

Meeting practices and tools
Pre-Meeting Tasks
- Update metrics, from these links:
    - https://snowdrift.coop/p/snowdrift
    - https://community.snowdrift.coop/admin?period=weekly
    - https://gitlab.com/groups/snowdrift/-/contribution_analytics
- Add items to the agenda. Include a timebox & GOAL(s). Decide on order.

Post-Meeting Tasks
- Clean up meeting notes
- Save them: https://gitlab.com/snowdrift/governance/-/tree/master/meeting-notes/2021
- Prepare this pad for next meeting:
    * Replace previous meeting eval notes with new, filtered down to essential points
        - Leave re-writing for meeting-prep, but do remove routine "thanks all" comments & next-ups
    * Clear previous-week metrics; update this week's attendee count if anyone arrived late
    * Clear the "NEXT STEPs Review" section, unless they have next-steps which are not done/captured.
    * Move new NEXT STEPs to that review section and then clear out other notes areas:
        - The snowshoveling check-in notes
        - Agenda items
            * Keep the topic title (but not topic leader/time) with the NEXT STEPs to review
            * Open discussion notes
    * Clear authorship colors
    * Update next meetings date

Location
- Normal: Mumble server: mumble.test.snowdrift.coop (leave port blank, or 64738) (mobile: half-duplex mode avoids echo)
- If we do video/screenshare: https://bigblue.cccfr.de/b/mra-qy2-x6n or https://meet.jit.si/snowdrift



Open Discussion
- Facilitator: make sure everyone speaks & that all outstanding NEXT STEPs are mentioned

Etherpad use
- Use chat in etherpad (and add your name)
- Option: audio notifications on firefox via https://addons.mozilla.org/en-US/firefox/addon/notification-sound/

Agenda topics
- As needed, ping !folks on matrix to read over anything advance, ideally before the day of the meeting
- Each topic facilitated by topic lead (?)

Timeboxing
- Prefer 5 minute increments. Leave time for Wrap-Up.
- Timebox each topic, settled during agenda confirmation
- At timebox end, facilitator may choose to extend by a specific amount
- Informal "thumb polls" inform the decision for extra timebox
    - "^" approve, extend the timebox
    - "v" disagree, move onto the next topic
    - "." neutral
- Use https://online-timers.com, start for each item, paste each url in the chat
- Quick links for here: https://smichel.me/snowdrift/timer/

NEXT STEPS
- Each topic should capture NEXT STEPS (or clarify that there aren't any)
- Should be clear and actionable
- Assignee agrees to capture or complete by next week

Discussion options
- Open discussion
- Call for a round ("pass the mic" style, facilitator makes sure no one is skipped)
- Hand symbols
    - "o/" or "/" means you have something to say and puts you in the queue
    - "c/" or "?" means you have a clarifying question and jumps you to the top of the queue
    - "d" means thumbs up, encouragement, agreement, etc.
    - ">" means you understand someone's point, please move on
    - "d>" indicates feeling complete on an agenda item, ready to move on to

Notetaking
- "???" in notes means something missed, please help capturing what was said
- Aim for shorthand / summary / key points (not transcript)
- Don't type on the same line as another notetaker (ok to do helpful edits after but not interfering with current typing)
    - The "suggest changes" feature can help make edits without interfering
<!---->
