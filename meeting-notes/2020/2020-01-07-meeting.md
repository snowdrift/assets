<!-- Previous week's meeting notes: https://wiki.snowdrift.coop/resources/meetings/2020/-->
<!-- Meeting location: Mumble server; backup option for video and screenshare: https://meet.jit.si/snowdrift -->

# Meeting — January 6, 2020

Attendees: mray, wolftune, salt, alignwaivers, msiep

<!-- Check-in round -->

<!-- Assign note taker -->

## Metrics

Discourse (over past week): <!-- stats from https://community.snowdrift.coop/admin?period=weekly -->

- Signups: 0 -> 0
- New Topics: 3 -> 4
- Posts: 13 -> 24

## Reminders on best-practice habits

### Sociocracy stuff
- Drivers, describing driver statements: https://patterns.sociocracy30.org/describe-organizational-drivers.html
    - Current situation + effect + what's needed + impact of doing it
    - forum discussion of our use: https://community.snowdrift.coop/t/sociocracy-3-0-drivers/1309
    - Objections vs concerns
    - Example of proposal phase: https://community.snowdrift.coop/t/sociocracy-proposal-phase/1374

### Use of GitLab kanban boards
- Use the boards to pick your tasks, dragging or marking To Do and Doing appropriately
- e.g. wolftune's: https://community.snowdrift.coop/t/using-the-kanban-boards-in-gitlab-plus-wolftune-check-in/1263

### Conversation queuing
- "o/" or "/" means that you have something to say and want to be put in the queue
- "c/" or "?" means that you have a clarifying question and want to jump to the top of the queue
- "d" means thumbs up, encouragement, agreement, etc.
-  ">" as an indicator of understanding someone and the point can be concluded, please move on
- three discussion mechanisms: hand symbols (above), call for a round, open discussion

### Notetaking
- "???" on the etherpad means the notetaker missed something and needs assistance capturing what was said
- aim for shorthand / summary / key points (not transcript)

## Review previous meeting feedback
- alignwaivers: maybe we could have a symbol or way to emphasize to note-taker/secretary if they missed or mistook something
- alignwaivers: another situation, when someone gets nominated for something and a symbol for the nominated person to accept?
- alignwaivers: maybe more often NEXT STEPS can be just capturing the tension / driver rather than already have a proposed action
- iko: good meeting, I do appreciate the open chat time
- mray: good meeting, good to make it on time. I'd appreciate it if we could somehow increase the call quality, don't know if it's compression or bad microphones but sometimes its really hard to follow what people are saying, could be a language barrier but the meeting could benefit from having clearer audio
- wolftune: overall good meeting. I like what were doing to refine it, thanks to alignwaivers for taking wonderful notes. one thing, I'd like to see more of shorthand, we don't need a dictation, we just want to capture the gist of the things people say.
- salt: just feel like we spent longer on some things that wasn't necessitated, a little annoyed by topics that didn't have next steps and no trace of the author
- salt: maybe put names next to agenda items, who claimed or brought it up

## Review last meeting's action steps
<!-- Verify that each item is captured or done. For any inadequately captured or needing additional bump or follow-up, make a new agenda item for this meeting -->

```
Note: we cleared this out this time, but it will stay as last-meeting's-steps after this

```
<!-- Confirm agenda order, inform if leaving early so as to not interrupt -->

<!-- New Agenda Down Here  -->

## Mumble server public / wiki / on pad? (wolftune)
- wolftune: if things are put in HTML comments they won't be rendered on the wiki page, (but they will be on the backend) is that enough?
- NEXT STEP (wolftune): check with iko

## Advanced agenda planning & coworking time for meetings (alignwaivers)
- trial and error this time, forum vs pad; thoughts?
-wolftune: round on pad vs forum or other ways to collect agenda
- mray: pad
- wolftune: pad, doesn't feel like it needs to be captured in a more permanent place
- salt: as long as pad is clearly linked, but maybe use the forum and/or IRC etc to *discuss* agenda items that aren't even clear enough or sure about putting on
- alignwaivers: pad
- msiep: pad
- NEXT STEP (alignwaivers): make this clear in appropriate places, use the pad for adding agenda, and notifying people to review agenda with couple day advanced notice
- alignwaivers: for questionable items that may need to be discussed, follow up on forum?
- salt: ideally people check in
- wolftune: posting a couple days in advance what the agenda is, how do we prompt everyone to review and prepare?

## Shared Calendar / Automated Reminders (alignwaivers)
- alignwaivers: what calendaring do we all use that we could coordinate with somehow?
- salt: there are standards that should usually all be interoperable
- wolftune: there's a calendar plugin but not sure of the features
- NEXT STEPS (alignwaivers): review previous past discussion and resources (for available / ideal tech - check discourse plugin as well)

## State of OSU OSL transition (Salt)
- access to GitHub updates?
- wolftune: nothing to do on GitHub directly, we engage via Trello and email; pinging OSU has seemed to be helpful for prompting progress

## Update meeting time? (Salt)
- Salt: so far this slot still works for me, but the quarter seems like a decent time to update everyone's availability
- alignwaivers: made a post about this 13 days ago https://community.snowdrift.coop/t/team-availability-and-weekly-meeting-details/1345/22
- salt: date by which this should be done? - by next meeting
- salt: and get people involved who may not have attended meetings , and announce new meeting 
- NEXT STEP (alignwaivers pings on this): decide by next week about continuing time; everyone get your times up to date (including those not currently attending)

## LFNW call for presenters, booth plans, etc (wolftune)
- https://lfnw.org/conferences/2020/program/proposals/new
- wolftune: we have a table, they don't currently have a whole lot of talk submissions, could make it a milestone
- salt: will put in talks
- salt: would like to propose a more snowdrift-focused talk
- talks are due in 9 days for LFNW!
- NEXT STEP: discussion thread https://community.snowdrift.coop/t/linuxfest-nw-bellingham-wa-april-24-26-2020/
- NEXT STEP (salt): get in proposals, including a Snowdrift-focused one (at least as stub)

## BoD, meeting, support, transparency / updates (wolftune)
- wolftune: met very little over last year, chair stepped down after startup trouble, now at top of my priority list, tentative new board member, setting date for a first board meeting of the year
- wolftune: would be nice to have a note taker separate from the board
- NEXT STEP (wolftune): schedule board meeting 
- alignwaivers could be available to help with notes
- wolftune: feel free to reach out if there are any questions about board stuff

## Marking prominently that we're under-construction (carry-over)
- NEXT STEP (smichel17): Privately reach out to h30, make sure to mention the 'under construction' site banner issue
- mray: mockup is finished, but not clear who it got handed off to. MSiep/iko moved issue to the code repo for implementation
- mray: it was allocated elsewere
- NEXT STEP (alignwaivers):  check in with smichel17 on this

## Code development progress / site building (carry-over)
- NEXT STEP (chreekat): deploy updated site, then after the updated site is deployed, run the backlog of crowdmatches https://gitlab.com/snowdrift/snowdrift/issues/169
- NEXT STEP (alignwaivers): ping chreekrat on this

## Issue grooming / dumping etc (carry-over)
- NEXT STEP (align_waivers): write first draft of a grooming-guide
- NEXT STEP (alignwaivers ping smichel17): Delete or close the LFNW 2019 milestone <https://gitlab.com/groups/snowdrift/-/issues?milestone_title=LFNW+2019>
- NEXT STEP (alignwaivers + smichel + wolftune + msiep): schedule grooming session
- NEXT STEP (msiep): groom design repo issues
- NEXT STEPS (alignwaivers): groom labels (planning this, forum post, issue for this), read https://gitlab.com/help/user/project/labels.md
- alignwaivers: really appreciate the in-depth postings recently from shak-mar / photm
- there's some distinction between grooming and defining good issues and when to have issues in general (and what a good issue looks like) , lots of overlap though, maybe this is all of that
- wolfune: lots of resources, e.g. from GitLab handbook, start with merge request sometimes; issue is for when things aren't clear. We need a starting point to guide people who may not understand next steps
- wolftune: we should define maintenance responsibilities for roles, e.g. a role responsible for grooming issues within a repo
- wolftune: should there be something about better defining responsibility
- NEXT STEP (wolftune): add note about issue management to plans for role updates etc

## Volunteer recruiting application (carry-over)
- msiep: Wireframe of snowdrift.coop/volunteer-application (or wherever the form will live), potentially with second stage "fill out profile" option (see meeting notes 
https://wiki.snowdrift.coop/resources/meetings/2019/2019-11-18-meeting#volunteer-role-recruiting-etc )
https://gitlab.com/snowdrift/outreach/issues/28
- msiep: any more feedback? otherwise, just waiting for Civi perhaps for implementation
- NEXT STEP (salt): continue to monitor state of civicrm

## State of pad vs when captured on wiki (wolftune)
- wolftune: cleaning up carry-overs etc.
- wolftune: prevoiusly has cleaned up notes, for instance the ##issue grooming above which is messy has lots of carried over next steps, wants to end up with notes in good shapes
- salt: sees a tension because carryovers are moved over ???
- wolftune: capture meeting notes and then put carryovers from today into carryovers from next, that state never gets captured on the wiki which is maybe fine
- salt: in favor slightly of going through the carryovers and then delete (but not during meeting)
- wolftune: when we say the thing is done we mark it as such (without deleting) and make a new agenda item that references it
- NEXT STEP (wolftune): capture / codify this idea (don't move carry-overs, but make new agenda referencing it) in the pad notes (maybe also on wiki)
- each meeting starts with review-of-past-meeting-next-steps
- include the checking in with everyone on having their issues captured
- NEXT STEP (alignwaivers): take on role in future meetings (after process captured)

<!-- Open discussion? ~5min. if time -->

---

## meeting evaluation / feedback / suggestions round
- salt:  took too long, 15 minutes to get to actual agenda today
- mray: went well, salt facilitated well
- wolftune: liked stephens '>' symbol
- salt: like advanced agenda planning
- alignwaivers: workflow is getting better, valueable use of time
- mseip: good meeting

<!-- Goodbye round -->
<!-- Capture NEXT STEPS -->
<!-- Clean up meeting notes, then add to wiki -->