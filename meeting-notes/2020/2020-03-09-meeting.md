<!-- Previous week's meeting notes: https://wiki.snowdrift.coop/resources/meetings/2020/ -->
<!-- Meeting location: Mumble server: snowdrift.sylphs.net port 8008; backup option for video and screenshare: https://meet.jit.si/snowdrift -->
<!-- Group chat usage on bottom right of this page, please update username and choose color in the top right of this page -->
<!-- Bookmarklet to make the chat bar wider. Create a new bookmark with the below. You can adjust the width by changing "280" to whatever you want, in pixels.
javascript:(function () { const width='280'; const box = document.querySelector('div#chatbox'); if (box) { box.style.cssText=box.style.cssText+' width: '+width+'px !important;'; } const pad = document.querySelector('iframe').contentWindow.document.querySelector('iframe').contentWindow.document.querySelector('body#innerdocbody.innerdocbody'); if (pad) { pad.style.width=(document.body.clientWidth-width-50)+"px"; } })();
-->
 
# Meeting — March 9, 2020
 
Attendees from last week: Salt, alignwaivers, Msiep, Mray, wolftune
Attendees (current): msiep, wolftune, smichel, Salt, alignwaivers, mray
 
<!-- Check-in round -->
 
<!-- Assign note taker -->
 
## Metrics
 
Discourse (over past week): <!-- stats from https://community.snowdrift.coop/admin?period=weekly -->
 
- Signups: 0 -> 0
- New Topics: 4 -> 3
- Posts: 11 -> 8
- DAU/MAU:  33%

Snowdrift patrons: 118 <!-- to be reviewed occasionally from https://snowdrift.coop/p/snowdrift -->
 
## Reminders on best-practice meeting habits
- Review previous meeting notes especially when absent!
- Audio notifications for etherpad chat available on firefox via https://addons.mozilla.org/en-US/firefox/addon/notification-sound/
 
### Use chat in etherpad (and add your name)

### Conversation queuing
- "o/" or "/" means that you have something to say and want to be put in the queue
- "c/" or "?" means that you have a clarifying question and want to jump to the top of the queue
- "d" means thumbs up, encouragement, agreement, etc.
-  ">" as an indicator of understanding someone and the point can be concluded, please move on
- three discussion mechanisms: hand symbols (above), call for a round (and vary the order), open discussion
 
### Notetaking
- "???" on the etherpad means the notetaker missed something and needs assistance capturing what was said
- aim for shorthand / summary / key points (not transcript)
 
## Review previous meeting feedback

- msiep: meeting seemed good
- smichel: good, short meeting
- mray: good short meeting
- wolftune: feels like the meeting is good for resolving tensions, could be done more explicitly, asking everyone to notice tensions and help each other understand them and turn them into concrete drivers
- salt: pretty good, still finding the balance of structure and lack thereof


## Last meeting next-steps review

### CiviCRM (Salt)
- deployed! getting configured, hopefully going to be schema matched so previous work transfers appropriately, going to find some time to work on it (by libreplanet)
- NEXT STEP (salt w/ mray and iko): get volunteer form working and looking good
- NEXT STEP  (salt) : put items (confirm availability for email, email headers setup, 403/404 pages needed) into gitlab

### SeaGL (Salt)
- NEXT STEP (alignwaivers): Make sure this is on the snowdrift calendar
- NEXT STEP (salt): write two user stories, for a conference and individual (respectively) being funded by Snowdrift.coop

### OSU Priorities (Salt)
- NEXT STEP (wolftune): ping them about main site as next priority
- NEXT STEP (smichel17): make sure nextcloud instance is up to date & non-expired ssl cert xD

### LibrePlanet blog post (wolftune)
- NEXT STEP (smichel17) : make a blog post

### OSI incubation situation and elections (wolftune)
- NEXT STEP (wolftune): discuss this with the Board
- NEXT STEP (wolftune): consider whether to post on Discourse (privately probably)


 
<!-- Confirm agenda order, inform if leaving early so as to not interrupt -->

## Current Agenda

<!-- New Agenda Down Here  (Added 48 hours before the meeting or earlier)-->




<!-- Late New Agenda Down Here (Added within 48 hours of the meeting)-->


### New metric: DAU/MAU? (smichel17)
- smichel: Discourse shares the ratio of daily active users to monthly active users, as an indicator of community health (want >30%), seems like it might be good to track?
 salt: Seems reasonable.
- NEXT STEPS (alignwaivers) add to pad

### Surfacing tensions (wolftune)
- wolftune: social assisting of one another to check in with ourselves, structured way of bringing up tensions (a formal round if we have time)
- Salt: Could you clarify? 
- wolftune: sometimes get to a meeting and captured discussion topics, but possibly substantial things not being addressed / acknowledged. A person may not have fully processed (enough to put as agenda item) but have things to discuss
- Salt: What do you want from this meeting?
- wolftune: I've found it helpful to bring up topics I was hesitant to bring up, because I didn't have a clear agenda in my mind — it's produced useful discussion.
- wolftune: tentative proposal as time (before or after feedback round) to bring up potential tensions
- smichel: good to implement during unstructured round
- wolftune: and find if there's things have gone unspoken, allow this as specific time to bring up
- Salt: Not sure there is value in adding additional structure, maybe better to leave it part of the open discussion (maybe implement as part of explicit instructions for open discussion)
- wolftune: facilitator could say "let's check in about any possible tensions"
- smichel: If we wanted to make it more formal, we could do a round at the beginning (part of checkin?), and turn any tensions into agenda items
- salt: still dont see benefit of these being a formally different section, not sure how it fits in to being a facilitator tool vs part of formal structure
- wolftune: good example: this very topic! I didn't come with a clear proposal, and didn't feel like it was fully addressed and by discussing, became more clear: initiating a prompt for tensions to come up with new topics (and discuss if there's time)
- wolftune: proposal: After personal check-in (during agenda-building), 
- wolftune take a moment to check in with how you're feeling, give a minute of silence, and return to meeting/agenda
- salt : sounds fine to give it a shot. This would be similar to adding new agenda items to the end of the agenda
- smichel: this might benefit from being an explicit round. even if people pass, vocalizing that is good, using an explicit turn for each person, easier to bring up things 
- wolftune: +1, can be part of agenda round:
- Salt: So, after review, before agenda building?
- NEXT STEP (wolftune): include instruction for a 1-minute silence, review agenda, surface tensions, round for confirming thoughts

### Conference/Individual funding user stories (Salt)
- salt: want to clarify this from last time (potential of seagl as a snowdrift project) . Was asking for creation of 2 user stories for those types of funded projects (but wasn't captured)
- wolftune: still wanting to move more toward sociocracy practices
- smichel: I think Salt should write the user stories (or at least their draft) :P
- Salt: I can open an issue, but am way too overloaded to write a draft
- wolftune: Maybe better fit for forum discussion than gitlab issue?
- Salt: The task is, "Write the user stories"
- wolftune: do we have all the info necessary for the user story or do we need to discuss
- Salt: I think we ahve all the info.
- Salt: Does someone have the link to our existing user stories?
- wolftune: we don't really have a repo, there's some documentation of user stories (in top level directory of main snowdrift code)
- https://github.com/snowdriftcoop/snowdrift/blob/master/SPECS-STORIES.md
- smichel: since salt seems to have this best formulated, that's why I suggest he write it (when he can)
- salt: the user stories here aren't for projects
- wolftune: if we have user stories they would be in the project repo
- salt: as a project, I come and do this - we don't have those specs
- wolftune: we don't have user stories for projects because they reflect what's on the site and the site doesn't have workflow for projects signing up yet
- salt: user stories for people visiting the site, but not for projects. I thought the projects workflow was figured out
- msiep: I dont remember creating this but would make sense for us to have it
- mray: since we don't have a plan of a substantial number of proposed projects immediately, maybe a bit premature to do this
- wolftune: could imagine something a little more general about how we engage with projects (of vice versa)
- salt: agree. I also think projects come to our site with interest in getting funding and thats a specs-story
- wolftune: they should know they cant sign up right now, but also how to contact us
- Salt: yes. that clarification of not being able to is part of what I'm getting to.
- NEXT STEP (miep): capture gitlab issue for writing User Stories for SPECS-STORIES.md to go along with the project-page and site-status implementations (add link (update project pages and that the site is a work in progress should both be user storeies)

"Interested project comes to site, sees that there is no automatic way to sign up"


### Libreplanet (Salt & smichel17)
- Salt: LP just got cancelled (got volunteer email during this very meeting), will be online-only.
- Salt: should still have an online presence)
- wolftune: how does conference operate online only?
- Salt: I was asked as a speaker, if I can present remotely, so that's probably what will happen. Don't know which platform, it'll be interesting to see what happens.
- mray: nice stress test for free software
- salt: Talks will be at <https://libreplanet.org/2020/live>
- smichel17: We can have BoF in matrix/irc
- NEXT STEPS (salt): update our LP forum event post

### OSI incubation (wolftune)
- wolftune: had meeting with Elana, disappointed that I didn't get much engagement from Snowdrift Board so I just did it myself
- wolftune: she got enough to bea able to go to the (OSI) board with details for snowdrift - gonna update the snowdrift wiki page on opensource.org.
- wolftune: General idea that despite all challenges, need to move forward (check-in at 6 months). If we're not making progress in about a year from now, we would discuss winding down as an incubator project)
- smichel: I think we should err on the side of meetings (and maybe email?) for board stuff rather than forum.
- Salt: On boards I've been on / boards in general, engagement has been… lower
- wolftune: In first board meeting, there was a lot of engagement, so this surprised me, is all.
- salt: how is progress defined in the context?
- wolftune: that's what I want the board's help with (more concrete plan). Alanna wants a more concrete roadmap (eg, gitlab issues + milestones) at the very least, defining the current status. Ex: civicrm & volunteer stuff
- wolftune: Means, we have set goals on what we want to accomplish in 6-12 months, and then if we're not accomplishing it, that's the problem. Progress defined by us, our responsibility is to update OSI 
- Salt: I think details are important when we have what sounds like an ultimatum.
- wolftune: example of failure: we set out to reach out to people in our backlog to recruit, but we didn't do any recruiting
- NEXT STEPS (wolftune/board): share the updates etc. and tasks for everyone's reference

### CiviCRM volunteer form etc. (salt)
- NEXT STEP (salt): discuss this next time
- salt: been working on volunteer form with iko. Deeply invested in school at the moment. Working with how to deal with OSUOSL. working well except the thing we used for web forms is still in development on porting to Drupal , not yet working well
- what should be the landing page? (it's mainly for the team logging in, not for public)
- mray: doesn't matter much for just internal use, defaults are fine
- what should be the landing at contacts.snowdrift.coop/volunteer (the place for volunteers to find the volunteer form to fill out)
- https://gitlab.com/snowdrift/outreach/-/issues/28
- https://contacts.snowdrift.coop/ - primarily for team using civicrm, but may be found by others
- https://contacts.snowdrift.coop/volunteer - primarily directed to, which form do we present them with first? 1 in gitlab?
- msiep: we should include in the volunteer landing making sure they know how to get to other resources if they get their not to fill out the form
- msiep: the top level page should at least link to /volunteer
- smichel: Paraphrased:"Were you looking for the volunteer signup form? [[Click here]]"
- salt: just thinking what's the process for getting this settled
- msiep: I can do a wireframe; I want to see what the default civi homepage looks like first.
- salt: it's a basic page with login prompt. Drupal default.
- msiep: okay, so we just need to make it clear that most people aren't supposed to log in there
- NEXT STEP (msiep): capture task to make basic wireframes for these landing pages

<!-- Open discussion? ~5min. if time -->
- salt: think it went pretty well, a lot of coverage - good balance of unstructured vs structured (maybe a little too unstructured at times)
- smichel: thought balance was good, the point where I got cut off I was fine with (for it to be that unstructured)
- wolftune: good meeting, feel free to cut me off more, I'm happy to be cut off and have the chance to insist on saying more when necessary

---
 
## meeting evaluation / feedback / suggestions round


<!-- Goodbye round -->
<!-- Capture NEXT STEPS -->
<!-- Clean up meeting notes, then add to wiki -->
<!-- Prepare this pad for next meeting: (A) replace previous meeting eval notes with new (B) Clear discussion notes, moving NEXT STEPs to "review last meeting's action steps" (C) Update next meetings date, clear attendee list  (D) Update old metrics, update date, leave new blank -->

