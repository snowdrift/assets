<!--

    Previous weeks' meeting notes: https://wiki.snowdrift.coop/resources/meetings/2020/

    Meeting location: Mumble server: snowdrift.sylphs.net port 8008 (half-duplex mode on mobile: avoids echo)

    Alternates for video/screenshare: https://bigblue.cccfr.de/b/mra-qy2-x6n or https://meet.jit.si/snowdrift

    See end of this pad for meeting-practices notes and reference

-->
 
Meeting — September 23, 2020
===========================

- Attendees from last week: Salt, MSiep, smichel, davidak, wolftune
- Attendees (current): wolftune, MSiep, smichel, Salt, alignwaivers

<!--

    Personal check-in round

    Assign facilitator

    Assign note taker

    Assign time keeper, use https://online-timers.com, start for each item, paste each url in the chat

-->

1 Metrics
--------

Discourse (over past week): <!-- from https://community.snowdrift.coop/admin?period=weekly -->
 
- Signups: 0 -> 0
- New Topics: 0 -> 1
- Posts: 0 -> 1
- DAU/MAU:  28% -> 29%

Snowdrift patrons: 126 <!-- from https://snowdrift.coop/p/snowdrift -->

2 Review previous meeting feedback
-------------------------------------------

- wolftune: facilitator can also suggest cutting timeboxes early as appropriate


3 Last meeting next-steps review
----------------------------------------

<!-- Mark NEXT STEPs as [DONE] or [CAPTURED] <LINK> by the next meeting -->

- NEXT STEP (wolftune): update timebox notes to defer to facilitator [DONE]

4 Snow-shoveling check-in round
-----------------------------------------
<!--

    One minute silence — check thoughts and notes/tasks/emails to surface tensions

    Quick answers:

    what is your current focus?

    are you blocked on anything?

    what help could you use?

    Add to agenda or breakout room topics as needed

-->
- smichel: Bylaws focus, low progress due to non-snowdrift focus; once I have more time, frontend dev focus; coworking on bylaws would be helpful
- Salt: civi focus, still blocked on osuosl update, don't think many oppty for help
- wolftune: focus: update directors/board for annual Michigan report; uncomfortable but nobody external has given push-back. Will need confirmation from board on who gets what titles. No blockers, but civi would help.
- msiep: focus on crowdmatching approach, no blockers
- align: Busy w/ other things, focus is on snowdrift funding pitch when I have time.

5 New Agenda
------------------

<!--

    Confirm agenda order and timeboxes (in 5 minute increments), adjust for anyone who must leave early

### TOPIC TEMPLATE (LEADER) <!-- Timebox: # minutes -->
<!-- -->

### Options for next steps for new approach to crowdmatching (MSiep) <!-- Timebox: 1 minute -->
- msiep: I posted on the forum about options for new approach. Too much to discuss today (not enough time to process), please everyone read it and maybe we can discuss next week.
- wolftune: How do you feel about forum discussion vs meeting?
- msiep: Whatever works best. I said in the post, mainly planning to discuss in meeting, and forum
- NEXT STEP (everyone): read forum post <https://community.snowdrift.coop/t/options-for-next-step-for-new-approach-to-crowdmatching/1608>, reply there or be prepared/plan to discuss next week


### Civi Update and Temp Spreadsheet Info (Salt) <!-- Timebox: 1 minutes -->
- Salt: current issue is this update, waiting on OSUOSL / figuring out what the issue is 
- Salt: low motivation to work on something this broken
- Salt: close to proposing throwing away config, using new install
- Salt: we decided to use a spreadsheet in the meantime; it's on nextcloud
- Salt: new spreadsheet may feel like it's based on volunteer form (it has same fields), but those can be left blank
- smichel: Is it currently on nextcloud or needs to be uploaded?
- Salt: the latter
- NEXT STEPS (Salt): Upload spreadsheet to nextcloud
- smichel: Nextcloud can share a public link or with other users on the instance, I'd suggest the latter

### Should we "appoint" the facilitator, too? (smichel)
- smichel: We have people volunteer for note-taker & timekeeper. Usually it's always the same people, and that's fine. We don't do it for facilitator, though.
- Group consensus: yes

---

<!-- If anyone arrived late, their snowshoveling checkins here -->

6 Open discussion <!-- if time, timebox: 5 minutes -->
---------------------



7 Breakout rooms <!-- Decide on topics, list, and invite specific participants; actual discussion later -->
-----------------------


8 Meeting eval round: feedback, suggestions, appreciations <!-- Reserves last 5 minutes -->
-------------------------------------------------------------------------
- msiep: appreciate attendence, time/note/facilitator
- smichel: open discussion with timebox feels weird, glad to end early
- Salt: +1 , still think open discussion w/ timebox is good overall
- align: thanks for being here, sorry for issues, was trying Mumla (vs Plumble)
- wolftune: happy we're meeting, for consistency. If I were in a better place personally, I'd like to spend the time thinking/talking about how we can build up momentum again. Like metrics, each meeting is a chance to reflect on how we're doing & re-commit. We should avoid just cutting short when little is happening and leaving it at that.
    - added note from smichel: we should end the meetings but then commit to *using* the rest of the hour to cowork or otherwise make real progress.

<!--

    Standup portion of meeting adjourned!

    People are free to go if not staying for breakout sessions

    Clean up meeting notes, then add to wiki

    Prepare this pad for next meeting:

    Update next meetings date

    Move new attendee list and metrics to previous, leave new blank

    Replace previous meeting eval notes with new, and filter it down to essential points

    Clear the "last meeting next-steps review" section

    Move new NEXT STEPs to that review section and then clear out other notes areas:

    The snowshoveling check-in notes

    Agenda items

    keeping the topic title (but not topic leader) with the NEXT STEPs to review

    unbold the topic headers

    Open discussion notes

    Clear authorship colors

    Breakout room discussion

    Use multiple mumble channels for parallel discussions, if necessary

    Don't worry about notes, but do capture next steps, if needed.

    Update notes in wiki if needed and clean up breakout room section

-->


<!--
Meeting best-practices and tools

Etherpad use
- Use chat in etherpad (and add your name)
- Option: audio notifications on firefox via https://addons.mozilla.org/en-US/firefox/addon/notification-sound/

Agenda topics
- Each topic facilitated by topic lead with main facilitator help
- As needed, ping !folks on matrix to read over anything advance, ideally before the day of the meeting

NEXT STEPS
- each topic should capture NEXT STEPS (or clarify that there aren't any)
- should be clear and actionable
- assignee agrees to capture or complete by next week

Timeboxing
- timebox each topic, rounded to nearest 5min., settled during agenda confirmation
- format is: "Timebox: 10 minutes (until hh:mm)" (in an html comment so it doesn't appear in note archives)
- at topic beginning, convert the :mm to expected end time
- at timebox end, facilitator may choose to extend by a specific amount
    - informal "thumb polls" inform the decision for extra timebox
        - "^" approve, extend the timebox
        - "v" disagree, move onto the next topic
        - "." neutral

Discussion options
- open discussion
- call for a round ("pass the mic" style, facilitator makes sure no one is skipped)
- hand symbols
    - "o/" or "/" means you have something to say and puts you in the queue
    - "c/" or "?" means you have a clarifying question and jumps you to the top of the queue
    - "d" means thumbs up, encouragement, agreement, etc.
    - ">" means you understand someone's point, please move on
    - "d>" indicates feeling complete on an agenda item, ready to move on to

Notetaking
- "???" in notes means something missed, please help capturing what was said
- aim for shorthand / summary / key points (not transcript)
- don't type on the same line as another notetaker ( ok to do helpful edits after but not interfering with current typing)
-->