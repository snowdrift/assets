<!--

    Previous weeks' meeting notes: https://wiki.snowdrift.coop/resources/meetings/2020/

    Meeting location: Mumble server: snowdrift.sylphs.net port 8008 (half-duplex mode on mobile: avoids echo)

    Alternates for video/screenshare: https://bigblue.cccfr.de/b/mra-qy2-x6n or https://meet.jit.si/snowdrift

    See end of this pad for meeting-practices notes and reference

-->
 
Meeting — September 30, 2020
===========================

- Attendees from last week: wolftune, MSiep, smichel, Salt, alignwaivers
- Attendees (current): smichel, Salt, MSiep, wolftune, mray, davidak, alignwaivers

<!--

    Personal check-in round

    Assign facilitator

    Assign note taker

    Assign time keeper, use https://online-timers.com, start for each item, paste each url in the chat

-->

1 Metrics
--------

Discourse (over past week): <!-- from https://community.snowdrift.coop/admin?period=weekly -->
 
- Signups: 0 -> 1
- New Topics: 1 -> 4
- Posts: 1 -> 26
- DAU/MAU:  29% -> 44%

Snowdrift patrons: 126 <!-- from https://snowdrift.coop/p/snowdrift -->

2 Review previous meeting feedback
-------------------------------------------

- all: appreciations for showing up despite slow progress
- smichel: open discussion with timebox feels weird
- Salt: +1 , still think open discussion w/ timebox is good overall
- align: sorry for issues, was trying Mumla (vs Plumble)
- wolftune: we should avoid just cutting short when little is happening and leaving it at that; meetings can be a chance to think/talk about how we can build up momentum again.
    - smichel: use can *use* the rest of the hour to (co)work, too. Progress => momentum.

3 Last meeting next-steps review
----------------------------------------
<!-- Mark NEXT STEPs as [DONE] or [CAPTURED] <LINK> by the next meeting -->

### Options for next steps for new approach to crowdmatching
- NEXT STEP (everyone): read forum post <https://community.snowdrift.coop/t/options-for-next-step-for-new-approach-to-crowdmatching/1608>, reply there or be prepared/plan to discuss next week [DONE]

### Civi Update and Temp Spreadsheet Info
- NEXT STEPS (Salt): Upload spreadsheet to nextcloud [DONE]

    - https://nextcloud.smichel.me/index.php/f/108811


4 Snow-shoveling check-in round
-----------------------------------------
<!--

    One minute silence — check thoughts and notes/tasks/emails to surface tensions

    Quick answers:

    What is your current focus?

    Are you blocked on anything?

    What help could you use?

    Add to agenda or breakout room topics as needed

-->

- mray: applied to prototype fund, we'll see what comes out of it. Current focus = re-thinking home page; blocked on mechanism changes; help = have this meeting :)
- davidak: focus = mechanism updates
- smichel: focus & progress on bylaws, plan to send to lawyer by weekend; not blocked & looking forward to higher availability
- Salt: focus=civi but feeling defeated by it; blocked on osu technical issue; considering just asking them to reset, could use help making that decision. other focus = mechanism updates
- msiep: mechanism updates; not blocked
- wolftune: seaGL talk accepted. legal focus + figuring out how to juggle personal time/availability; not sure *how*, but could use help on time management / task management. Bookmarks on the forum to deal with.
- alignwaivers: busy with personal stuff [smichel's guess]

5 New Agenda
------------------

<!--

    Does anyone need to leave early?

    Confirm agenda order and timeboxes (in 5 minute increments)


### TOPIC TEMPLATE (LEADER) <!-- Timebox: # minutes -->
<!-- -->


### Changing Meeting Times (Salt) <!-- Timebox: 5 minutes -->

Goal: Decide on a date by which to update the meeting time

- Salt: Background — we usually change schedule quarterly, to mirror my academic schedule. Switching usually takes 1-2 weeks.
- Salt: We'll need to wait until my & Aaron's schedule have settled to update
- wolftune: I can't answer until next week at least. Concerned about uncertainty / my own fluctuating availability.
- Salt: Some time slots do-able but non-deal. Yellow whenisgood = please try not to schedule there!
- NEXT STEP (): decide on new time by Oct 14
- NEXT STEP (everyone): update availability by Oct 14


### New approach to crowdmatching: Next steps (MSiep) <!-- Timebox: 15 minutes -->

Goal: Get a sense of team's thinking; figure out what's needed to come to consensus

- msiep: call for round on input: overall perspective on the new approach to crowdmatching
- ROUND:
    - msiep: re forum responses: I liked Aaron's way of preventing abuse via reducing goal. I lean towards option 4 or 5 that I presented. Want to be egalitarian. Max % of goal per patron is a good restriction.
    - alignwaivers: Generally like it. As long as there's a mechanism to get patrons' approval after goal reduction, it's ok though.
    - smichel: overall positive impression. Think multiple/changing goals can be tabled for now. As far as things that aren't core to crowdmatching: would err on the side of 'what other crowdmatching mechanisms have done 
    - Salt: generally positive. Some specific thoughts - if we are below a goal, its a percentage of the max amount. Against restricting patron max to arbitrary amounts. Partially relates to the project needs. Think that changing the scope seems reasonable to some degree. Changing the goal amount directly, not sure about. Wanted to discuss the ability of a project to change its' max percentage ???
    - wolftune: A while ago, in-person conference. Got specific feedback from a small group of people (focus group?). Like the direction a lot — "when im at my max, the goal is hit for the whole project" (but not all or nothing) is easy to visualize.  Would like a map of options so if we hit a pitfall, we can cross it off with the reason why. We should figure out the smallest acceptable version that everyone's ok with to unblock some homepage/video/code progress. Then, we determine the range we're exploring and make high-quality presentations of them designed to get outside feedback as we decide.
   - davidak: I like the overall direction and the requirements discussed earlier (especially #4 with a slight difference: we should have defaults for the options ($1, $10, or custom) because we know what we works best. Also, percent per patron, I think we should define it, that the funding should be sustainable, even if a project might want fast money from a few patrons, might not be sustainable. With 10% a single person makes too much of a difference. Patrons can change their max as described in other comments. If there were more than one mechanism, it will be confusing for projects who will not know which to use. 
   - mray: Agree with the 2 main elements msiep introduced: adding goals & patrons deciding how much to give. But, concerned about details. Too many versions to talk about all the options though.
- ROUND ENDED
- Salt: Mapping out the space is being done here; we asked msiep to come up with solid proposals, so mapping feels like moving backwards a little. 
- wolftune: It sounds like everyone agrees with: project has goal; patrons set what they're willing to give at that goal; they give proportionally to progress toward the project goal. We can agree on that, then the boundaries of what we disagree on / space to map out possibilities is clear
- align: There's short-term benefits of single patrons giving a large % of the max, may want to have flexibility there.
- mray: I see this proposal as branching off later from what is a common ground (as wolftune pointed out). I think the big question to clarify/discuss is matching people or money.
- wolftune: can we decide where we are in complete alignment? proposing that we explicitly say what we have to agree on
- smichel: Proposals seem like they *are* mapping the space.
- Salt: +1 to us selecting the defaults at the beginning, at least, and +1 to what alignwaivers said : sustainability is the big goal but we want to help FLO projects however they might benefit from the platform. Think mray has a great point
- wolftune: agreed, we've built certain values, if can defer to a project that's completely aligned with us that's fine. Would like to get consensus that "projects have a goal and patrons say what they would contribute at that goal and then give a proportional percentage along the way" (not saying whether the goal/% is based on $ or patrons)

    - Everyone: yes, consensus obtained (hooray!)

- NEXT STEP (wolftune): post on the forum (that the team has agreed) on this and more discussion [DONE] <https://community.snowdrift.coop/t/options-for-next-step-for-new-approach-to-crowdmatching/1608/>
- 

---

<!-- If anyone arrived late, their snowshoveling checkins here -->

6 Open discussion <!-- if time, timebox: 5 minutes -->
---------------------



7 Breakout rooms <!-- Decide on topics, list, and invite specific participants; actual discussion later -->
-----------------------

### Mechanism: Matching money or patrons? (Salt, davidak, mray)



8 Meeting eval round: feedback, suggestions, appreciations <!-- Reserves last 5 minutes -->
-------------------------------------------------------------------------

- smichel: Lots of attendees = long rounds (check-in and agenda). Not sure what to do about it, maybe just people try to speak in shorter chunks
- wolftune: thanks for facilitation Salt: facilitation feedback is coming to mind (perhaps to explicitly call for feedback). Having things clearer would be helpful
    - smichel: Instead of asking for permission, "Not sure if I should do this, stop me…"
- Salt: think it went pretty well, thanks to everyone for being here. Feedback from myself: had 10-15 people in a meeting, with that number I have to be more forceful with facilitation in order to move forward with the queue etc.
    - smichel: mumble has "priority speaker" (everyone else gets quieter when they talk), could use that in these situations
- mray: thanks for facilitation. Was looking forward to talking with msiep, pity the time is up.
- MSiep: thought  it went well, appreciate notetaking and timekeeping, glad we got a somewhat extensive discussion on this
- davidak: glad we got consensus and are moving in the right direction

<!--

    Who will clean and save notes and then update pad?

    Standup portion of meeting adjourned!

    People are free to go if not staying for breakout sessions

    Clean up meeting notes, then add to wiki

    Prepare this pad for next meeting:

    Update next meetings date

    Move new attendee list and metrics to previous, leave new blank

    Replace previous meeting eval notes with new, and filter it down to essential points

    Clear the "last meeting next-steps review" section

    Move new NEXT STEPs to that review section and then clear out other notes areas:

    The snowshoveling check-in notes

    Agenda items

    keeping the topic title (but not topic leader) with the NEXT STEPs to review

    unbold the topic headers

    Open discussion notes

    Clear authorship colors

    Breakout room discussion

    Use multiple mumble channels for parallel discussions, if necessary

    Don't worry about notes, but do capture next steps, if needed.

    Update notes in wiki if needed and clean up breakout room section

-->


<!--
Meeting best-practices and tools

Etherpad use
- Use chat in etherpad (and add your name)
- Option: audio notifications on firefox via https://addons.mozilla.org/en-US/firefox/addon/notification-sound/

Agenda topics
- Each topic facilitated by topic lead with main facilitator help
- As needed, ping !folks on matrix to read over anything advance, ideally before the day of the meeting

NEXT STEPS
- each topic should capture NEXT STEPS (or clarify that there aren't any)
- should be clear and actionable
- assignee agrees to capture or complete by next week

Timeboxing
- timebox each topic, settled during agenda confirmation
- at topic beginning, set timer
    - if using shared web timer, share link in chat
- at timebox end, facilitator may choose to extend by a specific amount
    - informal "thumb polls" inform the decision for extra timebox
        - "^" approve, extend the timebox
        - "v" disagree, move onto the next topic
        - "." neutral

Discussion options
- open discussion
- call for a round ("pass the mic" style, facilitator makes sure no one is skipped)
- hand symbols
    - "o/" or "/" means you have something to say and puts you in the queue
    - "c/" or "?" means you have a clarifying question and jumps you to the top of the queue
    - "d" means thumbs up, encouragement, agreement, etc.
    - ">" means you understand someone's point, please move on
    - "d>" indicates feeling complete on an agenda item, ready to move on to

Notetaking
- "???" in notes means something missed, please help capturing what was said
- aim for shorthand / summary / key points (not transcript)
- don't type on the same line as another notetaker ( ok to do helpful edits after but not interfering with current typing)
-->
