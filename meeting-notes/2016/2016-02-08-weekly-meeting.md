# Meeting: Weekly Meeting

Time: 02/08/2016 08:00 PM - 09:00 PM (GMT+00:00) Casablanca

Location: https://meet.jit.si/snowdrift

Invitees: Aaron Wolf; Brian Smith; Bryan Richter; Charles Wyble; Iko ~; Jason Harrer; Michael Siepmann; Robert Martinez; Stephen Michel; William Hale

Attendees: Aaron Wolf; Bryan Richter; Iko ~; Jason Harrer; Michael Siepmann; Robert Martinez; Stephen Michel; William Hale

---

# Agenda for Feb 8th, 2016

## Jason's Topics:
* Holocracy Roles / OP Groups & Roles

## Salt's Topics:
* CiviCRM
* LFNW
* SCALE contacts
* Portland meetup
* Sandstorm powerdown

## Bryan's topics:
* mechanism
* developer docs
* curious about user story / PM progress

---

# Minutes

## Check-ins

* Aaron
** has begun drafting personas. Will share when more complete.
** Mentions upcoming decrease in availability (kid on the way)

* Jason
** Drafted intentionally-overkill structure in OpenProject to test limits of features
** Asked for feedback on emails, including Bryan's response

* Bryan
** Progress on mechanism
*** Recognize the database as persistence component
*** Experiment with using Persistent (the library) in a way that can be combined with other Persistent-based components
** Proposes using the git repo as storage for stable developer docs (style guide, architecture guide, ...)

* Salt
** Report on FOSDEM (summary: it went very well)
*** 30-40 people, definitely some interest in Q&A afterwards.
*** Someone from opensourcedesign popped into irc, was very interested.
*** Slides are on website under presentations.

* Robert
** Explains seafile seems to be better fit than owncloud for Design team's needs
*** Better performance, better suited to file syncing

## Decisions

* CiviCRM accounts will remain separate from Snowdrift.coop accounts
* Salt takes ownership of all points of CiviCRM administration
* Snowdrift.coop will attend LFNW (April 23-24, Bellingham WA)
** Salt and Bryan will manage booth
** Salt may present
* Portland Snowdrift.coop meetup will take place on Friday Feb 19.
* Snowdrift needs more social media presence
** Salt will take ownership

## Action Items

* (Salt) Add SCaLE contacts into CiviCRM
* (Salt) Coordinate upcoming Portland meetup
* (Jason) migrate docs off of Sandstorm Etherpad:
** Project page specs - http://flurry.snowdrift.coop:2040/grain/pq7fzhoZ2yGJTQc3cfFpNt
** Pages for SCALE - http://flurry.snowdrift.coop:2040/grain/vuf3rR3dAgX8uajRBQCakz
* (Aaron) Place first draft of personas on OpenProject wiki, so Robert can begin collaborating
* (Bryan) Consider keeping Sandstorm, with Asheesh's support?
* (Stephen) Migrate "meeting" wiki pages into OpenProject
