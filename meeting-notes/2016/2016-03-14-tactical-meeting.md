# Meeting: First Weekly Tactical Meeting

Time: 03/14/2016 07:00 PM - 08:00 PM (GMT+00:00) Casablanca

Location: meet.jit.si/snowdrift

Invitees: Aaron Wolf; Anita H.; Athan Spathas; Brian Smith; Bryan Richter; Charles Wyble; curtis gagliardi; Iko ~; Jason Harrer; kenneth wyrick; Michael Siepmann; Peter Harpending; Robert Martinez; Stephen Michel; William Hale

Attendees: Aaron Wolf; Athan Spathas; Bryan Richter; Iko ~; Michael Siepmann; Robert Martinez; Stephen Michel; William Hale

Additional attendees: stoopkid, how

---

# Minutes

[Process for Tactical Meetings](http://www.holacracy.org/wp-content/uploads/2015/08/Tactical-Meetings-card-bw.pdf):

* Check-in Round - what's on everyone's mind, e.g. mental state, not necessarily project-related
* Checklist Review - status update, where we are
* Metrics Review  - measures to quantify how far we are, trackable numbers
* Project Updates - what's changed since last update 
* Triage Issues - issues preventing task completion and identify next steps
* Closing Round - final thoughts, e.g. about the meeting


# Check-in Round

# Checklist Review

# Metrics Review

Salt: 
- Configuration of civicrm %
Athan:
- import donors into civicrm 124/250
wolftune:
- writng bylaws: 60% 
chreekat:
- Pages for launch 40% done (avg of how done each page is)
smichel17:
- Roles created: 

# Project Updates

(Note: since this is the first tactical meeting, participants can recount current status as well as changes)
ikomi:
    - added some fixes to a minor WIP css MR
    - business card for LibrePlanet almost ready
Salt:
    - creating profiles for volunteer form, usability, testing group & announce list
    - implementation has been slow due to drupal permissions bug
Overall configuration of civicrm
  - Complete roadmap
  - Beginning of implementation
  
Athan:
    - Still updating civicrm
    
Aaron:
    - Large backlog of contacts for civicrm, waiting on civicrm working and other priorities
    - clarifying roles for others to keep snowdrift moving during my unavailability
    - mailing out rewards (wolftune)
MSiep:
    - Have a report to share now.
chreekat:
    - working on trimmed down version of code, "reboot" branch on gitlab, mirrored to github
    - Plan is to start there and move forward

# Triage Issues

* waiting on solidified site route structure for passphrase page css (ikomi)
  - bryan works with mray to get them the tools they need
* server issue (salt)
  - no next actions
* How to present report? (MSiep)
  - schedule a meeting to discuss design implications
* UI vs UX design? (mray)
  - Give mray update on design roles
  - msiep:
  - msiep: change "UX design" -> "interaction design"; visual + interaction = UX
  - msiep: UX doesn't capture user behavior - does the structure of the matching pledge affect how many pledge? Fits under interaction design.
* CiviCRM ambassador (salt)
  - send an email to elist solliticing feedback or questions on civicrm
* adding tasks to OP (chreekat)
  - help knowing how to use OP
  - wolftune has access to how charles is using redmine
    - smichel17 in contact with wolftune about this
  - smichel17 send an update on that 
* communications to create pages (chreekat)
  - What pages need to exist and what do they need to convey?
  - wolftune will go through the current wiki, etc to create a sitemap
* OSI vote (wolftune)
  - We can vote for the OSI board. Who should we vote for?
  - next: process tension: how do we as an organization decide who to vote for in OSI elections?
* Finalizing move to githost away from git.gnu.io (wolftune)
  - task: wolftune needs to coordinate this with sysadmin
* capturing more of everything into OpenProject (wolftune)
   - need guidance, already addressed
* update OpenProject? (wolftune)
   - update from salt: currently we have people in various roles doing sysadmin tasks. In the future we'll have one role for this. We should avoid "pushing any 'easy update' button"

# Closing Round
