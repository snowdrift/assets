# Informal Meeting — November 28, 2016

- This was an informal meeting held around 20:30 UTC (after the regular Monday
  meeting) to review and discuss the video script for the site's landing page
- Draft in progress: <https://pad.riseup.net/p/Fo9LxmtDZcua>

- Salt: there are currently 7 sentences in the script, each corresponding to the following:

1. the what
2. the why
3. the who
4. the where
5. the how
6. the goal
7. the ask


## Video script draft

    Chat SUGGESTION/

    1. Things like software, music, movies, journalism, and research *can* be released as public goods.

    2. Then, everyone may use and share them freely, without limitations.

    3. But who will fund their production?

    4. Our innovative platform empowers you to join with others to fund the public goods /you/ care about.


    5a. At Snowdrift.coop, you pledge to donate a tiny amount for *each* patron who supports the same projects, within a monthly budget you set.

    5b. At Snowdrift.coop, you pledge to donate to a project a tiny amount for *each* patron who supports the project with you. And you control your budget with a system-wide monthly limit.

    5f. At Snowdrift.coop, you pledge to support a set of projects, joining other patrons who have made the same pledge, all within a monthly budget you set. The entire community matches each patron, thus a tiny amount is able to effect real change.

    5g. At Snowdrift.coop, you pledge to support a set of projects with a tiny amount for each other patron who makes the same pledge, all within a monthly budget you set.

    5h. Your pledge to support a set of projects is matched by other patrons who have made the same pledge, all within a monthly budget you set.

    5i. At Snowdrift.coop, you pledge to donate to your favorite projects a tiny amount multiplied by the number of patrons who contribute with you. You still control your budget by setting a system-wide limit for monthly donations.

    5j. You pledge to support your favorite projects with a monthly m a tiny amount multiplied by patron who makes the same pledge, all within a site-wide budget you set.

    5k. It all begins with a tiny monthly pledge to support your favorite projects, this is multipled by other patrons who have made the same pledge for the same project, all within a site-wide budget you set.

    5l. You choose projects to support, and make a pledge: "I will donate a tiny amount each month for each patron who pledges with me!"

    5m. It all begins with a pledge to support your favorite projects, this tiny amount is multipled by all other patrons who have made the same pledge for the same project, never exceeding the site-wide budget you set.


    6a. So, your donation gets matched by other patrons, thus directing support to the most promising projects.

    6b. Crowdmatching naturally builds consensus, helping you direct your support to the most promising projects.

    6c. Because you are part of the crowd, support grows together and is directed towards the most promising projects.

    6d. We call this "crowdmatching", it means that support grows together and is directed towards the most promising projects.

    6e. This process, called crowdmatching, builds consensus and directs support to the most promising projects.

    6f. This process, called crowdmatching, directs support towards the most promising projects and allows you to effect real change with a tiny amount.

    7. Join us in clearing the path to a free and open future!

    /Chat SUGGESTION


## Discussion

- Salt: I like the intro and the way it gets straight to the point, but the list was too long
- wolftune: the one I had already or MSiep's script?
- Salt: MSiep's script
- wolftune: can you explain a bit about your view, just to clarify what you had in your mind when you wrote the altruistic bit?
- Salt: we're trying to make the world better, less the creation process and more that we're trying to support the development stage
- why people develop content and give it away. whether that's selling or giving it away for people to do whatever they want, for the betterment of the world
- wolftune: okay, I think I get what you're saying. however, while there are cases where people are inherently selfish and fund something because they want it, as well as people who are fundamentally good and we just need to give them the means to fund things, what we want to communicate is, snowdrift doesn't relying on either case, that it works regardless of either
- MSiep: just reconnected and didn't catch the entire previous conversation, but my comment, the altruism part isn't probably something everyone aligns with
- wolftune: to be clear, I'm not planning to address altruism directly in the script
- we want people to understand this is about access, use, distribution and remixing or modification
- you should be able to remix and share freely and everyone has access
- mray: what's your problem with saying it that way, in similar terms but without using terminology and going too much into detail?
- wolftune: that may be the best approach, I'm not sure. my concern in general is, people might say "I totally get it", and then when you ask them to explain it, it's not correct
- mray: there's the option to help people feel like that they basically get it, maybe not everything right, they're not totally wrong either
- MSiep: agreed, the more important part with this video is getting people interested and motivation rather than completely correct. as long as it's not misleading
- wolftune: agreed with MSiep, it's okay if people say "I'm interested, I'd like to find out more." that's a great result.
- mray: as the first paragraph, it should be in simple terms
- MSiep: it should be clear enough to not feel puzzled, to want to see it in more detail
- mray: we have to take care not to put too many questions in people's heads at the start. I think the current wording fits well for the purpose.
- wolftune: I agree largely that the wording is the right direction.
- the question I ask with each sentence is, "Does this give more clarity than
  before?"

- MSiep: enjoy is more "specific" than "benefit from"
- Salt: I agree that "without restrictions" was kind of important as we're talking about public goods
- I want to get it down to 3 items. I thought multimedia > media, but your list was a bit heavy on the list of things
- MSiep: I just imported it from previous versions, I didn't change it.
- mray: I would prefer media, because it's simpler
- MSiep: in US, the media is a loaded word, more associated with TV, newspaper owners and Rupert Murdoch
- mray: we don't have that connotation here in Germany
- Salt: MSiep, same reason I avoided the term media
- MSiep: since we're waiting for wolftune's feedback, it might be more efficient to wait until he's here. I can come back on to talk about it, but I'd like to get some other things done.

- *(MSiep leaves the discussion temporarily)*
- Salt: mray, in the "you pledge" paragraph, I moved where the budget statement was and added commas to the list since everything else were modifiers
- on the next sentence, I understand your meaning about growing community, but there probably is a better wording for that
- mray: I don't want to give the impression that things are better than they actually are, I think people see through it quite quickly
- Salt: yeah
- wasn't the "join us in clearing the path" an earlier slogan? it's pretty good
- mray: yeah, and aiui wolftune is not willing to give it up
- Salt: it keeps a ghost of why it's called snowdrift
- instead of "multimedia", what else can we use? because multimedia can include art, news even
- mray: e.g. software is an easy-to-understand, concise term. if we have 3 items, it's enough
- what about the using the word "art" to encompass movies and music? I think people can easily make the jump from art to software
- mray: it may have a too-strong tie to the art world
- Salt: "software, arts and research"
- mray: I'm not sure. I think "arts" is very vague. my impression of arts is, there's a strong tie to sculpting and painting which are things that for technical reasons we cannot cover
- Salt: that's not correct.
- iko: I thought the scope is non-rivalrous goods?
- mray: this is a problem of representing more than the scope
- Salt: I can see where you're coming from, but my understanding that it could be covered under the snowdrift dilemma.
- mray: there are mentions of public roads and parks, and even so, it would be something in the far future, since only a local subset of people can benefit from it. we should probably stick to the internet.
- Salt: yeah, if those are supported in the future we can make a new video

- *(wolftune and MSiep return, resuming discussion)*
- Salt: wolftune, to summarise previous discussion, there were some concerns about using "media" due to the connotations in the US
- wolftune: yeah, I agree. also, I wouldn't use "pioneering". how about just "new"?
- MSiep: I like "innovative", "revolutionary" is a bit strong, "new" is not that interesting
- wolftune: "revolutionary", I'd use that word if I know the current audience I'm talking to, but I know it'll sometimes get a different sort of reaction
- I agree that it has to be a list that's broad enough for people that doesn't limit it into a box
- I'm basically happy with the first sentence, but I don't like to emphasise the idea that you only get things when you pay
- I would rather the word "fund" or "support".
- the end is getting things done and funding is the way you do it. if we can get it done without paying people, great, but the important part is getting it done.
- I don't like the connotation. I would say something like: "how would we fund development in the first place?"
- we are not about rewarding people forever just because something became popular
- mray: we try to formulate the problem into a question, how are people getting paid
- wolftune: funding is a broad thing, pay is a specific action. "How do we fund things? We get people to pay for it."
- how does something get funded? snowdrift empowers people to pay for this work. I'm okay with it in that context.
- mray: my expectation is the phrasing it in reverse
- wolftune: I don't like the connotation that paying is the only way to get things
- the answer is we need to work together to pay for it
- MSiep: it pays money to projects to support them. keeping this simpler in fewer words might be better.
- wolftune: I agree with that as a guideline, just not sure it will apply in this case
- MSiep: if we start talking about getting people to pay then in might be too complicated
- you suggested "development" earlier, maybe that word?
- Salt: development, not necessarily creation, is what you're paying for — advertising, refining, etc.
- MSiep: maybe "development" is too closely tied to software. how about "produce"? "make" is maybe better than "create", but still similar.
- wolftune: I agree in terms of definition "development" is the best word, but it may have connotations for software, etc.

- wolftune: I have mixed feelings about the word "empowers". I really like the word and what it stands for, but in the context of snowdrift and that sentence is overloaded with syllables
- "platform" vs. "system"? no strong feeling about it
- MSiep: what I liked about "empower" is, from "what's in it for me" perspective, using "we" vs. "me" may make people feel less empowered, like they're not funding the things they want/agree with
- wolftune: that makes sense. you don't join the collective to undermine the individual wish, people still get to choose which projects they want to support
- "we" is important in the sense it's not effective to not fund on your own, we all agree to fund those things
- MSiep: it doesn't have to be everybody, just a critical mass of people. as an individual, snowdrift brings like-minded people together to fund things
- what you want to exist in the world, what you care about. if you don't care about it, it becomes more about whether you can afford it.
- I have a question about research. people get the impression that research is already being funded as a public good.
- mray: we're trying to draw a more detailed picture that isn't necessary at this point. the most important thing is to include research but not stressing we aren't limited to one kind of thing.
- wolftune: I think journalism is a good option to consider.
- mray: do we have plans to approach journalists in the near future?
- Salt: how about writers?
- wolftune: mray, sure. we don't want to give people the impression this is limited to a few things
- mray: I still have a problem with including journalism. journalism is tied to a different set of people and needs, if we go after drafting the entire scope, we would have to be even more diverse and have to cover other things
- wolftune: I would rather the more diverse version. the abstraction we want in people's heads is public goods.
- MSiep: what about changing the word "enjoy" to "share"? for one thing, journalism isn't about enjoyment, it's more about reading and sharing with friends
- wolftune: I don't want to push too hard, but I want to get this one concept out: public goods, non-rivalrous things which includes things like [list] that you can share
- emphasise to people this is the core concept, you have to understand it
- MSiep: the reason I have a pause before "a tiny amount" is because the budget is a global one, not for each project
- wolftune: yeah, I'm not going accept tying "a tiny amount" to each project, it's going to confuse people

- MSiep: another possibility, change "patron" to "person"?
- wolftune, Salt: I think the word "patron" needs to be there somewhere
- MSiep: okay, it's not a major problem

- mray: is there some ambiguity with "when"?
- wolftune: "when" and "can" both mean they can be, but aren't
- MSiep: why not put it at the beginning?
- wolftune: I want people to process it like this — this list of stuff, I got it. what are you going to tell me about them?
- "Software, music, movies, journalism, research, and so on released as public goods, can be shared freely by everyone, with limitations"
- Salt: add "when" before "released"
- mray: "these can be released as public goods"
- wolftune: I like that it's short. how about "everyone can share" vs. "it can be shared by everyone"?
- I like "everyone", including freeriders. everyone can do this.
- wolftune: I want to include the ability to not just watch a movie, but to be able share them
- MSiep: how about "share and use them freely without limitations"
- wolftune: agree that's the best option, it's not too long of a list
- Salt: it's awkward when narrated without the pause
- wolftune: I think I need to get rid of "so on" and replace it with some other phrase. not "more", but something like "similar things"
- mray: yeah, the list has to talk for itself
- wolftune: things like [list] can all be released
- wolftune: there's some level of saying, everything could be great, but currently it's not
- mray: for me, the question is "who will pay for it?"
- Salt: I don't like "them" twice
- wolftune: fund them -> their production. it's shorter now.
- Salt, MSiep: I like that

- MSiep: "you can join" is a little weak, e.g. with brand X car you can travel from A to B, but you can do that with other cars
- wolftune: I'm okay with putting it back, "empowers you to join"

- Salt: on the last sentence, I tried changing "directs support" but the meaning is what we're trying to get at
- MSiep: how about "building consensus to support"?
- mray: I think there's a problem with "Your donation is matched by the rest of the community." it's just the part of the community that agrees with you
- wolftune: the meaning we want to present is, there are two effects: 1. everyone is matching you and 2. this system inherently builds consensus
- there are people who are frustrated by junk and fragmentation—
- mray: I don't think we need to chase after that
- MSiep: that's a very advanced concern
- wolftune: I agree, but I see a lot of people with that concern might be snowdrift's core supporters at this point
- mray: what if "your donation gets matched"?
- wolftune: while I'd like people to remember public goods, freeloading, consensus, it's a jargon-y thing
- mray: I prefer to have this part being self-descriptive.
- Salt: I don't like that we have patrons in this sentence and the one before it
- MSiep: we could introduce crowdmatching there
- mray: crowd is a good word to use there
- wolftune: I liked some phrasing with "and together, …" you match others and others match you
- at least that can be visualised in a network effect visual, but we don't want it to be too word-y
- "through crowdmatching, we build consensus …"
- although, it's redundant to say "crowdmatching together"

- Salt: I'm still not sure about dropping platform/system from the previous sentence
- wolftune: I'm inclined to include it. when speaking I'd probably use "system"
- "platform" makes it sound like there are other crowdmatching sites with different goals, whereas with "system" it's "our system"
- mray, iko: I think it sounds too technical (mray: "innovative", "system")
- Salt: I know "crowdmatching sytem" sounds word-y, but it's a brand new thing that does not exist
- mray: you could say "the way crowdmatching works, …"
- wolftune: I like that
- MSiep: isn't that more words though?
- wolftune: I agree, but I wish there was a better transition

- mray: I still see a problem posed by "But who will fund their production?", which is climatic and the next sentence isn't really answering the question
- wolftune: now that I've restructured this, I wonder if we still need that sentence, since the lines after that explains it
- MSiep: I think it's important for generating interest. the section "join with others to fund" is a natural transition that answers that question.
- mray: people already donate to projects, there's nothing new or empowering about that.
- MSiep: yeah, we switched to "snowdrift.coop" before the explanation. I think "our crowdmatching system" should be in response to the question
- I'm also uncomfortable with "within your monthly budget." it sounds like it's referring to something previously mentioned. maybe add "you set" after it to clarify.
- wolftune: there's currently no budget limit setting
- Salt: okay, that should be made clear in the new site banner, and that we don't automatically withdraw money.
- wolftune: can someone notify chreekat about the two tasks, new banner and setting a temporary hard budget limit
- *(Salt messages chreekat)*

- Salt: in "At Snowdrift.coop …" the 1st part talks about something the patron does, the 2nd part is what happens as a result
- wolftune: in other words, the 2nd is a passive effect and yeah, I wouldn't want it to be misunderstood
- mray: the problem with "At Snowdrift.coop" is that's already the whole thing, there's nothing after that. the next sentence after that clarifies but isn't introducing any other new concepts. we can give the process a name (crowdmatching)
- wolftune: I tend to dislike that because it doesn't sound very poetic, but that sounds effective here. I like the idea and would go along with it if we can come up with a good structure.


## Previous suggestions

    Chat SUGGESTION/
    1. Things like software, music, movies, journalism, and research *can* be released as public goods.

    2. Then, everyone may use and share them freely, without limitations.

    3. But who will fund their production?

    4. Our innovative platform empowers you to join with others to fund the public goods /you/ care about.


    5a. At Snowdrift.coop, you pledge to donate a tiny amount for *each* patron who supports the same projects, within a monthly budget you set.

    5b. At Snowdrift.coop, you pledge to donate to a project a tiny amount for *each* patron who supports the project with you. And you control your budget with a system-wide monthly limit.

    5c. At Snowdrift.coop, you set a monthly budget, then pledge a tiny amount to a project for every patron who supports it with you.

    5d. At Snowdrift.coop, you pledge a tiny amount that is matched by other patrons.

    5e. At Snowdrift.coop, you only pledge tiny amounts but match each other patron.

    5f. At Snowdrift.coop, you pledge to support a set of projects, joining other patrons who have made the same pledge, all within a monthly budget you set. The entire community matches each patron, thus a tiny amount is able to effect real change.

    5g. At Snowdrift.coop, you pledge to support a set of projects with a tiny amount for each other patron who makes the same pledge, all within a monthly budget you set.


    6a. So, your donation gets matched by other patrons, thus directing support to the most promising projects.

    6b. Crowdmatching naturally builds consensus, helping you direct your support to the most promising projects.

    6c. Because you are part of the crowd, support grows together and is directed towards the most promising projects.

    6d. We call this "crowdmatching" and it means that support grows together and is directed towards the most promising projects.

    6e. This process, called crowdmatching, builds consensus and directs support to the most promising projects.

    7. Join us in clearing the path to a free and open future!
    /Chat SUGGESTION


    MSiep SUGGESTION/

    When music, software, movies, news, research, and so on, are released as
    public goods, everyone can enjoy them freely, without limitations.

    But who will pay for them to be created?

    Snowdrift.coop's pioneering crowdmatching platform empowers you to join
    with others to fund the public goods /you/ want created.

    You pledge to donate a tiny amount each month for each patron who
    supports a project with you, within a budget you control.

    Your donation is matched by the rest of the community, building
    consensus that directs support to the most promising projects.

    Join us in clearing the path to a free and open future!

    /MSiep SUGGESTION


    Salt SUGGESTION/

    When goods such as software, multimedia, and research are released
    freely and for altrustic reasons, everyone is able to enjoy and benefit
    from them.

    But who pays for this content to be developed and sustained?

    Snowdrift.coop's revolutionary crowdmatching platform empowers you to
    join with others to fund the public goods /you/ want created.

    You pledge to donate a tiny amount, with a preset limit, every month,
    for each patron who supports a project with you.

    Your donation is matched by others in the growing community, building
    consensus that directs support to the most promising projects.

    Join us in clearing the path to a free and open future!

    /Salt SUGGESTION

