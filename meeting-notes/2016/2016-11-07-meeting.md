# Meeting — November 7, 2016

Attendees: wolftune, msiep, iko, mray, Salt

---

# Checkin

* wolftune: in Michigan this week, some distractions and won't be at SeaGL
  (Athan should be going)
    * my issue will be figuring what should be done between now and next
      weekend
* iko: fixed two wiki css bugs, messed around with Salt's FOSDEM2016 slide deck
  with his permission and updated 1-2 images
    * <https://snowdrift.sylphs.net/d/0e1086d735/?p=/slides-salt&mode=list>
* MSiep: staying in touch with updates, having some issue with irc sasl
* mray: busy with looking into software for proper animations and switched
  again, tried out Blender and trying out Natron
* Salt: China trip was good, visited a makerspace in Shanghai, gave a talk
  about toorcamp
    * went to a larger barcamp, gave a translated talk about snowdrift there as
      well
    * SeaGL is coming up on Fri/Sat, this year I ended up being the lead
      organiser
    * I have a talk for indieweb for Fri and need to write slides for that
    * on Sat I have a Lightning Talk about snowdrift (5 min)
    * the big thing is (I'm confirming this with chreekat) the announcement of
      a soft launch, where Dec 1 will be the press release launch


## Agenda

- Feedback on intro video script (wolfune)
- Update presentations on wiki (wolftune)
- Intro video and soft launch (wolftune)


## Feedback on intro video script

- wolftune: msiep, have you seen the comments robert and I had about the video
  script?
- msiep: I read the email comments but couldn't open the script file, when I
  tried to download it, it was a .bin file.
- wolftune: there were two .odt files which have change history, his comments
  and my replies to them
- msiep: okay, let me look at it now
- wolftune: it needs to be clear to everyone what the point is, but when I went
  through this, I think it's important to emphasise this is about public goods.
- I want people to walk away not just about snowdrift dilemma.
- msiep: it's part of our tagline, so it's important to introduce the concept
  quickly
- iko: generally agreed, one part I'm not sure about is "And besides roads"
- I can see why it's used there, but snowdrift is focusing on non-rivalrous
  public goods and some people might read it literally and say they're not the
  same thing (as digital music, software, etc.)
- wolftune: roads are in a sense non-rivalrous. it's only rivalrous when it's
  all filled up with traffic. some roads do and some don't
- iko: yeah, but then you might have to explain this, which maybe the comment
  about removing it will avoid
- wolftune: the important wording is "can", I think it's inferred that roads
  can be non-rivalrous
- msiep: about the mutual assurance sentence, I do agree with robert that's a
  lot of concepts to digest in one sentence
- wolftune: yeah, I see that. the way Salt has talked about it in terms of
  combining the pledging of Kickstarter with the ongoing funding of Patreon
- in this video I don't want to call out specfic platforms
- I'd imagine some visualisation of the concepts like
- mutual assurance = characters in handshake
- ongoing support - a calendar
- accountability - ? spending
- wolftune: and iko, this might address your question in irc earlier, we could
  just have minimal slideshow version of a video, to have a video for now, not
  spend a lot of time/energy on that
- msiep: there's some threshold before something makes a bad impression, but as
  long as it basically doesn't look bad, having something is fine
- iko: sure, have you suggested this to mray?
- wolftune: not yet
- iko: okay, you guys might want to discuss that later
- wolftune: what do you think of the last comment?
- msiep: it's a maybe not invitation, how about incentive?
- wolftune: I guess I want it to be an invitation, encouraging people to pledge
  and then tell their friends
- msiep: yeah, that's the incentive once they have pledged, you're not directly
  messaging anyone
- wolftune: yeah, I guess I can accept that
- msiep: how about "each pledge helps to attract additional pledges"?
- "each pledge increases the incentive for new patrons to join"?
- wolftune: I wanted to capture Joey Hess' excitement:
  <https://joeyh.name/blog/entry/snowdrift/>
- what I wanted in the narration is to visualise this element
- msiep: I think the word "encourage" is a good one … "and increases the match
  to encourage future patrons" or something?
- wolftune: I don't really want to say "future patrons", more like the next
  patrons
- msiep: maybe more personal, "the next patron"
- wolftune: maybe it doesn't need to be in the video, but the idea is it
  provides ongoing salaries, keeps projects accountable as people don't have to
  give a big sum up front
- msiep: it assumes people understand it's a monthly thing and they can
  withdraw anytime
- how about moving the word monthly, "a little bit monthly"?
- wolftune: I tried that but it feels like too many qualifiers to process
- msiep: to make it easier to visualise, maybe make project singular
- wolftune: I understand your concern, but it may lead people to think they're
  budgeting for one project
- we want people to feel like there's lots of projects to support and people
  can go in and choose what they want to support
- iko: how about removing the "so" in "So, snowdrift.coop help …"? more concise
- msiep: I think it can be removed, it's sometimes considered fluff words like
  "um, ah" , even pausing would work


### Intro video script

(Revised by wolftune and msiep via shared document)

When a snowdrift blocks the road, everyone wants it cleared, but nothing gets
done if we all wait to see whether others will do the work.

This snowdrift dilemma is an example of the PUBLIC GOODS PROBLEM. When everyone
gets the results, whether or not they helped, how do we get enough people to
contribute in the first place?

Anything we can all share with no limitations is a public good. And besides
roads, that can include music, software, movies, news, research, and so on… and
the snowdrift dilemma comes up for any of these that have substantial
development costs.

Snowdrift.coop helps solve this dilemma with our new crowdmatching system.

You simply pledge to chip in a little bit for each patron who supports the same
projects with you. Each month, we calculate your donations based on the numbers
of patrons and your budget limit.

So when you pledge, you get matched by the rest of the community AND increase
the matching incentive for the next patron.

And crowdmatching helps build consensus around the most promising projects so
that we can focus our resources and create the most value for everyone.

Come join us in clearing the path to a free and open future!


## Update presentations on wiki

- wolftune: Salt, can you update the presentations page on the wiki with your
  recent talks (along with any related assets from iko)? you don't have to do
  that right now
- Salt: yeah, I'll do that

- Next steps: Salt will update presentations page and the new assets to go with
  them


## Intro video for soft launch

- wolftune: since the soft launch is coming in a week, and the full video won't
  be done in time, what's the minimal thing we can release until we have a full
  video?
- how about a placeholder with the video "coming soon"?
- mray: I don't think that would look good. some options — 1. add text that can
  do the same job, 2. slides with audio
- wolftune: my vote is to have audio in place
- let me read this aloud this right now and you can give feedback on it
- *wolftune does a demo narration of the intro video script*
- Salt: it's still a bit long, I'd like to try and keep it to 45 seconds
- wolftune: it's just over a minute, I think under 2 minutes is acceptable for
  an intro video
- mray: what is the job of this video?
- wolftune: to explain to people why it's called snowdrift.coop, so they can
  remember the name to tell their friends
- clarify the slogan, what crowdmatching is and what public goods
- why this is exciting and why they'd want to use this
- clarify what kind of projects we're focusing on
- mray: in the 2nd sentence there's already 2 terms. "snowdrift dilemma" and
  "public goods"
- "This snowdrift dilemma is an example of the PUBLIC GOODS PROBLEM." doesn't
  really further understanding of the dilemma, whereas the next sentence is key
- the thing that bugs me is iterating the concepts, I would like to have a
  simpler way of explaining it
- wolftune: I agree that it's a poor sentence and would be happy to hear
  alternatives
- "here's the setup and this is about public goods", not just "this is about
  public goods"
- Salt: when there's little space, it's better to not repeat words. when you
  remove repeated words and find other ways to explain it, suddenly you're
  under 45 seconds
- wolftune: normally I'd agree with you, but this is just over a minute
- Salt: after 45 seconds you start losing focus for a homepage video, with
  experience from making websites
- wolftune: if you show me some data —
- Salt: I'm going to try and find you some research. the threshold is like 80%,
  80% of people drop off
- wolftune: this isn't about the website or design, it's about messaging.
  what's wrong with having a 15-second video?
- Salt: people take info in different forms, some people like having info read
  to them
- 45 seconds is a reasonable length, with enough time to cover things briefly
- I think it's good value in having a 2-minute video on the how-it-works page
- wolftune: that clarifies things, I thought we were making 1 video. if we're
  only making a single video, I don't think 45 seond version is going to get
  people to click the pledge button
- if there's only 1 video, then we should have a 1-2 minute video
- mray: the way you describe it is the problem, because the video is not being
  used the way it's intended
- wolftune: if we're making 2 videos, we could prioritise this, is it okay for
  this homepage video to hint at this?
- mray, Salt: yeah
- wolftune: I think we can make a script as a general intro, has some animation
  and hint at stuff. my inclination is to not make a 45 second video, but
  something basic
- then later move on to make a video that's going explain things to people
- Salt: sounds fine to me
- wolftune: from my communications standpoint, I'd really like to have a
  concrete video, 2-3 minutes, that explains it all, and if they have questions
- Salt: you gave me a beautiful 5 minute Lightning Talk, and I'm right with you
  on that
- by the way, someone had trouble understanding and I used a museum as an
  example
- wolftune: a museum?
- Salt: yeah, to explain that projects, like museums, need ongoing support to
  keep it running
- a question that came up was "why would anyone do anything not for profit?"
- wolfune: yeah, a capitalist view. an example of not doing things for profit
  is wikipedia
- Salt: some people don't know wikipedia exists, that's why I'm bringing it up
- "why does everybody want the result?" became the next question
- wolfune: because for example with music, it allows you to share, remix, etc.
- Salt: but that's a hard concept for people who aren't already familiar with
  the concepts that we are
- wolftune: we'll work out how to explain this to people
- mray: do you have an outline of sections?
- Salt: basically, look at my slides
- wolftune: I still want to have a longer explanatory video. one step at a time
- mray: about the background music, do you have any ideas?
- wolftune: yeah, I wrote something a while back that ended up being too
  complex, but I can work on it again after finishing the script
- if you have any examples you like, you can send them over
- mray: I would like to lean on the snow theme and use some sound effects like
  the sound of walking in the snow, crackling sound

- Next steps: wolftune will work on a "stupid vague" intro script for the front
  page video, send it by email and make a new thread on the Design list
