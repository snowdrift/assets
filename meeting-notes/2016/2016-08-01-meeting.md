# Meeting — August 1, 2016

Attendees: iko, mray, Salt, MSiep, wolftune, Ioka (visitor left shortly before checkin)

---

## Pre-meeting informal discussion

### MVP tasks on Taiga

- mray: is there a list of all MVP tasks on Taiga?
- iko: as of current state, there isn't a comprehensive list of all mvp tasks. backlog is a mix at the moment. 
- MVP is big so chreekat has sectioned out chunks, first milestone goal. this milestone consists of two sprints.
- milestone contains immediate goals (to access Sprints, go to Backlog > see right sidebar > Sprint Taskboard
- July https://tree.taiga.io/project/snowdrift/taskboard/end-of-july 
- August https://tree.taiga.io/project/snowdrift/taskboard/start-of-august

### US-433 CSS issue

- US-433 [Restyle the navbar now that the Projects and Search links are gone](https://tree.taiga.io/project/snowdrift/task/433)
- iko: on smaller screens, is the login button supposed to be aligned right (but because the two buttons are missing it aligns left)?
- mray: yes. the most straightforward solution would be to have the [login button] element be absolutely positioned on the right


## Checkin

- Salt - just got home (was delayed due to mechanical issues)
- Iko - setting up VM to build dev code (for US-316, US-319, US-330)
- mray - busy with prototyping details
- MSiep - sent out email to design ML with transaction history mockup (will be on vacation on the rest of the week)
- wolftune - working with chreekat and iko to move items off old site, wiki redirects; 
    - new /about section with recommended reading
    - public update about project status

---

## Agenda

- launch timeline (Salt)
- new font (mray) 
- too high fees for stripe (mray)


## Launch timeline

- launch by end of Aug? Design status? 
- Prototypes (mray)
    - Preview: https://snowdrift.sylphs.net/prototypes/mray/#home_page
    - Source: http://snowdrift.sylphs.net/d/e8d313536a/
    - should cover most of the content for MVP, except how-it-works page
    - wolftune: I'd like to take some time to work with you and others interested in the how-it-works page
    - at this point it's okay to link to wiki pages for further reading
    - mray: would prefer to avoid that, using basic animations, illustrations or video instead
    - wolftune: that would be ideal
- (US currently in backlog, not being milestone) https://tree.taiga.io/project/snowdrift/us/370
- Salt: looking at the timeline, looks like a hard date by end of September. date for soft launch before September?
    - wolftune: yes, we should definitely aim to launch by then

## New font

- mray: having a bad feeling about current font Nunito, it's not as suited for current purposes, the font isn't in active development. to avoid future problems, we should change the font
- "Rubik" may be best candidate: https://www.google.com/fonts/specimen/Rubik
    - it's rounder than Nunito, it works well with other design aspects and has other weights
    - it has clear numbering and spacing (problem noticed with Nunito in spacing of certain numbers in footnotes)
    - wolftune: I'm okay with this, it's readable enough. I'm not a fan of the standard weight heading, but I like it in paragraphs.
    - Salt: sure, would like to see how it looks on the site
- Salt: next step is to deploy on dev.snowdrift.coop?
    - mray: there's no rush but yes

## Too high fees for Stripe

- mray: setting up stripe might fail if the amount is too small, is this a situation we need to handle?
- wolftune: yes. for launch, the safest best is charging in arrears, so we basically have a running bill to be large enough to charge people. (e.g. people charged for 3 months' worth)
- MSiep: make sure there's no current about monthly maximum vs. this minimum, with things carried over from previous month
- wolftune: fees will be passed on, but that's another consideration as well, who is paying the fee and how it works with the buffer
- Salt: are we sure we'll go with this instead of the $5 credit model?
- wolftune: legal issue, general legal advice: "do not hold money meant for other projects"

- mray: we want to avoid transactions that seem "silly", but that's arbitrary. what's the plan to come with criteria?
- wolftune: we didn't make a plan about that. if we use another payment service, e.g. Dwolla takes no fees, the problem may be avoided
- we'll need to choose an arbitrary amount, e.g. we will charge when the amount reaches $2
- MSiep: in the mockup I sent out: at end of each month (of whenever the pledge occurred), it shows actual pledges to the projects, shows charges or charge was not happening because of minimum
- wolftune: if Sep the charge is $1 (too low), in Oct it's $10 and patron maximum is $10, then in Oct patron is charged $11 (amount is carried over)
- mray, MSiep: that sounds too complicated
- wolftune: the point of a maximum is not that the patron doesn't want $10.30, but to set a budget 
- MSiep: as long as the maximum is presented as a *maximum pledge*, not a *maximum charge*, then it should be okay
- mray: this may cause confusion as to why Snowdrift takes more money than pledged
- wolftune: I understand the concern
- MSiep: I'd like to revise the mockup to make it much clearer
- wolftune: it would only be in certain edge cases where people get close to maximum but didn't quite reach it previously. as a co-operative, we're not trying to hide fees
- mray: it's not the intention of hiding fees, I think that sets up complexity that hides fees. 
- the problem is, I set up everything properly, get visual feedback that all is well, 1 month passes, the charge is too low. what happens?
- MSiep: let's resume the discussion after revising the mockup

## Prototype update

- mray: dashboard change, no unpledge button in dashboard project list. the button is still on the project page. (cleaner view)
-MSiep: I still prefer the box visualisation suggested at the previous meeting
- wolftune: another idea is to have a colour coding key, like a linear version of a pie chart
- MSiep: that could work, though less direct. some might be very small and it gets harder to represent
- but having some kind of visualisation for the crowdmatching (the white space between 11.37 to 20.00)
- wolftune: maybe filling that whitespace with a message, "invite your friends!" 

- mray: history tab - strong visual similarity to the dashboard. self-explanatory and easy to do for MVP.
- payment system - link to Stripe setup and indication of how things are currently?
- wolftune: yes
- mray: any other payment systems we need to be concerned about with for MVP? 
- wolftune: just one system for MVP. there would have to be multiple projects, one for each payment service.
- for one project (us), we could set more payment systems. it gets complicated when we add multiple projects.
- mray: anything else missing?
    - wolftune: total amount since joined
    - mray: that's not MVP?
    - wolftune: it's not strictly MVP, but it's easy to add. I think it's useful info people will want to have. 
- mray: can you make a short recording explaining the mechanism in how-it-works?
- wolftune: sure

---

Next steps:
- Meeting on Thursday 18:00 UTC (Salt: I'll be around at the same hour Wednesday and Thursday)
- Add new Taiga task: Record a short audio explanation of basic mechanism of how-it-works
    - Added post-meeting: see https://tree.taiga.io/project/snowdrift/us/453
