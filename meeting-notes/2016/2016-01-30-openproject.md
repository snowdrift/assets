# Meeting: start OpenProject

Time: 01/30/2016 09:00 PM - 11:30 PM (GMT+00:00) Casablanca

Location: https://meet.jit.si/snowdrift

Invitees: Aaron Wolf

Attendees: Aaron Wolf; Brian Smith; Bryan Richter; Charles Wyble; Jason Harrer; Robert Martinez; Stephen Michel; William Hale

---

# Minutes

## Figure out OpenProject, get used to workflow, team project organizing…

### Discoveries:
* Groups are higher level than Projects, so the Groups created apply to all projects under that particular instance of OpenProject.
* Once a work package is created, the type can be changed after the fact.
* Milestone work packages are not intended to contain child work packages, however they can 
* Best to have two notetakers in Etherpad:  1 person focuses on actual brain dump notes and focuses on entering in the notes, another person to reorganize and format.
* Wiki currently three tiers of pages:
   # Root page (high level concept for visitors to understand concepts)
   # Branch page (lower level concepts for visitors to get more info)
   # Leaf page (ultra low level concepts that visitors may not necessarily be interested, but team members may need)

### Workflow
* Put User Stories into Wekan
* Review User Stories to validate User Stories
* ?

### Recommendations/Questions from Charles:
* We should consider having a dev subdomain (which we do have but don't currently use), current team believes after first milestone
* what are the key points of ambiguity? do we lose or gain ambiguity during discussion? do things need to be developed further before discussion?
* how critical is consensus? what is the consenus process? what are the points of break down?

### Additional Chat Notes
* Groups (http://www.aia.org/practicing/groups/kc/AIAB081947)
* User Stories (https://en.wikipedia.org/wiki/User_story), recommend 3 - 4 of different user types (patrons, project teams, etc.)
* Develop Personas (http://www.usability.gov/how-to-and-tools/methods/personas.html)
* the preferred consensus process is via a facilitator and a process I can describe a bit
* see book "How to Make Meetings Work" or other stuff on facilitated consensus
* actually, all of http://www.usability.gov/ is pretty awesome

### Etherpad Notes (Full brain dump)
Visitors understand Snowdrift.coop

Visitor comes to site

1) "I heard something about this website" we assume they don't know a whole lot, they have an impression that this is collaborative and has a general feeling of optimism.  Key introductory text (or something) this is about public goods, snowdrift dilemma: we need to cooperate to get the results we all want, if we don't we're stuck with toll-roads / proprietary etc.  Core goal: Core funding for public goods.  Public goods include open source software, music, arts, research, etc.

Link to longer page (introductory page, which will include snowdrift dilemma and other key items* that the users need to understand)  *Root wiki pages and/or hard-coded

image:  If we don't cooperate, we end up with toll roads.  Acknowledge that road maintenance is funded through taxes usually, and that Snowdrift.coop is intended to help fund other underfunded public goods (that taxes don't really cover)  This is an ongoing "voluntary alternative to taxes" for society to support public goods.

Visitor should recognize that patrons at Snowdrift.coop:

* Are pat of a larger group of people that are supporting the public goods together
* Get to choose where to direct their funds, which projects, where if you don't think your money is being well spent, you can end your pledge.
* Get to set their budget overall
* Have a say as to how this is run overall (because it's a cooperative)

Snowdrift.coop is a platform bringing together people who are creating public goods and need more funding and people who are willing to fund public goods.  (promoting active stakeholdership vs. passive users). Patrons aren't altruists, not charity, they are joining in hopes of sharing in the results of better public goods.

Ideally, projects have engaged audiences already

Once someone is clear on these core concepts, additional clarity needed:  Not a threshhold system like Kickstarter.  If they have questions, have additional FYIs/things to read and ways to get ahold of our team.


Design Action Items Commentary:
    * Overall Intent Clarification
    * Would need to have more material involving text (script?)
    * Design scope - need more detail about current content and desired structure (remove/minimize chances for misinterpretation)
    * Would design team determine how many/what pages these items impact?)
    
Development Action Items Commentary:
    * Everything is static content, infrastructure already in place.
    * Maybe A/V stuff to add in?  Infrastructure may have to be added in, depending on what design team comes up with.


PLEDGE USER STORY
User wants to become a project's patron, visits the project's page, clicks pledge button, see button changes to show that they are pledged and are a patron.

* include that they are now opted-in for default notifications about project?
* After click, get success alert indicating they are now a patron
* Previous number of patrons is now +1


We should develop list of personas for stories… that's TOP PRIORITY

several concrete example personas for each co-op member class (recommend 3 - 4 personas per class)
also, outside folks: visitors, press, people connected to competing crowdfunding platforms, trolls, spammers
different values, skills, goals…
