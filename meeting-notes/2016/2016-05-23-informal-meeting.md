# Informal Meeting — May 23, 2016


## Explaining the mechanism

```
wolftune: we need to get the system up and running ASAP to get data and not speculate. 
By the time we see that kind of dramatic increase in funding overnight, the platform will already be established/successful, it's not going to happen early on. There are early adopters who are cautious and ask questions before pledging. we want to address the questions to avoid creating false impressions as people spread the word about the project.

Points to cover:
- here is exactly what happens if a project gets successful
- this is the model and it works like this
- accountability to enable people to provide feedback and shape

Regarding what happens if there is explosive funding into a project, it doesn't immediately drop people, but it will say," this is the last pledge unless you raise the pledge budget"

Pledging options:
- one-time amount - people may be cautious, want to try once and see how it works first
- monthly plan - people who don't want to manually renew every month

One way to frame the mechanism — put in a lot less than one is able and have other people join you

mray: incentive to support many projects and a few select projects, contradictory?
wolftune: given limited resources, we want to encourage people to support FLO projects and projects that are most effective

mray: this relies on people following the project and keeping up with updates. Early adopters may have higher interest than future adopters, so they may not be representative audience for future adopters
wolftune: we don't want to encourage fire-and-forget. newcomers will find the project socially validated with an existing membership already supporting it

mray: Salt raised the question of how we should explain corner cases to remove people's doubts
Salt: what happens if a project has considerable funding growth in a short time? The threshold at the time is low, when it goes up, do a lot of people get dropped?

wolftune: recent proposal:
when user gets notification, "this month will be the last, will you like to add more funds?"
we still split up the dollar among the projects pledged, even though theoretically the amount paid should be higher

In the current design, user needs at least 3 months' at the current level in order to pledge
The total of your current pledges = x dollars/month
If user wants to pledge to another project, they can either drop a project or add more funds

opinion: I want there to be options later to enable people to continue pledging even if they don't have a lot of money. This is to not exclude people when a project gets popular and people get dropped. Bi-annual, annual donor?

mray: this needs to be very clear

wolftune: If project gets popular and some people don't have the means, don't worry, the project is still doing great — they can volunteer, support other projects

Salt: my concern, if jumping from 300 users to 3000 users and people get dropped, people may interpret "this project is failing"
wolftune: when user 3001 signs up (maybe person with the lowest pledge amount?) one patron will get a notification (people are dropped one at a time)
it won't suddenly drop off. By the time a wave of people have pledged, the threshold would have raised which will be displayed to new donors. Existing patrons will receive notifications of higher threshold.

there are some technical details to work out: calculation of who gets dropped when

smichel17 also raised the idea of showing the x dollars available to claim. However, there's the end-game problem, already knowing this fixed amount available. we don't want it to be a fixed amount, we want people to raise their pledge when they feel good about it

Salt, mray: I'd vote for just dropping the patron (rather than adding more options)
wolftune: originally we dropped the biggest project. in technical implementation, we found it's easier to split the last pledge to the various projects. There were 2 elements:
- give people 1 more month to participate, time to make decision
- technically more resource-intensive to do calculations. It was easier to proportionally split the amount among the projects pledged ("zeroing out")

mray: from the new user perspective, goal: distribute money among projects
I want to minimise cognitive load. Can we simplify it?

2 flows of money: 
- bank account to system
- distribution within the system

situations people want to avoid:
- don't want to empty bank account
- funds all taken by one project

### Dashboard mockup

MSiep: here's a "not holding funds"/monthly model I've been working on for dashboard mockups (see shared screen)
- if threshold is above maximum, pledge will be suspended
- instead of splitting equally among the projects, an algorithm to make best use of maximum until user intervenes
- another possibility is to split randomly

wolftune: it can't be random, things would be more weird. technically, it's easier to drop person from the same project that has a raised threshold. Dropping a different project will affect someone else's limit.

mray: I think MSiep's proposal can solve the problem. Simplify to a binary choice, support or not support. Instead of stop everything/do nothing, randomly assign something or closest assimilation of user choice

MSiep: another idea in progress, is show many pledges the amount can support across projects. There will need to be some buffer so user doesn't get suspended every 5 minutes as threshold fluctuates
A mockup would be helpful to see if the mechanism can be conveyed clearly to users

MSiep: if you understand what happens for a month, then you understand the model , e.g. $10/month, ongoing. the other variable: do this every month or one-time
wolftune: two options for one-time: authorising $10/month, however long it takes, or up to $10/month
MSiep: I prefer the latter. 

wolftune: originally donations were expressed as shares, x4 shares (represent 4 people). however, it was difficult to explain how shares translates to the amount people pay
MSiep: dashboard accounts for allowing people to donate a certain amount to a project
mray: it will take away the network effect by letting people decide amount they want to donate to a project
MSiep: people want their autonomy, so they'll try to get it in some way. it's just a matter of what options we give people
people have different relationships to different projects, some projects they might not want to donate as much. will we prefer they not donate at all, or $5-10 to the project. Something to consider for mvp

wolftune: I like the mockup
mray: I'm opposed to anything that complicates the pledge/unpledge button
wolftune: let's change the Change link
MSiep: when patron pledges, system will notify current maximum is adequate or prompt user to increase budget
wolftune: I like the line at the top, "x additional pledges could be matched before exceeding your monthly maximum"
mray: how is it distributed?
wolftune: it doesn't matter (10th of a cent/patron). it's the combination of people's increase that makes the difference to project funding
MSiep: a colour-coded comfort meter to visualise the maximum? What we want people to get the idea that maximum is greater than what user is actually being charged monthly, to allow for threshold to grow 
wolftune: I'd like to see an indicator of how much I'm helping in getting more people on board. As more people come along, the user's pledge increases. It's dollars but it's because more people join
MSiep: ideally, I'd like to make the dashboard self-explanatory so user can figure it out even without reading the how-it-works page

wolftune: an indicator on the website of a project I like with no. of patrons, click on it to go to snowdrift.coop website
MSiep: a humorous touch could be added, e.g. "you can make x no, of people give us money!"

### Other mechanism discussion

wolftune: technical question that came up regarding mechanism, is it feasible to drop people every hour? 
chreekat: there's no reason we can't do real-time evaluations

Post-meeting note from wolftune: with arrears it makes less sense to zero-out funds when underfunded, and thus we need to go with the previous suspended-pledge approach.

```

## Blog

```
chreekat: I'd like to not have it part of the site right now
Salt: yeah, that was a design decision mray raised earlier
chreekat: next step?
wolftune: it may be worthwhile to pay for hosting to free up dev time. okay with the tasks being assigned to me.
chreekat: I have to wait for blog to be done before I can push code to production. In the meantime I can continue pushing to dev.snowdrift.coop
Salt: we'll try to get to it soon
```

### Requirements

```
chreekat: hosted solutions strongly preferred
Salt: my only requirement — git .md files. 
wolftune: need footnotes
```

### Options

```
wordpress - ?
ghost - I haven't looked at it recently, but we could approach them for hosting (wolftune)
middleman -  suggested by Salt. Like jekyll, probably like hakyll
withknown - suggested by MSiep (found from indiewebcamp)
```

## Gluu

```
Salt: I'm concerned about the number of different tools with different accounts (website, wiki, blog, etc.) gluu.org is a single sign on (SSO) repository
chreekat: partners and volunteers can manage, but it's nice to have
Jason: so the question is whether or not to take the time to set up the infrastructure now vs. later? Is that the question?
chreekat: if we set up sooner we can connect apps to it as apps go live, but the sites already exist and would have to be migrated, so I can wait until later
Salt: not sure how Discourse integrates with SSO. They do have OpenID.
chreekat: it just sounds low-priority
Salt: Discourse is high-priority for me. We should discuss our common priorities.
Jason: did blogging and other things become MVP now? Am I just out of the loop? I thought we were focusing on the mechanism...
Salt: they're not MVP, but needed internally
chreekat: we don't want to destroy the blog content or spend sysadmin resources to host the blog
Jason: I guess my concern is that we're trying to set up the community before we have a product for the community to develop around.
Jason: Do we have someone who focuses more on the operations, so Bryan can focus on the dev?
Salt: not yet, we're stretched and could use more people
```
