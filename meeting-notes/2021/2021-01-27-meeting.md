<!-- See end of this pad for where to join, meeting-practices notes and reference -->

Meeting — January 27, 2020
=======================

1. Centering
--------------
### Personal Check-In Round
- Attendees: Salt, smichel, alignwaivers, wolftune, MSiep, bnagle17, iko, tannerd, mray
    - Leaving early: 

- Facilitator: Salt
- Note Taker: alignwaivers
    - Note-saver/pad-updater (if different):

### Previous week highlights
<!-- https://snowdrift.coop/p/snowdrift
<!-- https://community.snowdrift.coop/admin?period=weekly
<!-->

#### Metrics
- **Attendees last meeting:** Salt, smichel, msiep, iko, wolftune, alignwaivers
- **Snowdrift patrons:** 135
- **Discourse:**
    - Signups: 0 -> 1
    - New Topics: 0 -> 0
    - Posts: 4 -> 1
    - DAU/MAU: 25% -> 35%

#### Meeting Feedback
- smichel: ambivalent about staying to take notes instead of doing the org post, but it was useful to have notes. meeting went fairly well.
- wolftune: Distracted about as much as can be expected in circumstances. Thanks for note-taking and facilitation. I think not trying to cover every topic that we wish were covered is really good. Quality > quantity. Sometimes hard for everyone to understand each other. Maybe we could be more efficient, but this was within the norm. Talked through a topic, came up with next steps => productive meeting
- msiep: good to be here, thanks for notetakeing and facilitating.
- iko: retreat is a good idea, thanks for facilitation/notetaking
- Salt: appreciate everyone being here. Sad to see forum so quiet, excited about ???, see as one of the big social blockers
    - Got confused about not having next steps in one place together
    - Liked open discussion round, nice to ??
- alignwaivers: thanks for notes, tried to help kept getting disconnected, thanks for being here, facilitating

#### Facilitator reads over last week next steps

### 2 minutes of silence
<!-- Look over the agenda & think about what to bring up today -->

Reminders
----
<!-- Tensions, progress updates, blockers, questions
<!-- GitLab: https://gitlab.com/groups/snowdrift/-/issues
<!-- Forum: https://community.snowdrift.coop/
<!-- Personal notes/tasks/emails
<!-- NEXT STEPs — mark each as [DONE or CAPTURED] <LINK>
<!-->

### Nonprofit / org structure
> - NEXT STEP (align): post nonprofit options post on forum [captured]
> - NEXT STEP (smichel): post org structure/nonprofit options post on forum

### GitLab Cleanup
> - NEXT STEP (smichel): look into mirroring sdproto repo to gitlab.com/snowdrift/prototype [DONE]

### HedgeDoc
> - NEXT STEP (Salt): Try HedgeDoc next week

### HPP Blog Post
> - NEXT STEP (wolftune): review footnotes in HPP etherpad, approve for Salt to add to the post [DONE]

### OSUOSL Etherpad
- NEXT STEP (wolftune): Share pgp pubkey for getting admin credentials at https://trello.com/c/gAIXhDoM/23-etherpad [DONE]

### Planning for the team reatreat on Jan 31st
- NEXT STEP (wolftune): make a list of things that should be in the visualization and review with Tanner's demo ahead of the retreat [DONE]
- NEXT STEP (smichel+wolftune): Gather all the information about team retreat into one place & post it (use a timebox) [DONE]
- NEXT STEP (Salt): whenisgood between wolftune+tanner [DONE]
- NEXT STEP (Salt): confirm there is a BBB instance we can use for this. Mrays? [DONE]

### Personal Scratch Pad

#### alignwaivers
- been using hedgedoc, prefer color scheme parsing
- delayed posting about nonprofits but still planning on it

#### chreekat

#### iko

#### mray
- state of ui

#### MSiep

#### Salt
- Would like to get past roles roadblock, but lower prio than bylaws
- wants to put some time into being ready to facilitate sunday
- does anyone strongly desire to run the meditation bit, if not it won't be my first time

    - sounds like aaron will do this, can offer advice and such as wanted

- team building suggestions? I'll likely poke around various materials, but happy to take more

    - have some ideas, still looking for more 

- total lack of forum activity...
- retreat planning, still need to get some things in order, would like to put forum announcement, would also like to do 1-1 touches for rsvp

#### smichel
- Personal scratch pad is your section to put whatever you want in it
- moved gitlab issues to main sd repo
    - Anyone volunteer to take <https://gitlab.com/snowdrift/ops/-/issues/78>?
- Org/bylaws updated — probably not needed to discuss @retreat
- Meetings as a roadblock for new contributors
- Access controls as breakout?

#### wolftune
- OSUOSL forum upgrade schedule
- prep for Sunday

### tannerd
- share current progress with shiny
- get input on multiple goal behavior/additional features for visualization
- Link to current progress on interactive Model: https://tannerd.shinyapps.io/helloPublish/

2. Open Discussion (8 minutes)
-----------------------------------
<!-- Add breakout rooms as needed.-->

- [introductions]
- wolftune: fosdem is online

3. Breakout Rooms
-----------------------
<!--
### TOPIC <LEADER> (# minutes) Room ?
#### PLAN, GOAL
<!-->

### Retreat Planning <Salt> (10 minutes) Room A
#### What hasn't been decided upon, who can put their name next to things, what to include in a forum post
https://community.snowdrift.coop/t/blocker-retreat-january-31st/1700

- Salt: Not sure if we should decide topics/session in advance
- 1st session mechanism probably
- Salt: Input on team-building activities?
    - one of the proposals we had is to work on css transition that's blocking design -> production 
    - Could split into two groups
    - I'll message chreekat but is there someone else we'd like to get feedback from / invite (such as fr33domlover)
    - session two can be about coworking / hacking
- smichel: would David Thomas show up?
    - wolftune: in contact with him, maybe
- salt: should send a message to the board: what should be in the message
    - schedule. Invited to retreat as people (vs as board members)
    - in intros, I want people to share how people got involved
- wolftune: do we have a decided link?
    -   <https://bigblue.cccfr.de/b/mra-qy2-x6n>
- NEXT STEPS (smichel): get a dedicated pad set up for the retreat, share link to ???
- NEXT STEPS (smichel): ping board, adroit
- NEXT STEPS (Salt): ping chreekat
- NEXT STEPS (alignwaivers): send msg to fr33, davidak, mray
- wolftune: here's the topic this is the premise, figure out whether to break into groups:
    - setting the stage, thinking about how to best cowork on it all (some all-hands, some breakout room)
- smichel: probably going to boil down to figuring it out day of, especially during "breakfast"
- salt: have experience and ideas on this

#### Retreat Schedule (All times in PT, also a bit wibbly-wobbly)
05:00 AM : 1 hour : Arrival (breakfast, coffee/tea, hangout, sleeping-in, etc.)
06:00 AM : 15 min : Check-ins (everyone is logged in and ready to participate)
06:15 AM : 15 min : Guided Meditation (presencing, focus, centering, etc.)
06:30 AM : 55 min : Session 1a
07:25 AM : 10 min : Break (coffee/tea, snack, stretching, etc.)
07:35 AM : 55 min : Session 1b
08:30 AM : 30 min : Break
09:00 AM : 45 min : Session 1c
09:45 AM : 1 hour : Lunch
10:45 AM : 40 min : Session 2a
11:25 AM : 10 min : Break
11:35 AM : 40 min : Session 2b
12:15 PM : 15 min : Break (decompression, coffee/tea, snack, stretching, etc.)
12:30 PM : 30 min : Team Building & Reflection
01:00 PM : Retreat officially over


### How bnagle17 can help <smichel> (5 minutes) Room A
#### Get a sense of what bnagle would like to work on (wolftune, smichel, bnagle)
- salt: interested in writing roles, not super technical
- bnagle: my experience is mostly writing/editing. english major. Was doing admin work pre-pandemic, sorting spreadsheets etc. Freelance as journalist/reporter 

- smichel: can use help *organizing* gitlab too. have many issues on gitlab, not sure how up-to-date they are. checking which are duplicates
- Salt: another thing that would be nice is roles — sorting through links on a pad, pulling out useful information
- https://snowdrift.sylphs.net/pad/p/role-development
- alignwaivers: I think roles is probably higher priority, gitlab is quite convoluted
- salt: I sorted personal task lists on my board and it was really nice
- wolftune: one of the roles (prefer better name than coach/hr), could be checking in with people and their pertaining tasks, volunteer wrangling
- Salt: wiki & blog are also places that could use work, both organizing content & active writing for blogs
- smichel: Related, roadmap is here, could be higher-level: https://wiki.snowdrift.coop/planning


### Modeling with Tanner <wolftune> (# minutes) Room A
#### PLAN, GOAL
- tanner: was trying to work on visualizing difference between the 'two mechanisms'
  - Old demo: https://tannerd.shinyapps.io/helloPublish/
  - Current Progress: https://tannerd.shinyapps.io/mechDemo/
- wolftune: given inputs on pledge side, can demonstrate is that they can be equivalent at certain points.
   - next premise is if you mess with the inputs, the mechanisms diverge
- tanner: potentially a button that allows preset to match mechanisms at given equivalence
- wolftune: after seeing how this game works, once players change their strategy
    - can the goalsetting be adjusted to counteract the pledges?
- tanner:  can see how much one pledge changes the campaign on the new demo
- mray: creating new data points after each pledge?


### FOSDEM, worth attending? <LEADER> (10 minutes) Room B
#### review https://fosdem.org/2021/schedule/
- no one moved here, guessing not :P

### Access control list <smichel> (5-10 minutes) Room C
#### Chat about access controls w/ iko
- documenting in progress on ACL (who has access to what) https://gitlab.com/snowdrift/ops/-/merge_requests/7 and part of https://gitlab.com/snowdrift/ops/-/issues/48
- Salt: we should document the current state of things, in order to work towards what should be
- iko: I'll have a look at the smichel's MR comments and fix items, if they're out of scope of the MR then they can be pulled out into separate issues, thanks
- NEXT STEP (smichel): [wait for iko's feedback, then] open new issues or move comments on how things should be, fix then merge
- NEXT STEP (): discussion on backups @retreat (or separate ops meetings)


4. Wrap-Up (last 10 minutes)
-------------
### Report back: Summarize conversations, add notes if needed


### Meeting eval round: feedback, suggestions, appreciations
- smichel: overall went well, probably coulda ended open discussion earlier, didnt seem like we had too much to talk about but did have much to discuss when we got to it. During meeting on saturday, had a break and made topics to focus on return: maybe would be good to lean in that direction again (more time during open discussion and figuring out priorities)
- mray: +1, thanks everyone
- msiep: good meeting, thanks all for being here
- iko: good meeting, good to see people, good discussion
- wolftune: thanks everybody, think good meeting overall. Wondering about alternating (one week more dedicated to open discussion, another week no time for open discussion). Like the idea of working in breakout rooms more, perhaps a way to inspire people not interested in specific topics to work on their other tasks). thanks for facilitating and notetalking
- Salt: Don't remember the last time we have 9 people, happy about that. Still feel like some hangups in first section
    - Don't want to read anything until next steps, then it's jsut those that are marked as not done. Liked personal scratch pad, but need to delete my own (getting too long), no real mechanism to delete it
    - the meeting feedback wasn't edited enough down to just the take-aways that are applicable to the next meeting.
    - wish people could create named breakout rooms in mumble on the spot
    - Imagining mumble as a physical location, "coworking space, hangout space" where people can move between rooms if they're not implicated in discussion.
- tannerd: appreciate all the feedback i'm getting, interested to show another idea after meeting
- bnagle17: appreciate welcomes, hard to contrast with other meetings since it's my only meeting, good by default. Helpful to understand context. I have more catching up to do, some discussion I didn't understand (as expected)
- alignwaivers: good meeting etc +1, alternating between formats makes sense as we figure out what works best for us. Thanks for facilitation & collaborative note-taking.

#### Meeting Adjourned!


<!--
Meeting practices and tools
======================
Post-Meeting Tasks

    Clean up meeting notes

    Save them: https://gitlab.com/snowdrift/governance/-/tree/master/meeting-notes/2021

    Prepare this pad for next meeting:

    Move new attendee list and metrics to previous, leave new blank

    Replace previous meeting eval notes with new, and filter it down to essential points

    Clear the "last meeting next-steps review" section

    Move new NEXT STEPs to that review section and then clear out other notes areas:

    The snowshoveling check-in notes

    Agenda items

    keeping the topic title (but not topic leader/time) with the NEXT STEPs to review

    unbold the topic headers and add one # mark

    Open discussion notes

    Clear authorship colors

    Update next meetings date


Location

    Normal: Mumble server: snowdrift.sylphs.net port 8008 (mobile: half-duplex mode avoids echo)

    If we do video/screenshare: https://bigblue.cccfr.de/b/mra-qy2-x6n or https://meet.jit.si/snowdrift


Personal Check-in
- This is a place for everyone to get in sync with where everyone else is at mentally, not for Snowdrift updates.

Previous week review
- Facilitator reads/summarizes each section

Open Discussion
- Facilitator: make sure everyone speaks & that all outstanding NEXT STEPs are mentioned

Etherpad use
- Use chat in etherpad (and add your name)
- Option: audio notifications on firefox via https://addons.mozilla.org/en-US/firefox/addon/notification-sound/

Snow-Shoveling Discussion
- Rule of thumb: If anyone needs to speak twice (ie, if there is any back-and-forth), use a breakout room.
- Make sure NEXT STEPs are done or captured, marked with [DONE|CAPTURED] <LINK>

Agenda topics / Breakout Rooms
- As needed, ping !folks on matrix to read over anything advance, ideally before the day of the meeting
- Each topic facilitated by topic lead (?)
- Timeboxes: prefer 5 minute increments. Leave time for Wrap-Up.
- Rooms: First topic is 1A. Letter = room; parallel discussions get the same number.
    - Any template that's not filled out all the way — that topic goes last


NEXT STEPS
- Each topic should capture NEXT STEPS (or clarify that there aren't any)
- Should be clear and actionable
- Assignee agrees to capture or complete by next week

Timeboxing
- Timebox each topic, settled during agenda confirmation
- At timebox end, facilitator may choose to extend by a specific amount
    - Informal "thumb polls" inform the decision for extra timebox
        - "^" approve, extend the timebox
        - "v" disagree, move onto the next topic
        - "." neutral
- Use https://online-timers.com, start for each item, paste each url in the chat
- Quick links for here: https://smichel.me/snowdrift/timer/

Discussion options
- Open discussion
- Call for a round ("pass the mic" style, facilitator makes sure no one is skipped)
- Hand symbols
    - "o/" or "/" means you have something to say and puts you in the queue
    - "c/" or "?" means you have a clarifying question and jumps you to the top of the queue
    - "d" means thumbs up, encouragement, agreement, etc.
    - ">" means you understand someone's point, please move on
    - "d>" indicates feeling complete on an agenda item, ready to move on to

Notetaking
- "???" in notes means something missed, please help capturing what was said
- Aim for shorthand / summary / key points (not transcript)
- Don't type on the same line as another notetaker (ok to do helpful edits after but not interfering with current typing)
    - The "suggest changes" feature can help make edits without interfering
<!---->
