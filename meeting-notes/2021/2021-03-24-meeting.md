<!-- See end of this pad for meeting-practices notes and reference -->
![](https://pad.degrowth.net/uploads/upload_77445cab95fcc615d82149e0622dd4ab.svg)

Meeting — March 24, 2020
================================================================================
- Meeting location: Mumble
- Server: mumble.sylphs.net port 8008

1. Centering
---------------------------------------------------------------------------------------------------------------------------
### Personal Check-In Round
- Attendees: smichel, MSiep, Sam, bnagle17, Salt, wolftune, iko
    - Leaving early: 

- Facilitator: Salt
- Note Taker: smichel
    - Note-saver/pad-updater (if different):

### Previous week highlights
#### Meeting Feedback
- Could do better at tracking who's going to do the timing
- note the halfway point of time for a topic and warnings at like 2min and 1min, maybe just in the chat
- thought meeting notes in advance was helpful
- felt like a few voices took up a lot of the meeting space
-  was a bit confused about when open discussion was started vs reminder/next steps review. Maybe announce when going through reminders?
- would've liked to hear an update about elm (maybe a reminder to check in about this)
- [x] Update mumble on the calendar — it has an old url

#### Pad Changes
- Gitlab stats!

#### Metrics
<!-- https://snowdrift.coop/p/snowdrift
<!-- https://community.snowdrift.coop/admin?period=weekly
<!-- https://gitlab.com/groups/snowdrift/-/contribution_analytics
<!-->

- **Attendees last meeting:** smichel, alignwaivers, mray, Sam, MSiep, wolftune, iko, adroit: (8 total)
- **Snowdrift patrons:** 138
- **Discourse:**
    - Signups: 0 -> 0
    - New Topics: 2 -> 0
    - Posts: 4 -> 2
    - DAU/MAU: 26% -> 20%
- **Matrix (subjective):** About normal? (3-17 - 3-24 94 lines / 1225 words)
- **GitLab stats:**
    - Pushes: 16 pushes, more than 27.0 commits by 4 people
    - Merge Requests: 3 created, 1 merged. 
    - Issues: 12 created, 12 closed

#### Facilitator reads over reminders (except scratch pad)

###  Minute of silence
<!-- Look over the reminders & think about what to bring up today -->

Reminders
---------------------------------------------------------------------------------------------------------------------------
<!-- Tensions, progress updates, blockers, questions
<!-- GitLab: https://gitlab.com/groups/snowdrift/-/issues
<!-- Forum: https://community.snowdrift.coop/
<!-- Personal notes/tasks/emails
<!-- NEXT STEPs — mark each as [] and [x] when done add <LINK> when appropriate
<!---- Prefix of > indicates carryover from previous week
<!-->

### OSI Funds
- [] *  <https://gitlab.com/snowdrift/snowdrift/-/issues/566>

### General
- [] * Look into replacements or running fpbot on another server (iko?)
- [ ] Bring up 'about page' with bnagle
- [ ] open new gitlab issues for all todos related to wiki/gitlab split, and an epic to track them all (smichel)
- [ ] move "PLAN,GOAL" into stuff for facilitator to read (smichel)




### Personal Scratch Pad
#### alignwaivers
- perhaps could be useful to state # for rounds, helps track how many people are left?
- For next steps, I think differentiating bw captured and done *is* worthwhile

    - Also, strikethrough for next steps which are no longer needed perhaps?

    - [ ] ~~here is a task that is no longer necessary? or captured but not done~~

- salt: I like/use these:
    - [-]
    - [<]
    - [>]

#### Salt
- LibrePlanet

    - 3/20-21

    - we have a BoF, still need to give them a date!

    - potentially a talk, yes, still potentially...

    - https://pad.snowdrift.coop/p/libreplanetoutline

    - need to update slide deck, would be nice to have a short/long version ready to go for anyone

    - https://gitlab.com/snowdrift/design/-/tree/master/slide-deck-kit

- figure out roles pad progression?

    - https://pad.snowdrift.coop/p/role-development

    - still could use help with feature extraction before grooming/growing stage

- community engagement

    - people who absolutely have higher priority things to do shouldn't be volunteering

- work with athan on multiple versions of funding request but modified for researchers?
- civi...
- spring break/next quarter

    - supposed to be done-ish 20-28, but still need to do some thesis stuff

    - next quarter is looking to be required due to insurance

    - taking a class on development for social good and going to try to get them to accept snowdrift work for my time

- project for bnagle

    - was talking with iko and the closest to an intro page we can link right now is https://wiki.snowdrift.coop/about

    - but that is a bit overwhelming, we should have a primer that reduces the valuable info into ~5 pages max of reading

    - this seems like a great task for bnagle!!!

    - relatedly, bring up conv with smichel about focus of wiki on being like a wikibook


#### smichel
- Want to cowork on GitLab (issues) cleanup https://gitlab.com/groups/snowdrift/-/epics/2
- How can I can help w/ Elm prototype?
- <https://gitlab.com/snowdrift/snowdrift/-/issues/475> status: I think we decided not to pursue 501(c), correct?
- Gitlab progress: gov repo cleanup, issue tracker sidebar links


Open Discussion (8 minutes)
---------------------------------------------------------------------------------------------------------------------------

- [multiple people]: Some pad issues still
- Salt: I'm distracted, fsf situation is tragic, don't think the open letter is the right approach
- The board is an opaque entity without a way to step down. I don't want us to ever be in that position
- smichel: it's a paradox. rms is outspoken and also a defacto leader of FSF, his views may be seen as representative of the FSF. On the other hand, It's the attitude of "I see something wrong and speak out or take a stand on it" that facilitates free software movement.
- wolftune: we can discuss what lessons we can take away in formation of board structure

- smichel: https://gitlab.com/snowdrift/snowdrift/-/issues/475

    - are we still looking into it or close?

    - Salt: still worth looking into, but low-priority





Finalize agenda + Minute of silence
---------------------------------------------------------------------------------------------------------------------------
1. Finish adding agenda items, if needed
2. Each leader says what they want from their topic
3. Minute of silence (think about what you want to say)

Agenda
---------------------------------------------------------------------------------------------------------------------------
<!--
### TOPIC <LEADER> (# minutes) Room ?
#### PLAN, GOAL
<!-->

### bnagle primer project <Salt> (5 minutes)
- Salt: we don't have a good intro/"primer" page
    - Not for people who want to become patrons or the project, it's to introduce people to the world that we live in
    - We have the wiki-book itself, but that's really long
- bnagle: Sounds appealing to me
- wolftune: Are we talking about — as if Brendan is an independent journalist, describing the project as if encountering the project for the first time
- Salt:…some people don't even know what Free Software is
- wolftune: ???
- bnagle: This is the first thing we'd link to a newcomer
- Salt: Yes, this is the first thing I'd like to show somone
- wolftune: Or someone looks at the site, isn't sure, ???
- Salt: Maybe this is just the /about page…
- wolftune: wiki or main site?
- Salt: wiki. For context, I'm thinking of the wiki as a wiki-book, the think-tank activities
- wolftune: tie-in, brendan could help update the wiki to be more of a wiki-book
- Adroit: I was thinking the same thing, making an about page with links to other places to explore
- Salt: Doing that is something at the same time.
- bnagle: other projects/sites with successful/good pages to use as a model, that would help. "This would be perfect, if it were about
- smichel: my conception of this is is an updated version of the illustrated intro, which is quite out of date
- wolftune:  is bnagle comfortable with the process of working on it and getting feedback?
- bnagle: I think so, also monday coworking would be helpful for something like this
Next step:
    - [] start working on update to illustrated intro / wiki-book (bnagle)
    - [ ] send bnagle examples of other site

### Elm prototype status update? <smichel⇒adroit> (10 minutes)
- adroit: I made a lot of progress, 6-7 hours, biggest thing for me is to learn to use elm-pages, it's new to me (but simple enough)
    - If we're building pages in markdown, adding interactive bits inline is supported 
    - Adding styles to individual pages is a big step, I was looking into how we can get contributors/designers (e.g. mray) to take what's in penpot and make it real
    - You can see updates on the site now, or repo
- smichel: anything I can help with?
- adroit: right now I'm not sure where I will draw inspiration from, for what to build (since the prototype isn't fully up to date)

### FSF situation takeaways <wolftune> (10 minutes)
What from the fsf situation can we apply to our situation / avoid doing / learn from their mistakes?

No notes, but planned to follow up on this discussion & work on writing snowdrift policies on dealing with controversy.

One take-away: make sure our FLO advocacy doesn't come across as treating outsiders or those who disagree as necessarily being enemies

Also: really make sure to recruit for diversity¸variety of people in the Snowdrift community and leadership

Another: communication, trust and governance — inform, discuss and get feedback internally before a major announcement

Wrap-Up (last 10 minutes)
---------------------------------------------------------------------------------------------------------------------------
### Open Discussion: Summarize any breakouts, plan next-steps
<!-- Make sure next-steps are captured -->




### Sign-off round: what's next, appreciations, suggestions
- msiep: appreciate the discussion, everyone being here, note-taking
- iko: interesting discussion, appreciate Salt facilitating despite being tired. My focus: stats
- Sam: Nothing else from me; thanks all
- Adroit: Enjoyed diversity of topics in the meeting (some work, some talking)
- smichel:
- wolftune: I have OSI follow-up, feel like there's a lot I could do if I were putting in more hours on Snowdrift, some tension around that. Appreciated other voices, it was good to have a specific prompt "What do you think about what's been said? Are you following?" Good to figure out further prompts to best help people share their views (generic "any thoughts?" often only elicits "nope"). Suggest Salt prioritizes sleep :)
- Salt: Appreciate everyone being here, glad we were able to talk through stuff that's taking up a lot of my brain space. board mtg when? Tension around so much to do, not bothering people when they're doing improtant things, but that can backfire. Appreciate everyone speaking up
- bnagle: Next step, working on about page. Will reach out in irc or to individuals.

#### Meeting Adjourned!

<!--
Meeting practices and tools
Post-Meeting Tasks
- Clean up meeting notes
- Save them: https://gitlab.com/snowdrift/governance/-/tree/master/meeting-notes/2021
- Prepare this pad for next meeting:
    * Move new attendee list and metrics to previous, leave new blank
    * Replace previous meeting eval notes with new, filtered down to essential points
    * Clear the "last meeting next-steps review" section
    * Move new NEXT STEPs to that review section and then clear out other notes areas:
        - The snowshoveling check-in notes
        - Agenda items
            * Keep the topic title (but not topic leader/time) with the NEXT STEPs to review
            * Unbold the topic headers and add one # mark
            * Open discussion notes
    * Clear authorship colors
    * Update next meetings date

Location
- Normal: Mumble server: mumble.sylphs.net port 8008 (mobile: half-duplex mode avoids echo)
- If we do video/screenshare: https://bigblue.cccfr.de/b/mra-qy2-x6n or https://meet.jit.si/snowdrift

Personal Check-in
- This is a place for everyone to get in sync with where everyone else is at mentally, not for Snowdrift updates.

Previous week review
- Facilitator reads/summarizes each section

Open Discussion
- Facilitator: make sure everyone speaks & that all outstanding NEXT STEPs are mentioned

Etherpad use
- Use chat in etherpad (and add your name)
- Option: audio notifications on firefox via https://addons.mozilla.org/en-US/firefox/addon/notification-sound/

Snow-Shoveling Discussion
- Rule of thumb: If anyone needs to speak twice (ie, if there is any back-and-forth), use a breakout room.
- Make sure NEXT STEPs are done or captured, marked with [DONE|CAPTURED] <LINK>

Agenda topics / Breakout Rooms
- As needed, ping !folks on matrix to read over anything advance, ideally before the day of the meeting
- Each topic facilitated by topic lead (?)
- Timeboxes: prefer 5 minute increments. Leave time for Wrap-Up.
- Rooms: First topic is 1A. Letter = room; parallel discussions get the same number.
    - Any template that's not filled out all the way — that topic goes last

NEXT STEPS
- Each topic should capture NEXT STEPS (or clarify that there aren't any)
- Should be clear and actionable
- Assignee agrees to capture or complete by next week

Timeboxing
- Timebox each topic, settled during agenda confirmation
- At timebox end, facilitator may choose to extend by a specific amount
    - Informal "thumb polls" inform the decision for extra timebox
        - "^" approve, extend the timebox
        - "v" disagree, move onto the next topic
        - "." neutral
- Use https://online-timers.com, start for each item, paste each url in the chat
- Quick links for here: https://smichel.me/snowdrift/timer/

Discussion options
- Open discussion
- Call for a round ("pass the mic" style, facilitator makes sure no one is skipped)
- Hand symbols
    - "o/" or "/" means you have something to say and puts you in the queue
    - "c/" or "?" means you have a clarifying question and jumps you to the top of the queue
    - "d" means thumbs up, encouragement, agreement, etc.
    - ">" means you understand someone's point, please move on
    - "d>" indicates feeling complete on an agenda item, ready to move on to

Notetaking
- "???" in notes means something missed, please help capturing what was said
- Aim for shorthand / summary / key points (not transcript)
- Don't type on the same line as another notetaker (ok to do helpful edits after but not interfering with current typing)
    - The "suggest changes" feature can help make edits without interfering
<!---->
