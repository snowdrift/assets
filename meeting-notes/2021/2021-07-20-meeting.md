<!-- See end of this pad for meeting-practices notes and reference -->
Meeting info: https://community.snowdrift.coop/t/weekly-all-hands-on-deck-check-in-meetings/1010

Meeting — July 20, 2021
=====================
### Personal Check-In Round
- Attendees: Salt, alignwaivers, iko, wolftune, smichel, davidak, adroit
    - Leaving early: 
- Facilitator: Salt
    - Facilitation links: <https://smichel.me/snowdrift/timer/ timers and metrics>
- Note Taker: alignwaivers
    - Note-saver/pad-updater (if different): 

### Previous Meeting Appreciations
n/a

### Meeting/Pad Updates & Reminders
n/a

### Minute of silence
https://www.online-timers.com/timer-1-minute

### Metrics
- Meeting attendees: 6 -> 7
- Snowdrift patrons: 139 -> 139
- Discourse:
    - Signups: 0 -> 0
    - New Topics: 4 -> 0
    - Posts: 9 -> 4
    - DAU/MAU: 26% -> 17%
- Matrix (lines | words): 160 | 3733 -> 126 | 1574
- GitLab:
    - Git (commits | people): 24 | 4 -> 36 | 5
    - Merge Requests (created | merged): 2 | 3 -> 4 | 2
    - Issues (opened | closed): 7 | 96 -> 8 | 34

NEXT STEPS Review
--------------------
1. Plan how to deal with each incomplete step. [ ] is incomplete; [x] is complete.
    - Lines starting with '>' are a carried over from an earlier week.
2. Ask who would like time for post-meeting discussions.
3. Confirm Agenda length & order (including open discussion).

> ### Stuff smichel would rather someone else do :P
> - [ ] smichel: try putting in a half-hour writing a post without the team member list
> - [ ] smichel: reach out to david thomas about contract payment


Agenda
---------

### Open Discussion (Facilitator) - 5 minutes
- smichel: I've noticed we have a habit of not finishing stuff (maybe because too many things going on at the same time, get distracted easily). Really should work on getting stuff done and getting to the next thing
  - as much as I wanna focus on development etc, would like to finish up already assigned tasks first
  - let's focus on: https://gitlab.com/groups/snowdrift/-/milestones/17
- salt: you might be forgetting that sometimes there's a blocker
- smichel: but there are things on there that aren't blocked that we could focus on
- wolftune: there's the attention shifting cost and there's the cost of things being undone and also the balance of figuring out what to do etc
  - sometimes there's being tired and then getting pushed to make it happen
- davidak: for me the best solution is a kanban board
- wolftune: we *sort* of use this
  - I've been largely absent past couple weeks which is an unfortunate timing
  - is there a way to get caught up on what has happened?
- smichel: on gitlab, epics are the thing to look at in terms of high level overview. Other places will be noisier / may be less pertinent
  - would suggest removing notifications for some of the noisier things and just subscribe to epics
- wolftune: smichel, can you suggest notification settings that would be good for me?
- alignwaivers: in relation to figuring out things to do, it's nice to choose something that's easier to do, but that can be distracting. the key is to strike a balance
- [ ] smichel: walk through gitlab issue settings at thursday's coworking session

### Round for anyone who hasn't spoken yet (Facilitator)


Wrap-up (5 minutes)
------------------------
### Check that all NEXT-STEPS have someone assigned (Facilitator)

### Reflection Round — short, concrete, & specific
Clear on what's next for you? Meeting appreciations & suggestions?:

- Salt: happy about how this went through. I like if we don't have stuff to talk about there's tasks to be done. One suggestion, wolftune's intro is long, not necessarily bad
- alignwaivers: I love this format, good to see davidak¸good work everyone (lots to juggle)
- iko: good short meeting
- wolftune: pad is having a reload issue, maybe we could do video meetings? I'd like to be as close to that feeling as if we're all in the same room. I feel as though I haven't been able to be as present in part as a result.
    - Personal report ("how are things?") vs "how are you feeling right now"
    - happy it was a short meeting and glad smichel is figuring out how to do stuff and not necessarily needing insights from others etc
- smichel: (in response to wolftune) I've also been in individual contact with folks, didn't need to bring thigns to the full team
  - I've also had a tension about length of personal checkins, but wasn't sure what to do about it / didn't have clarity — I like that distinction
- davidak: nice to hear updates/progress and new website (looks kinda good), I think you are in the right direction, that there is consensus about things being short. to have more meetings with a more personal touch, you can try matrix video chat
- adroit: pass

Meeting Adjourned!

Post Meeting Discussion
----------------------------

- https://gitlab.com/snowdrift/snowdrift/-/merge_requests/209

- salt: iko: when civi is hosted we should figure out how to theme? (iko's note: this is for the web forms)

### Smichel tasks to be reassigned (athan)
- assuming they aren't too burdensome I'm happy to knock out at least one...
- update blog post is separate from the volunteer one:
- was going to try to get adroit chreekat david thomas and I in a 'room' 



### Maubot reminder plugin (install as-is for now or wait till no updated to reccur permanently
- note: subscription via 'thumbs up' may or may not be unncessary with !folks feature (or can borrow that code)

### insights setting up funding platform now for a FLO project (alignwaivers)
- have the option for fiscal sponsorship through music nonprofits *or* through open collective (which also can have fiscal sponsorship
- already have some degree of 'seed funding' though it is already to be allocated through the project, it might make sense (despite fees)
- they have the foundation



<!--
Personal Scratch Pad
Salt
- computing for social good class project
    - https://pad.snowdrift.coop/p/role-development
- need to update slide deck, would be nice to have a short/long version ready to go for anyone
    - 5-min version (newer): https://seafile.sylphs.net/d/bd5f906b1cff4e55b76c/?p=%2Fpromotional%2Fslide-deck&mode=list
    - longer version (old): https://seafile.sylphs.net/d/07dfe7aefa944abc95e1/?p=%2Fpromotional%2Fslide-deck-kit&mode=list
- work with athan on multiple versions of funding request but modified for researchers?
- civi... OSUOSL has been asking us for an update
    - CiviCRM 8.9.13 installation process test completed: https://pad.snowdrift.coop/p/civicrm-deploy

smichel
- Want to cowork on GitLab (issues) cleanup https://gitlab.com/groups/snowdrift/-/epics/2
    - With bnagle?
- Meeting meta ideas:
    - WIP: Update sections below
    - Chat in element?
### Gitlab notification settings and epics (smichel) - 10 minutes

Meeting practices and tools
Pre-Meeting Tasks
- Update metrics, from these links:
     - https://snowdrift.coop/p/snowdrift
    - https://community.snowdrift.coop/admin?period=weekly
    - https://gitlab.com/groups/snowdrift/-/contribution_analytics
- Add items to the agenda. Include a timebox & GOAL(s). Decide on order.

Post-Meeting Tasks
- Clean up meeting notes
- Save them: https://gitlab.com/snowdrift/governance/-/tree/master/meeting-notes/2021
- Prepare this pad for next meeting:
    * Replace previous meeting eval notes with new, filtered down to essential points
        - Leave re-writing for meeting-prep, but do remove routine "thanks all" comments & next-ups
    * Clear previous-week metrics; update this week's attendee count if anyone arrived late
    * Clear the "NEXT STEPs Review" section, unless they have next-steps which are not done/captured.
    * Move new NEXT STEPs to that review section and then clear out other notes areas:
        - The snowshoveling check-in notes
        - Agenda items
            * Keep the topic title (but not topic leader/time) with the NEXT STEPs to review
            * Open discussion notes
    * Clear authorship colors
    * Update next meetings date

Location
- Normal: Mumble server: mumble.sylphs.net port 8008 (mobile: half-duplex mode avoids echo)
- If we do video/screenshare: https://bigblue.cccfr.de/b/mra-qy2-x6n or https://meet.jit.si/snowdrift

Personal Check-in
- This is a place for everyone to get in sync with where everyone else is at mentally, not for Snowdrift updates.

Previous week review
- Facilitator reads/summarizes each section

Open Discussion
- Facilitator: make sure everyone speaks & that all outstanding NEXT STEPs are mentioned

Etherpad use
- Use chat in etherpad (and add your name)
- Option: audio notifications on firefox via https://addons.mozilla.org/en-US/firefox/addon/notification-sound/

Agenda topics
- As needed, ping !folks on matrix to read over anything advance, ideally before the day of the meeting
- Each topic facilitated by topic lead (?)

Timeboxing
- Prefer 5 minute increments. Leave time for Wrap-Up.
- Timebox each topic, settled during agenda confirmation
- At timebox end, facilitator may choose to extend by a specific amount
- Informal "thumb polls" inform the decision for extra timebox
    - "^" approve, extend the timebox
    - "v" disagree, move onto the next topic
    - "." neutral
- Use https://online-timers.com, start for each item, paste each url in the chat
- Quick links for here: https://smichel.me/snowdrift/timer/

NEXT STEPS
- Each topic should capture NEXT STEPS (or clarify that there aren't any)
- Should be clear and actionable
- Assignee agrees to capture or complete by next week

Discussion options
- Open discussion
- Call for a round ("pass the mic" style, facilitator makes sure no one is skipped)
- Hand symbols
    - "o/" or "/" means you have something to say and puts you in the queue
    - "c/" or "?" means you have a clarifying question and jumps you to the top of the queue
    - "d" means thumbs up, encouragement, agreement, etc.
    - ">" means you understand someone's point, please move on
    - "d>" indicates feeling complete on an agenda item, ready to move on to

Notetaking
- "???" in notes means something missed, please help capturing what was said
- Aim for shorthand / summary / key points (not transcript)
- Don't type on the same line as another notetaker (ok to do helpful edits after but not interfering with current typing)
    - The "suggest changes" feature can help make edits without interfering
<!---->
