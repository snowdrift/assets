<!-- See end of this pad for meeting-practices notes and reference -->

Meeting — January 31, 2020
=======================

Audio/Video Chat in BBB: https://bigblue.cccfr.de/b/mra-qy2-x6n


Schedule (in Pacific time)
------------------------------
5:00 AM : 1 hour : Arrival (breakfast, coffee/tea, hangout, sleeping-in, etc.)
6:00 AM : 15 min : Check-ins (everyone is logged in and ready to participate)
6:15 AM : 15 min : Guided Meditation (presencing, focus, centering, etc.)
6:30 AM : 55 min : Session 1a
7:25 AM : 10 min : Break (coffee/tea, snack, stretching, etc.)
7:35 AM : 55 min : Session 1b
8:30 AM : 30 min : Break
9:00 AM : 45 min : Session 1c
9:45 AM : 1 hour : Lunch
10:45 AM : 40 min : Session 2a
11:25 AM : 10 min : Break
11:35 AM : 40 min : Session 2b
12:15 PM : 15 min : Break (decompression, coffee/tea, snack, stretching, etc.)
12:30 PM : 30 min : Team Building & Reflection
1:00 PM : Retreat officially over


1. Arrival (1 hour)
----------
<!-- Look over the agenda & think about what to bring up today -->
<!-- Tensions, progress updates, blockers, questions
<!-- GitLab: https://gitlab.com/groups/snowdrift/-/issues
<!-- Forum: https://community.snowdrift.coop/
<!-- Personal notes/tasks/emails

- (place any discussion/thoughts/etc. that occurred pre-check-in here)


2. Check-ins (15 min)
--------------
- Attendees: Salt, davidak, wolftune, MSiep, Tannerd, iko, mray, smichel, chreekat, davidthomas, Adroit, alignwaivers, bnagle17
    - Leaving early: 

- Primary Facilitator: Salt
- Primary Note Taker: alignwaivers
    - Note-saver/pad-updater (if different):

### Metrics
- **Snowdrift patrons:** 135
- (add any you feel relevant)


3. Guided Meditation (15 min)
------------------------


4. Session 1 (3 hour 15 min)
--------------
### Breakout Rooms
<!--
#### TOPIC <LEADER> (# minutes) Room ?
##### PLAN, GOAL
<!-->

### Session 1a (55 min)
<!-- Add breakout rooms as needed.-->

#### Mechanisms Being Actively Discussed

- [initial mechanism](https://wiki.snowdrift.coop/archives/communications/formula) had shares, logarithmic matching, folks didn't understand and just decided on large numbers like 100 shares in order to give more.
- you don't want to give all you can right away.
- Decided to drop "shares", too hard to explain. Later conversations ⇒ too many decisions = bad
- So we went with a simple in/out decision
- The remaining tension was how to control budget in the unlikely case that the crowd grows enormously.
- Budget was set: if you hit your limit, you're not in the crowd any more, since you're no longer matching (no longer increasing your donation when someone joins) ⇒ complexity around who is matching or not.
- But the feeling of getting kicked out is uncomfortable.
- Summer 2020, got feedback from mozilla, both around the kicking out and the intuition "shouldn't the per-patron amount go *down* as the crowd grows?" but that implies that the project is fully funded with a growing crowd. The problem was the lack of emphasis on whether a project is adequately funded or not.
- msiep suggested that we have goal-based target, after which matching is turned off, and that provides both budget control and clarity that the donations are growing *because* the project remains underfunded.
- Salt: another problem — our mechanism doesn't work at "mozilla scale"
- wolftune: Yes, adapting to different size projects is another concern
- Today we have several suggestions for variations on msiep's goal-based idea. We need to narrow down our hypotheses, figuring out which questions we still have to answer with data, which parts we have concensus on
- Assumption: Latent potential for lots of people who might donate but are freeriding due to snowdrift dilemma. 
- We want to target the latent donations, not just shift people from Patreon to Snowdrift.coop
- Our goal is to reach people who are currently not donating to public goods and help them realise if there's a crowd doing it, we can overcome the issues. That's the premise/basic assumption.
- Having a goal allows us to have a budget without the "kicked out" issue.
- If it's clear everyone is working together, as long as we do that up to the point of the goal, then we can do that without getting kicked out of the mechanism.
- Can we agree that we're eliminating the part about "kicking people out"?
- ROUND:
    - chreekat: cool summary, I didn't know the story about how the mechanism discussion came up. I think that there is an orthogonal avenue of direction here. We don't have the data, but we could generate the data. I generated the current model, but it was a strategic error: spent a lot of time making sure the current model would work very reliably — what we need is a bunch of throwaway models and showing them off. 
    - davidak:  I'm for dropping that part of the mechanism as it doesn't work at scale
    - adroit: I'm a fan of ditching kicking people out. When David posted about whether crowdmatching makes sense, I agreed somewhat, think there are some core problems w/ it. Q about latent donors: if they are willing to give, but aren't b/c of the snowdrift dilemma, I would expect them to want to get something out of it / see that the snowdrift has been cleared, rather than just reducing the burden on the project — how much of the latent donor pool is holding back, & for what reasons?
    - smichel: I am also all for dropping the dropping. More thoughts later. What Aaron described is also how I understand the premise.
    - iko: +1 to not excluding people, it should be a cause for celebration, that they've helped bring the project to that point; they shouldn't be excluded.
    - msiep: +1
    - tanner: Came on board with this idea of project goals, think it makes sense.
    - mray: I agree, but also see goals as part of the mechanism too. Goals not just as means to removing limits, when msiep brought up goals, there's more — ??? scaling the solution, ties in to the flexibility to scale the system, ??? flexibility from the patrons. Lots more than just removing the limits.

    - Salt: +1 to everything above

- wolftune: what are the goals, who sets them, what options do we give to patrons when pledging?
- Project has at least one goal
- Patrons get to pledge
- What choices do patrons have?
- How are goals measured? Who sets them?
- What about changing them?
- Goal-setting: how will people actually behave in different scenarios?
- msiep's original focus: goals are a dollar (per-month) amount. Projects can express what it would mean to hit a goal. Once you hit that number of dollars, matching is turned off (unless you define a new goal).
- mray suggested a crowd-size goal.
- If we take our *current*, live, mechanism and add a goal, it doesn't matter which we use, the two are identical.
- If we allow patrons to vary their pledge amount, then …
- Salt (moderating): I want to get past this kind of framing sooner and looking at the proposals
- wolftune: There is some crowd/dollar size goal that results in the same $ for the project
- If you set a fixed number of patrons, & set a fixed final amount, the goal remains the same
- In another proposal, if new patrons join, the goal changes, and there is a back-and-forth where, as the number of patrons change, the goal changes
- Do one of these mechanisms affect the goal directly?
- Do we want people to pledge at different levels?
- How does this relate to what our mission is?
- mray: question of how to describe the goal is separate from the mechanism.
- Tannerd's latest interactive demo: https://tannerd.shinyapps.io/mechDemo/
- Salt: I think it's likely we'll have a decision by the end of the day, at least within a small set of options
- mray: maybe we can approach it from another direction. Start with what it takes to build it, how it works
- tannerd: I added a feature by request, if you modify the settings, you can paste a code to share it with others
- wolftune: given a starting point, with change in crowd and no change in goal, what you'll see is the projects get more dollars from patrons. Premise: if the patron's goal is to maximise the dollars to the project, at the least cost to themselves, there's more incentive to increase the pledge amount, vs. crowd goal, where they get more people to join.
- However, if the project has the capacity to change the goal before charging everyone, this effect can be counteracted.
- MSiep: this is complex, let's simplify the question. if the project says here's our goal of $x/month in order to hire a person to do y, how does that goal come across to people who are being asked to join the crowd, and which proposal makes more sense to people (comes to them more intuitively)? 
- wolftune: also, how does this relate to our values?
- MSiep: I think we agree that if we find a billionaire to fund everything. we're not going to be happy and call it a day. On the other extreme, we don't want large crowds that aren't making a significant difference, e.g. thousands of patrons only contributing a cent.
- Each patron would say, here's how much I'm willing to contribute as part of meeting that goal, e.g. $10k/month, and patrons say I'm willing to give $10. if the current total is 1/2 way to the goal, then donations are 1/4 way to the goal because everyone would be donating 1/2 of their pledge. The idea is once we get to the goal, the crowdmatching turns off and everyone donates what they promised.
- Adroit: I think the perfect middle is the quadratic graph someone posted, where the wealthier people have less impact. Having dIfferent pledge amounts is more risky, we should make everyone give the same amount.
- smichel: the purpose of crowdmatching is to test the hypothesis, I would like to continue with that currently
- Adroit: I wasn't talking about dropping crowdmatching, but whether people can have different pledge amounts
- wolftune: the tension is, if we don't let people limit or change pledge size, how do we know where to set it? The natural value of setting pledges is bottom-up, allowing crowds to determine what pledge size is right.
- MSiep: patrons specify what's the maximum they're willing to donate, whichever amount feels right to them. Like donating to a public radio (they don't say donate $10 a month or nothing, they take whatever people are willing to donate). What I was asking: does anyone think that the best long-term solution is to not allow people to donate or to require people to donate to the same amount?
- mray: I agree with MSiep
- Salt: this is the same vein as whether to allow multiple project goals, what we launch doesn't change those beliefs.
- davidthomas: (in response to MSiep's question) snowdrift may probably go with the latter. the simplicity we gain from people being on the same page might be worth more than the cost of not letting projects change the amount.
- chreekat: we should probably emphasise that snowdrift is not solving the problem of funding FLOSS, it is solving the snowdrift dilemma.
- wolftune: personally I hesitate to pledge in systems where I'm not sure what is the right thing to do — then I just don't do anything
- wolftune: one reason I like crowdmatching in concept is that I can turn over the decision of how much to pledge to the crowd and the project, that has an idea of how much money they need to be sustainable.
- mray: looking at it from a high level is good to not get lost in details
- We basically want people to be in a position where, for the amount they are going to pay, we provide an easy mechanism for just taking the right portion. 
- msiep: Wanted to bring up adroit's recent proposal. I find it helpful to imagine the literal snowdrift metaphor. How crowdmatching is done: someone passes around a signup sheet saying, "here's what you're agreeing to.".
- In the past: "I'll show up at 10am and do some shoveling; I'll shovel for 1 minute for every person who shows up"
- But really, I don't want to spend any time on it if the snowdrift is still going to be there.
- There is a simple version where you say, "I'll contribute *this* as part of a crowd that will get rid of the snowdrift, otherwise I don't want to shovel."
- Salt: dd. It's like gitlab: previously I didn't open issues because I had to think about where to open it. Yesterday I opened one b/c I didn't have to think about it.
- On the other hand, I get pissed off at gnome for not allowing me to make decisions. I want to balance that.
- To mray's point, we are the experts to some extent, and systems are not neutral. But maybe we can also *allow* people to change things.
- wolftune: "How much am I willing to give toward sustaining as-is; towards what it could be?…"
- I don't see it as fixed as some people — "here's how much I"m willing to give" — I think this can be pushed around a lot via social norms
- "honest pledge" - what are the inputs? money vs crowd goal, the only difference is what the inputs are for one or another.
- "honest inputs" - like when people vote for a candidate they don't like, for a better outcome
- Do people play the crowdmatching game honestly, or try to game it? 
- smichel: (referring to the last question) that's what I want to talk about
- mray: 2 levels… there's a problem we run into quite often.
- So far we've had mechanisms that influence how people behave. while allowing people to set their preferred amount. depending on how honest people are, really easy to run into that problem again if we don't simplify the process(?)
- wolftune: I want people to feel they can participate honestly, not have to analyze the game in order to participate
- Adroit: following on the snowdrift analogy, if someone already cleared the snow, then you go around collecting donations for thanks, is that still the snowdrift dilemma?
- chreekat: the wikipedia entry on the snowdrift dilemma also links to the chicken dilemma. the drift will get cleared, but who will be most desperate first?
- wolftune: yes, someone might go first, but there's the risk of burnout, it's not sustainable and the load should be shared. sometimes someone bringing a coffee for example makes a difference to encourage that person to keep going.

### Session 1b (45 min)
<!-- Add breakout rooms as needed.-->

#### Mechanism, continued (30 minutes) Room 1

- There's the goal in terms of our values and in terms of what the different goals do in incentivizing different inputs (goal setting and pledge settings)
- mray: question about how the goal is defined, this is a problem
- mray: projects being able to set goals might itself be a problem in terms of adapting the goals, projects will try to maximize their funding
- mray: I propose the patrons set the goals for themselves
- wolftune: discomfort with different goals, egalitarianism is awkward
- msiep: I feel it's important that the crowd is rallying around something, should be more specific than just the project in general, e.g. hire another person.
- wolftune: projects can express whatever goals independent of how the mechanism works
- msiep: connecting the goals to the pledge makes pledging a much more understandable and easy decision
- wolftune: how do we feel about both types of goals available?
- msiep and tanner: prototyping and feedback is important, critical to attract patrons
- tanner: mray seems right that there's a power dynamic between projects setting goals and patrons setting goals, but we can make sure changes in the middle of campaigns don't enable too much gaming, I think projects really can set goals well enough and it makes it much more tangible, I think the patrons setting goals fails to address the snowdrift dilemma as well
- adroit: projects do seem more qualified to set the goals
- mray: agree with the rally-around point from msiep, projects can create and define goals (plural), but let patrons still deviate from the set goals, they are just guidance
- adroit: patrons can still request goals from projects
- msiep: projects must keep goodwill with patrons, or else they lose them
- mray: the idea from Adroit to have multiple goals to choose from, this gives patrons more control
- wolftune: we should be careful not to devolve into bounties


#### Technical Blockers (30 minutes) Room 2

- smichel: we need to figure out how to get more people to contribute
- Salt: some of what you've asked for is css help. I have a friend who might be available to help, as long as he's not making decisions (e.g. what frameworks to use)
- smichel: I don't have the css background to evaluate
- chreekat: architectural decisions can't be made unless strategic decisions have been made. I think there's no tech advancement happening because there's no clarity on mission. It's fine to discuss the mechanism, but if we keep doing that then there's nothing to code. I would suggest having multiple iterations of the mechanism.
- Salt: good point. When we make a decision, we could have projects re-affirm.
- davidthomas: can have two "logical projects" for a single project, esp. as part of a migration?
- Salt: I've been in dialogue with Kodi's president, they're interested but hesitant because they haven't seen visible development progress
- We should also address/figure out the pipelining for getting from mockups to the live website.
- chreekat: random observations — I think we move too fast and far on designs. It's a major bottleneck and it doesn't need to be. We overextended ourselves on html/css.
- Salt: I think it's part of a symptom of not being able to iterate quickly on designs.
- smichel: chicken/egg problem. It takes 6 months to do the design because of an urge to get the design to a state close to perfect (because it takes a long time for changes to go live)
- Salt: what if we decouple the html/css from the mechanism?
- Haskell is a huge draw for us, but finding people who know haskell and html/css has been difficult
- davidthomas: one move is better documentation. It produces an artifact, where people who know the system better can then correct your understanding.
- smichel: there's a lack of visibility that might dampen progress on dev, so issues have been moved to one repo now
- chreekat: one thing to remember, open source contributors come of their own volition. Tech blockers can turn people off, but removing tech blockers doesn't necessarily mean people would contribute.
- What are the things you'd like to see changing in the code (in git commit history)?
- iko:

    - faster compile time for html templates and live reloading

    - sample data generator - to facilitate prototyping of new fields

    - server to preview development branches - to show ux/layout updates and get feedback

- smichel:
    - shorter compilation times / live reloading
- salt:
    - style/information updates
    - ui/ux for patrons
    - documentation updates
- chreekat:
    - merging the two repos, prototype to production


#### Technical Blockers, continued (Main Room)

- chreekat: the fact we're discussing merging two repos is a sign of overextending html/css
- iko: had connection issue, was trying to ask, what's the status of the "skin" implementation? There are issues open for porting individual pages from prototype to production, but it seems until the skin abstraction is done, those tickets can't proceed. Are there specific tech blockers for those right now or are we debating a decision whether or not to even proceed with that prototype, in light of new mechanism discussions that will precipitate new ui/ux iteration? If we're overextending on html/css, what is the solution? Should design wait?
- mray: I'm blocked on not knowing what the mechanism is for future updates
- chreekat: mray is blocked on not knowing what the end goal is. Unfortunately. the technology is blocked on not having a concrete first step. With software, you can't know the end goal without taking iterative steps. These two blockers are in opposition, so trade-offs must be made 
- mray: At the same time, we have a duty to clearly communicate where we're at / what we're planning to people who want to use the website now.
- wolftune: one way to frame it, release alpha + say "we're aware of these limitations"
- msiep: could be described in both ways


### Session 1c (55 min)
<!-- Add breakout rooms as needed.-->

#### Frontend mechanism, Room 1
##### Capture/clarify what next steps are needed in order to proceed

- wolftune: current mechanism: all patrons are $6 at 6,000 patrons = $36,000
- Currently phrased has the kickout mechanism: need to describe at that threshold there is no more crowdmatching. how far are we from describing that on the site
- mray: what's our time frame for this iteration
- wolftune: we should express this is our alpha state, and we are actively working on next version. We want to express what are the missing elements, acknowledging to the public we are aware there are specific issues i.e.:
- adroit: can't choose to contribute to only growth goals
- adroit: maintenence vs growth  goals
- alignwaivers: wanted to point out that multiple pledge levels doesn't seem as compatible with pledge per patron

ISSUES NOT ADDRESSED BY ALPHA (to be publicly acknowledged):

- Varying pledge options
    - Different patrons have different wealth / willingness
    - Different patrons have different relation / enthusiasm for project
- Varying pledges brings up question of the goal being crowd size vs dollar-amount
- Different project scales/ambitions: how are project goals set?
- Adjusting a single goal (might be partially in theory)
- Multiple goals (concurrent or sequential/stretch)
- The question of holding off funding until a threshold is hit (maybe as a patron option)

FRONT-END NEXT-STEPS for alpha: (will be an epic, make sure these issues are all captured and connected to launch)

- Express the pledge as $6 toward the $36,000 = 6,000 patrons goal, showing the proportional concept
- Express the meaning of $36,000 etc for Snowdrift.coop progress
    - Express the value of partial progress (multiple points along the way to the full goal)
- Statement to acknowledge (the primary) shortcomings/issues to be improved on in future iterations
- Patron dashboard view
- Project page
- Change the video
- Update how-it-works
- Express the mechanism *version* clearly (e.g. 1.0 or "alpha" etc)


#### Development, Room 2

- iko: what should we do about the overextending of html/css? Should design wait?
- Salt: and smichel, can you explain a little more about the "skins"?
- smichel: skins are the styles available. The plan was to create a new one with just the pages that are currently in the old one, migrate those pages then get rid of the old ones.
- chreekat: back up a bit, can we entertain the idea of whether to implement everything in yesod?
- smichel: we could have an api
- Salt: we need someone to head this, re-thinking how to position the frontend and backend. Is this a big ask?
- smichel: yes and no, it's a site generator after all. the main thing is to hook up the crowdmatch library to whatever we're using.
- chreekat: we're better off thinking how to implement the prototype ui as production, as a service. We can keep it no-js for the most part, but the backend can access a js api. 
- davidthomas: yesod can convert pages to static site. we could have the prototype compiled to yesod on the few places where we need the dynamic thing.
- Salt: there's a lot of tooling, the average developer may not have all of that.
- chreekat: while it would be great if people working on frontend didn't have to know what haskell was, it would be a big ask for us
- smichel: while I'm not as fluent with haskell, I'm comfortable hooking up the frontend with the backend
- Salt: the frontend/backend roles are so different but still have to meet somewhere, and decisions made in one could affect the other
- davidthomas: are we aiming to not use js at all?
- Salt: as long as we're coding the js (i.e. it is FLO), I don't have an objection to js
- davidthomas: the backend can be an api, the backend can do most of the work. At the same time, a lot of things can be hardcoded and caught.
- Salt: the next step is to make a job description for this and we want to make sure they're on the team because it is a major role.
- chreekat: how many pages are there in the prototype? How many are dynamic? Where is all the complexity?
- iko: around 14, 4-5 of them expecting dynamic elements (projects list, project pledge page, dashboard sections)
- Salt: is there a way to see the dashboard?
- iko: https://sdproto.gitlab.io/dashboard/now/ (ignore potential oddness with the sub nav menu)
- chreekat: too much happening. We should pare this down to 1/10th of the parts.
- Salt: I think it's already pared down for features, e.g. we still need a dashboard history
- chreekat: how about we start with a html table? Faster to implement, less inertia
- smichel: and it will unblock mray, to make a simplified design!
- iko: okay. However, I hope in the future we work out these questions as part of the pipeline/iteration cycle and whoever is doing frontend lead works with the visual and ux leads to determine the scope. This over-complexity scenario will keep happening if there's no dialogue during the design stage (before mockups or prototyping) about what are most important, which features are needed now in mockup vs. a little later.

#### Reporting back and takeaways

##### Room 2

- smichel: talked about the scope and why it's been difficult to have a 14 page site up in years. Why are we connecting the mechanism with the website. Started leaning towards the concept that they don't necessarily need to be connected.
- chreekat: to clarify, it was a discussion, no decisions were made
- adroit: makes sense to have a mechanism-agnostic website
- Salt: the 'pledge' app /login would be different though (pledge dashboard, etc)
- smichel: modifiying the design to be easier to implement might help unblock mray

##### Room 1

- what needs to be changed on the current site for launch. need to communicate / express the drawbacks of the current mechanism and that we are iterating etc.

- Salt: does this feel like progress?
- adroit yes
- mray: concerned that we will remain in this state
- wolftune: the small next steps we identified won't take for 5 years because it's doable
- msiep: + 1 to wolftune & adroit
- smichel: seems easy to switch/transition pledge 
- Salt: project goals need to be set
- wolftune: everyone has a $6 max crowdmatching needs to stop
- Salt: that's a backend task that needs to happen

- Salt (moderating): potential topics for after break?
- smichel: coordinating mray with coders to figure out how to improve ease of implementation
- wolf: Proj management, gitlab issues, recruiting / getting people onboard
- adroit: show off visualization
- Salt: recruitment — explicit write-up job description for front end architect (that may already be on the team) and this will help with broad recruitment


5. Lunch (1 hour)
----------

6. Session 2 (1 hour 30 min)
-------------


### Session 2a (40 min)
<!-- Add breakout rooms as needed.-->

#### Development, continued

- smichel: one idea that came up during the previous session room 2 was to bootstrap some css to be easier-to-implement 
- wolftune: so it would be easier to tweak (and know where to tweak)
- Salt: one of the hesitancies with this split is someone to own this task and be around as a volunteer

    - first person I thought of was adroit

- adroit: so the stack takes a long time to build
- wolftune: and the css is very messy
- salt: separating these two will help figure out how to move forward
- iko: agreed with Salt, wanted to add this is not only about technical issues of porting existing design work to production, but a decision-making/leadership question. Per chreekat's comment about over-complexity in the mockups and prototype, frontend lead role should work with design/ux leads to avoid this issue in the future.
- Salt: a bit of background on overextending complexity in the mockups. by the time of implementing, chreekat suggested paring down some of the elements
- Backend shouldn't have anything to do with the front end beyond an api call:
- From chreekat's perspective, the design has gotten outrageous. Because of design change it makes it all more cumbersome
- adroit: so a smart frontend?
- smichel: doesn't have to be smart front, some backend commandline that gets prompted, could happen with the service frontend handling the rendering
- salt: there's a javascript question
- davidthomas: from what I understand we need some js for the payment. The explanation should be visible with js disabled?
- smichel: need js to pledge credit card but not after
- davidthomas: could theoretically be handed a credit card and it gets entered on the backend
- Not a large portion of our audience that doesn't need js
- wolftune: seems like the javascript issue is more about alignment with our values, not a corporate open-sourcey thing. Not a blocker/requirement, just about having ideals.
- Whoever's going to step up and take ownership, they should have a lot of say on these choices
- davidthomas: possibility of limiting haskell to mechanism stuff, the rest of it being a service running periodically, e.g. database the point of integration and connect to a Django frontend
- Adroit: i have a lot of experience with elm (language kinda ripped off of haskell), have had a good experience with it - no runtime errors which is nice. It's fast, compiles to a large blob of javascript.
- (Adroit shares screen with elm visualisation) https://ellie-app.com/cf9w2GrXjkZa1
- Salt: part of what we're looking for is someone making decisions, e.g. where the css is living
- smichel: that has been the biggest blocker for me, learning and making css decisions pushes on the time I have to spend working it
- wolftune: the concern is less speed and what happens if someone has js turned off
- Salt: yeah, that may be a blocker — I would prefer not to go the site with js turned off and not be able to interact with it at all
- The thing we should be talking about designs going from mray's head to the main site, the connecting part comes later
- adroit: the reason I'm showing this is, let's say I make a change, then I'm able to hit compile. in yesod, same thing is possible in 1 second. 
- mray: how is the install process? is cross-compatibility is a concern?
- adroit: no, elm is batteries included
- wolftune: how do you feel about owning the implementation? of mockups by mray 
- smichel: there are elm to reactjs extensions/elm pages, any thoughts on that?
- adroit: there are a bunch of pre-renderers for elm, but none are built in, I haven't looked into it
- Salt: I like elm, but I'm not sure yet if it is the best route
- smichel: our approach has been progressive enhancement. 
- adroit: if you're going to have buttons, need some javascript
- Salt: if it's a button and it doesn't work because js is disabled, that's acceptable enough. I draw a line on a blank page.
- wolftune: someone could rebuild all javascript buttons with forms (someday)
- salt: again focus is designer to implementation. how easy would it be for designers to learn?
- Adroit: it's easy if you already know html, takes about a week to learn.
- mray: its a barrier to people like me who may be wary of learning a new language, even if its a just a small amount
- david: think we'd want to lean heavily into organization, each chunk of html could live in place where most html lives
- iko: elm's compile speed sounds great. I'm not sure, there would have to be some onboarding/training to help people learn how to do things? People unfamiliar might encounter the same problem as with yesod, how to add new pages and variables, and not knowing how to reach into the haskell/backend
- Adroit: the syntax to produce html isn't much different from regular html, it's using lists and brackets. The fragment that generates html can be moved to a separate file. There will be no need for designers to know about models, I will make the variables for them.
- alignwaivers (helps with moderating): there are 30 mins left, how much time do we want to spend on elm vs. evaluating other options?
- Salt: learning html syntax for elm is different than static site generators
- Mostly just html unless you're doing interactive stuff
- I don't want to end up in a place to have to teach the designer how to code/programming concepts
- A demo would be useful though
- davidthomas: elm has documentation, though it might also be a case of the grass is greener elsewhere. whether we'll have the ability to attract developers though is another question. We might be leaving too much on the table uncertain instead of going with something more mainstream, I don't know if that's really the case here.
- alignwaivers: there are some converting programs
- salt: frontend lead could decide on such a protocol
- (Adroit gives a demo on adding html elements in elm and takes questions about usage) 
- adroit: css can be split up however the designer wants. I would optimze everything to be grid based so they dont have to touch html at all
- mray: if I have a file how do I see it?
- adroit: elm sandbox, one command that can be run
- alignwaivers: if we go with this language what happens if you lose some availability?
- iko: would you decide to use vanilla css or a css preprocessor (and if so how would you integrate?)
- adroit: I'd leave that up to the designer which is a benefit. Adding a preprocessor could be done as a webhook, still no delay to worry about in compilation. I can add a line to npm, no problem.
- salt: yes, can leave up to designers but a decision needs to be made. 
  - As long as we aren't going over the line in the sand I mentioned then I'm very pro elm. as much as it is a niche, similar to how we are haskell. people who like elm are going to be aligned with snowdrift culture
  - The idea that it'll become a single person reliance isn't the worse prospect. Owning this space will mean developing for future people who will take the reins.
- adroit: very maintainable, some people lost their jobs because of this
- davidthomas: 2 points in favor, one against elm
- I think this is a grass is greener thing. With smaller ecosystem and limited contributers. this is not remedying the issues of haskell very well (but some degree of improvement because web dev in elm is greater than in haskell). 
- In favor of elm for this project: almost a subset of haskell - designers that want to make things more flexible has corollary with haskell, and vice versa
- mray: any guarantees we won't run into similar problems with haskell and the css preprocessor?
- Adroit: it'll be fine. Elm is very focused on webdev (too much sometimes, I want to use it for other things)
- salt: yes, more than haskell: bc of broader webdev community
- wolftune: the problem was hacked workarounds with haskell ecosystem especially for frontend devs
- Elm seems to solve a lot of these issues
- salt: this is a step towards the prototype and website become integrated
- Half of our css is in one form or another
- a friend had this to say about css:
    "So this CSS stuff… if it's just work translating from one system to another I can probably help out. I wouldn't feel comfortable advising on how to architect it though; my strongest reaction is that Snowdrift should just be using an existing UI framework, and it doesn't really matter which one. This stuff is deceptively complicated to figure out, can likely never be done satisfactorily, and is just really unimportant with respect to Snowdrift's mission."
- adroit: i agree, better to use something already existing 
- salt: do you have a css framework you are more comfortable with?
- smichel: the css on the prototype is the only css we care about
- wolftune: optimistic, especially connor's leadership on this. rapid development and prototypiing is exactly opposite of what we've experienced. if you could demo more and more how elm could implement, e.g. do one page first, people can see progress and be motivated to do more dev
- wolftune: since you didn't have opinions about css, do we want to recruit a specific css person?
- adroit: can't hurt
- Salt: I'm opposed to it, I think it's a step after this, and that becomes a blocker 
- How opinionated is it? a question in dev circles
- NEXT STEP (adroit): explore what we have, something like elm pages exploration & research
- NEXT STEP (adroit, mray): work with mray, select one page with changes he'd like to see; implement it as it is in the prototype, and then see if it can be updated by mray

### Session 2b (40 min)
<!-- Add breakout rooms as needed.-->

7. Team Building & Reflection (30 min)
-----------------------------------
- davidak:  I hope we get more work done on the mechanism, don't think we achieved much on that, just a little of the old discussions. But in general got some stuff sorted out. Wished we could get more done
- wolftune: wanna thank salt for organizing, everyone for showing up especially davidthomas after having been away for a while. Nice to have people with relative full stack experience, not just isolated interests. adroit not just here because he cares about elm but the values and ecosystem, allows for coherent conversation with big picture etc. Think we got about as we could. Can iterate on the mechanism, hard to decide when all theoretical, will eventually get much real world feedback etc. Excited to delegate more of the tech stuff away from me
- adroit: feel good, glad to see faces. Did think this was the mechanism retreat but that was only half of it. No bad feelings of progress in that regard. Time flew. 
- tanner: as new person in the community, grateful to Salt for organizing. List of next steps for mechanism was great and very useable. 
- alignwaivers: time frame was hard, but I really feel we got team improvement, that many people, seeing faces, we laid groundwork. More mechanism progress would be ideal, but appreciate Connor especially excitement about Elm, and the retreat was great, we should do another
- smichel: first annual snowdrift retreat went pretty well. Aaron succeeded at not interrupting, at least for 8 minutes there. I wanted to express my appreciation for Aaron, his ability to take criticism graciously is unparalleled. The meditation was great, you could do that professionally if you wanted. Thought it was okay to not discuss as much about the mechanism because it's not as much of a blocker as I thought it was. Nice to see faces (though it highlights a lack of diversity), impressed by bigbluebuttton. Let's put times on sections next time.
- iko: great retreat, thanks to Salt for organizing, terrific facilitating. Thanks to the notetakers smichel and alignwaivers. Glad to come to some concrete direction with frontend dev. Thanks to Adroit for taking the lead on it. Minor feedback on setup detail, the Breakout room timing could've been a bit longer so people don't get cut off, and maybe allow a bit of time for room switching/mic testing step. Thank you everyone for coming. 
- mray: nice to see faces, thanks to Salt for the idea. I would have liked more discussion space for the mechanism (and more outcome on that front), great to see everyone
- bnagle: agree with what others have already said, appreciate being able to see people's faces and matching to names. Was helpful for orienting to dynamics of how things work and looking forward.
- david thomas: thought it was good. definitely hoping things happen here. Appreciative of all of you being involved
- Salt: 8 hrs was a lot. This is amazing. A little confusion at the onset about it being a mechanism meeting, sorry about that, but the core unblocking was indeed the focus and super valuable for David Thomas being present. Splitting front and back end took so long for us to go ahead on, but this is transformational. So much to appreciate!


8. Retreat Adjourned!
--------------------------


<!--
Meeting practices and tools
======================
Post-Meeting Tasks

    Clean up meeting notes

    Save them: https://gitlab.com/snowdrift/governance/-/tree/master/meeting-notes/2021

    Prepare this pad for next meeting:

    Move new attendee list and metrics to previous, leave new blank

    Replace previous meeting eval notes with new, and filter it down to essential points

    Clear the "last meeting next-steps review" section

    Move new NEXT STEPs to that review section and then clear out other notes areas:

    The snowshoveling check-in notes

    Agenda items

    keeping the topic title (but not topic leader/time) with the NEXT STEPs to review

    unbold the topic headers and add one # mark

    Open discussion notes

    Clear authorship colors

    Update next meetings date


Location

    Normal: Mumble server: snowdrift.sylphs.net port 8008 (mobile: half-duplex mode avoids echo)

    If we do video/screenshare: https://bigblue.cccfr.de/b/mra-qy2-x6n or https://meet.jit.si/snowdrift


Personal Check-in
- This is a place for everyone to get in sync with where everyone else is at mentally, not for Snowdrift updates.

Previous week review
- Facilitator reads/summarizes each section

Open Discussion
- Facilitator: make sure everyone speaks & that all outstanding NEXT STEPs are mentioned

Etherpad use
- Use chat in etherpad (and add your name)
- Option: audio notifications on firefox via https://addons.mozilla.org/en-US/firefox/addon/notification-sound/

Snow-Shoveling Discussion
- Rule of thumb: If anyone needs to speak twice (ie, if there is any back-and-forth), use a breakout room.
- Make sure NEXT STEPs are done or captured, marked with [DONE|CAPTURED] <LINK>

Agenda topics / Breakout Rooms
- As needed, ping !folks on matrix to read over anything advance, ideally before the day of the meeting
- Each topic facilitated by topic lead (?)
- Timeboxes: prefer 5 minute increments. Leave time for Wrap-Up.
- Rooms: First topic is 1A. Letter = room; parallel discussions get the same number.
    - Any template that's not filled out all the way — that topic goes last


NEXT STEPShttps://wiki.snowdrift.coop/resources/meetings/2021/
- Each topic should capture NEXT STEPS (or clarify that there aren't any)
- Should be clear and actionable
- Assignee agrees to capture or complete by next week

Timeboxing
- Timebox each topic, settled during agenda confirmation
- At timebox end, facilitator may choose to extend by a specific amount
    - Informal "thumb polls" inform the decision for extra timebox
        - "^" approve, extend the timebox
        - "v" disagree, move onto the next topic
        - "." neutral
- Use https://online-timers.com, start for each item, paste each url in the chat
- Quick links for here: https://smichel.me/snowdrift/timer/

Discussion options
- Open discussion
- Call for a round ("pass the mic" style, facilitator makes sure no one is skipped)
- Hand symbols
    - "o/" or "/" means you have something to say and puts you in the queue
    - "c/" or "?" means you have a clarifying question and jumps you to the top of the queue
    - "d" means thumbs up, encouragement, agreement, etc.
    - ">" means you understand someone's point, please move on
    - "d>" indicates feeling complete on an agenda item, ready to move on to

Notetaking
- "???" in notes means something missed, please help capturing what was said
- Aim for shorthand / summary / key points (not transcript)
- Don't type on the same line as another notetaker (ok to do helpful edits after but not interfering with current typing)
    - The "suggest changes" feature can help make edits without interfering
<!---->
