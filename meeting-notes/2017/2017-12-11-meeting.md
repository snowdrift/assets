# Meeting — December 11, 2017

Attendees:  iko, JazzyEagle, mray, MSiep, Salt, wolftune

<!-- Agenda -->

# Alpha announce / blog
- wolftune: the core, core of alpha is the system working. I verified with Bryan payout is working
- I'd like to announce that we're basically at alpha, even if it's not a great UI
- mray: concerns — from a functional pov it's possible to announce, but skeptical about not taking any steps to assure quality
- wolftune: I plan to make the status clear
- Salt, MSiep: I agree with Robert
- MSiep: I can see telling people who are frustrated about progress, but I don't think we should be drawing attention when what they'll see isn't what we want, when we want to make a good impression
- wolftune: it won't be announcing publicly
- Salt: putting sysadmin hat on, even if you didn't mean to make a public launch announcement, we're currently not in the position to handle crowd influx when others get the word out
- maybe the server can hold up to it, but we're currently not tracking with analytics. there's a variety of testing things, usability-type testing that should happen before attracting more people to the site
- wolftune: you brought up some things that aren't being tracked at all (Salt: not sure what is being tracked/where)
- I want to figure out what is the minimal, minimal that we need for announce
- JazzyEagle: we want to be transparent, but we're missing the capability to be transparent. we don't have the 
- there's still some work that needs to be done before analytics are ready
- wolftune: we should make notes from this discussion and have an updated list
- Salt: it should be a group discussion to review the list
- NEXT STEPS: make notes from this discussion (on analytics, usability testing, etc.), and go over alpha milestone list on taiga

# Patreon
- Salt: I got a lot of messages about Patreon. they changed their payment fee structure
- I wanted to leverage this event, though we're not in a position to launch. I did link people to our wiki, etc. does anyone have thoughts?
- wolftune: they were pressured into changing fee processing to a perks/paywall system by people concerned about freeriding (pledging to get access to perks then cancel before the end of the month/payment time)
- <https://wiki.snowdrift.coop/market-research/other-crowdfunding#fn5>
- MSiep: I was listening to Sam Harris' podcast (he uses Patreon), it might be interesting to introduce him to snowdrift
- to paraphrase what he said, advertising underlies most of the problems on the internet. encouraging people to support podcasts if possible, though not if it's a choice between getting food and supporting
- Salt: any ideas to leverage this event?
- wolftune: we have people interested in snowdrift. we can explain the changes (i.e. Patreon is largely a paywall service) and link to the wiki. they can see that's not the solution they're looking for and can be open to learning more about snowdrift and the values it espouses
- NEXT STEPS: none really, just use consistent messaging asking people about it

# Project 2
- wolftune: thinking about partner projects, I think David Revoy might be a good person to approach to onboard for the next project. we can get feedback to adjust the site
- as well as being flo-aligned, he's an illustrator who makes a web comic using flo software <https://pepperandcarrot.com>
- Salt: do we want a project that's more value-aligned and smaller, or a project that has a higher project impact for release?
- mray: concerns. #2 project should have a significant role in targeting. I recall we don't aim at single persons. we're also under pressure to recruit a project with maximised impact
- looking at his Patreon page, there are 800 people supporting currently, we couldn't expect them all to move over to snowdrift. we can't ask him to leave Patreon to support snowdrift exclusively
- the chances aren't as high as larger projects like blender, gnome, etc.
- wolftune: I agree. however, we'll be having him as the first outside-project contact and giving an outside perspective
- getting gnome on board involves board and bureaucracy
- chreekat: wolftune summed up what I would've said
- it sounds like people's concern is choosing project 2. we're trying to have a small group of projects to invite in mind to give feedback
- Salt: my concern is whether we're at a good stage to be asking for that feedback
- wolftune: we can make a list of projects, and reach out to them to see what happens
- Salt: I have such a list. how many do we want to ask at a time? what still needs to be done on the site before we bring them onboard?
- chreekat: not something we have to worry about right now, we should have a list first
- mray: a project of a similar size would be 80 cents/patron, which isn't a very large total. people may see that and form their first impression of snowdrift's potential (poor publicity if performs worse than existing systems)
- chreekat: I disagree. Patreon doesn't solve the problem, snowdrift does. the problem is collaboration, snowdrift is to help projects grow
- the point of reaching to someone like David is part of figuring out how to make snowdrift successful for people like him, with the capacity for success
- mray: not sure we'd achieve better results than Patreon in starting like this, given they have kickstarted the funding
- wolftune: I'd like to get David's perspective anyway, whether he'll actually be onboarded
- mray: sure, I'm more concerned about the impact of attention
- wolftune: there's Salt's idea of having multiple projects that signal different things, e.g. art, software
- NEXT STEPS: consolidate our list of partner projects, specifically to even consider reaching out to

# Discourse
- Salt: Bryan, any update?
- chreekat: as of today, I'm no longer travelling, I'll put an hour/day for snowdrift for a month. finishing discourse with nixops today
- it should only take a few hours
- NEXT STEPS: chreekat finishes nixops configuration

# Other infrastructure
- Salt: any update on email setup?
- chreekat: our infrastructure needs tending to (running ubuntu)
- I'd like to put more things on nixops, doing some architecture structuring.
- Salt: do we want to recruit someone for sysadmin stuff?
- wolftune: we'd welcome someone showing up, but don't think we need to actively search?
- Salt: I have someone in mind, but may entail drastic medium-term changes (they need fedora; neither Salt or chreekat do fedora). just putting forward that option.
- wolftune: re: email setup, I sent another email to Kolab, waiting for reply
- NEXT STEPS: waiting for Kolab response

# Bitcoin
- wolftune: we received some btc a while back, David picked up the btc wallet earlier this week
- we can pay Bryan money owed, and some other expenses
- chreekat: the people I know who do btc, the general consensus is to not sell right now
- wolftune: we could sell some of it for expenses
- NEXT STEPS: wolftune relays views to davidthomas and determine what expenses being held off that would be worth investing in


# Carry-overs

```
Carry over: librejs payment processing
Assigned: wolftune: email rachelfish info from pj and crowdsupply etc.

Carry over: tools for managing outreach
Assigned: Salt: Salt decides this stuff, pings wolftune

Carry over: initial board
NEXT STEPS:
- plan board meeting, set available time, contact everyone
- gather list of resources and advisors to contact
- reach out to advisors with reasonable set of questions

Carry over: update on the video
- wolftune: still working on the music
Assigned: wolftune: final audio recording done

Carry over: legal email, discussion, etc. from <https://wiki.snowdrift.coop/resources/meetings/2017/2017-08-11-meeting#next-steps-3>
- wolftune: no progress yet. it's about how to run co-op representation, and other issues.
Assigned: wolftune

Carry over: Holacracy thoughts: smichel: make report about what we’ve got so far, what seems to be working, and what we would need to do to go further
Assigned: smichel17

Carry over: code policies, specs for code modules
Assigned: fr33 will talk to chreekat, get him to document his criteria/policy for code merging and onboard other maintainers; also mechanism specs written out

Carry over: add notes from wolftune’s feedback to Salt about his SeaGL presentation to appropriate wiki page with presentation resources/ideas
Assigned: wolftune

Carry over: Salt describes a process for how we’ll use CiviCRM to enter and tag prospective first projects
Assigned: Salt
```