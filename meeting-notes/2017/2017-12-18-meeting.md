# Meeting — December 18, 2017

Attendees:  chreekat, iko, jazzyeagle, wolftune

<!-- Agenda -->

# New work on UI?
- chreekat: does anyone know the status? are we still implementing mray's first version? what's outstanding?
- wolftune: anything visual design is mray's domain, anything already done. if it's trivial enough to do, we should probably do it
- using the command you gave to identify items, maybe working with mray on it
- anything new we go through the new US process we've been doing to get consensus, then open up issues with one independent US
- come up with implementation plan on that US
- it would be ideal to do a review of where we are post-reset, look at old commits and open issues to move them back in. does that make sense?
- chreekat: a possibility, but I'd rather start with mockup and backforming US, figuring whether or not we're still doing which US
- wolftune: I think there are problems with backforming from mockups, we don't want to focus on working that way as the working process
- chreekat: in that case, people need to generate US
- wolftune: we can use the gathering specs tag you made
- chreekat: fine, just talk to me about any new tags related to the code repo
- NEXT STEPS:
    - draft US for consensus; assigned: anyone, mainly Robert, Michael, Bryan, Aaron
    - make issues for implementing individual US that have consensus; assigned: Bryan, Aaron
    - update tags (which we have and add to issues appropriately); assigned: Bryan

# Grants?
- chreekat: I don't know about sourcing grants and applying for them to work on snowdrift. anyone help/advice?
- wolftune: <https://wiki.snowdrift.coop/market-research/grants>
- chreekat: this is a good resource, if you have anything more to add to it
- wolftune: I have a list of 40+ groups from Karl Fogel, I'll have to find the task about reviewing them for inclusion to the wiki page
- wolftune (post-meeting): chreekat, found the longer list: <https://tree.taiga.io/project/snowdrift-outreach/issue/531>
- NEXT STEPS: send links to groups like FLOSS Foundations to chreekat; assigned: wolftune

# Shut down old discourse
- chreekat: has it been done?
- iko: not as far as I'm aware, Salt and I have been busy
- NEXT STEPS: shut it down; assigned: chreekat

# Co-working
- wolftune: I have a giant backlog, but not something to offload here at this time
- NEXT STEPS: n/a

# Development next steps?
- JazzyEagle: what are our next dev steps? it seems we have some cleanup to do, but what are our next steps after that?
- wolftune: there will be issues opened in the US
- once people in dev and design agree to do it, then there will be issues opened to formulate implementation plan
- JazzyEagle: have you decided on the type of US you're asking people for?
- chreekat: I'd like to see US related to UI and UX. all that work done on UI before, I want to bring that back in as soon as possible
- I really want the group overall focus to be on the design
- we're going to focus on the priorities that mray and MSiep think are priorities
- e.g. looking at the project page and history. one thing that's missing is email notifications. chreekat, do you think we should have a US for that?
- chreekat: that is definitely part of UX even if it's not a UI part of the website. if you think that's high-priority, then you should write that US
- wolftune: I'll write the US, show it to mray and MSiep, then we discuss it
- JazzyEagle: chreekat, should I continue with the backpack work, or wait?
- (Background: backpack is a framework that enables templating and modularising code, will become useful for crowdmatch mechanism as well)
- chreekat: you should continue, it's a good separate feature from design stuff to work on. figuring that out will help us write better tests
- JazzyEagle: I may need your help once backpack is set up, since you mentioned modularising the stripe runner
- chreekat: if you can put it on a sample project with no dependencies, it'll help me understand how it works
- NEXT STEPS: figure out backpack stuff ; assigned: JazzyEagle

# Code process / specs / docs
- chreekat: I'd like to get Haddocks easier for people to read
- maybe that means putting snowdrift on stackage
- tests *are* specs and could show up in Haddocks
- CONTRIBUTING.md is long, and maybe need to split it up and focus more on the "how to make good MR" per GitLab / GitHub standards
- NEXT STEPS: update / split up CONTRIBUTING.md, document haddocks etc.; assigned: chreekat

# Carry-overs

```
Carry over: Alpha announce / blog post etc
Assigned: who? go over alpha milestone list on taiga, clarify prereqs (analytics, usability testing, etc.)

Carry over: recruit projects
Assigned: wolftune and/or Salt: consolidate our list of potential partner projects

Carry over: tools for managing outreach
Assigned: Salt: Salt decides this stuff, pings wolftune

Carry over: Discourse w/ NixOps
Assigned: chreekat: finish setting up NixOps

Carry over: email hand-off
Assigned: wolftune: follow-up with Kolab

Carry over: initial board
NEXT STEPS:
- plan board meeting, set available time, contact everyone
- gather list of resources and advisors to contact
- reach out to advisors with reasonable set of questions

Carry over: update on the video
- wolftune: still working on the music
Assigned: wolftune: final audio recording done

Carry over: legal email, discussion, etc. from <https://wiki.snowdrift.coop/resources/meetings/2017/2017-08-11-meeting#next-steps-3>
- wolftune: no progress yet. it's about how to run co-op representation, and other issues.
Assigned: wolftune

Carry over: Holacracy thoughts: smichel: make report about what we’ve got so far, what seems to be working, and what we would need to do to go further
Assigned: smichel17

Carry over: add notes from wolftune’s feedback to Salt about his SeaGL presentation to appropriate wiki page with presentation resources/ideas
Assigned: wolftune

Carry over: Salt describes a process for how we’ll use CiviCRM to enter and tag prospective first projects
Assigned: Salt
```