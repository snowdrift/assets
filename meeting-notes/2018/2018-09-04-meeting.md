 <!-- Previous week's meeting notes: https://git.snowdrift.coop/sd/wiki/tree/master/resources/meetings/2018 -->

# Meeting — September 4, 2018

Attendees: msiep, Salt, smichel17, wolftune

<!-- Check-in round -->

## Carry-overs from last meeting

```
## tracking ops tasks for just server maintenance
- NEXT STEP (Bryan): Review/capture ops priorities [chreekat, in his own list]

## Better visibility for weekly meetings (esp. including inviting people who aren't core contributors/team)
- NEXT STEP (Stephen): post a discourse announcement about weekly meetings <https://git.snowdrift.coop/sd/outreach/issues/40>

## Governance overhaul
- NEXT STEP (wolftune): posts on discourse about things he has personal notes on governance roles.

## Bryan/Stephen meeting — CI/CD improvements
- NEXT STEP (Bryan): write blog post about this process, document <https://git.snowdrift.coop/sd/snowdrift/issues/111>

## Volunteer recruitment
- NEXT STEP: Wait until ~end of September, to decide whether we want to open up option of moving off of haskell for our web framework.
- NEXT STEP (Aaron): Include this "call for frontend/backend split help" in discourse announce [added to draft: https://community.snowdrift.coop/t/please-join-us-on-the-snowdrift-community-forum/567]
```

## Metrics

Discourse (over past week):

- Signups: 2
- New Topics: 4
- Posts: 24

Sociocracy:

- Page 108

<!-- New Agenda Down Here  -->

## Open Collective
- smichel17: 
- wolftune: Probably the right strategy is to get a board together and then discuss outreach to Open Collective with the board
- wolftune: I'm certainly open to if someone wanted to take initiative on reaching out to them
- Salt: I want to clarify that I think we're separate entities; I do want to reach out to a contact I made
- wolftune: tangential, I'm following the GitHub thread where Liberapay is dealing with their payment stuff https://github.com/liberapay/liberapay.com/issues/1171, we can likely learn from whatever the outcome is

## CoC etc
- wolftune: I think I'd like to re-assess our CoC before the major announcement of forum (blog posts, announce list)
- wolftune: I don't want to delay that much; thinking of a partial announce (eg, to design list), then update, then major announce
- wolftune: Questions / thoughts / information you want from me?
- wolftune: Social.coop was in a situation where, if they'd had a better CoC with conflict-resolution focus, they could have avoided much/most of the unfortunate blow-up of tensions.
- wolftune: I got roped into drafting some stuff for them. I now more strongly recognize the sorts of people who will tag us with unwarranted assumptions if we don't have certain signals in our CoC.
- wolftune: (Especially to Salt): Do you agree we should revisit the CoC before major discourse announce?
- Salt: Yes, let's talk about it before announce — CoC should be considered *part* of finalizing legal terms / ToS etc.
- NEXT STEP: Schedule co-working session with wolftune, Salt, whoever else.

## Project outreach, Kodi etc
- Salt: What are our current blockers to reaching out?
- wolftune: Do we have any? We have an issue for it?
- smichel17: I searched and found:
    - Solliciting feedback from potential projects <https://git.snowdrift.coop/sd/outreach/issues/41>
    - Process for tracking first projects in civi <https://git.snowdrift.coop/sd/outreach/issues/11>
    - Add info for interested projects to the wiki <https://git.snowdrift.coop/sd/wiki/issues/1>
- smichel17: Do we *need* outreach#41 done before we do more outreach?
- Salt: Yes. We've already started the dialog; we need something prepared for the next step.
- NEXT STEP (Michael): work on feedback form <https://git.snowdrift.coop/sd/outreach/issues/41>

## Prioritization
- wolftune: quick question: how much should I prioritize video follow-up, like blog post, putting up audio source files under a FLO license, etc
- Salt: Doesn't sound like a priority.

<!-- Decide where to capture outstanding tasks -->
<!-- Closing round -->
<!-- Capture TODOs -->
<!-- Add meeting notes to wiki -->