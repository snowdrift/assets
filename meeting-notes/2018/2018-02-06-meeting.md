# Meeting — February 6, 2018

Attendees: chreekat, iko, MSiep, wolftune, smichel17, Salt, fr33domlover

<!-- Agenda -->
# Responsive web design (rwd)
- MSiep: I asked iko about it on irc, I wanted to check we're not doing things that make it more difficult to incorporate rwd
- the other thing is how much it is necessary before we want to get a bunch of people's attention (priority)?
- Salt: mobile is important
- wolftune: I don't have a strong opinion about it, if everyone wants to do it, go for it. any pushback?
- chreekat: I'd rather see mobile-first design. start with mobile then move to desktop
- MSiep: in other words, if we can't do responsive design, then design to make things work for mobile first
- smichel17: I'm not likely the one implementing, so even if I had concerns about responsive (I have no strong opinions), I'd defer to others
- NEXT STEPS: get mray's feedback before proceeding

# New process so far
- MSiep: I wanted to check in with mray on the process, but he's not here today
- wolftune: I spoke with him, he's okay with trying the process
- NEXT STEPS: https://git.snowdrift.coop/sd/governance/issues/25 (add them here if there are any new steps)

# Discourse
- *(agenda item was added by wolftune)*
- Salt: it's currently not used except by a few people. the rollout plan is to have a small group work on it
- then others on the team start using it actively for a few weeks. next, bring in people from the mailing list and irc
- before rolling out to the public
- there was a hiccup: a thread was started on a category where posting was not intended
- there are currently 5 parent categories. the way it works is the topics in subcategories show up in the parent categories
- it was set up so that people won't start topics in the parent category (only in subcategories)
- so when a topic is started there, people can't reply. how do people feel about it?
- wolftune: I would defer to interaction design, but I don't see why topic starting should be blocked in parent categories
- is there any benefit for people to post in subcategories instead of posting in General (without choosing a subcategory)?
- smichel17: do have enough categories or can they moved to top-level?
- Salt: the advantage of subcategories is allowing people to subscribe to specific categories they're interested in, it's based on the mailing list format
- wolftune: everything in clearing the path is about getting snowdrift running, whereas people might have other topics like FLOSS, economics, etc.
- that they're welcome to discuss in another section
- I don't want to want people discussing design, dev, etc. get mixed up with the economics and broader topics
- whichever way we go is fine, as long as we're consistent
- NEXT STEPS: defer discussion and decision on categories structure for a later meeting

# CiviCRM
- wolftune: there are some tensions regarding setup/usage, but no particular need to discuss here
- checking if there are any status updates?
- Salt: I don't have any updates at this time
- NEXT STEPS: wolftune and Salt discuss tensions related to usage

# Code status update?
- wolftune: chreekat, status update?
- chreekat: my next step is to polish an execution plan as soon as this meeting is over (now that I'm no longer sick, and settled on a place)
- there was some experimentation going on for the frontend (html and connecting them to the backend), simplifying the crowdmatch library
- Some promising, some less; included in execution plan
- iko: is hamlet still being used for the foreseeable future?
- chreekat: yes. nothing that has been worked on in the last few weeks is changing anything. I'm interested in getting information about possibilities / tools
- it's all going to get thrown away when the experiment's over
- fr33domlover: btw I'm going to write a charade-for-hamlet ^_^. well, try at least
- salt: noticed you sent out a great [status update] email a while back, anything planned for the future? 
- chreekat: yes, there will be an email (the execution plan). I want to publish it to the ML or Discourse
- NEXT STEPS: chreekat sends an email about the execution plan

# MitchellSalad's Haskell friends (could be 1-on-1 with smichel17 and chreekat)
- smichel17: initial project: GHC?
- I met up with MichellSalad and friends last weekend. one of his friends, Ian, suggested getting a haskell project on board
- because they'd be excited about snowdrift for more than a crowdmatching platform, and because we may get some devs interested in helping
- wolftune: interesting idea. we can capture this idea in civicrm? (we've been tracking this in the wiki)
- Salt: agreed
- NEXT STEPS: add idea to civcrm or wiki?


# Carry-overs

```
# LFNW
- NEXT STEPS: reach out to OSI about LFNW table etc.
Assigned: wolftune

## Board: organise the process, reach out to people
- NEXT STEPS:
- plan board meeting, set available time, contact everyone
- gather list of resources and advisors to contact
- reach out to advisors with reasonable set of questions
 Assigned: wolftune
 
## Roadmap
- NEXT STEPS: make archive wiki page with links to old discussions that might be useful at some point, so wolftune will be happy (smichel17) 
Assigned: wolftune, smichel17

## Discourse
- NEXT STEPS: create discourse topic about moving from mailman to discourse (and send a link to it to the relevant mailing list?)
Assigned: smichel17

## Legal email, discussion, etc. from <https://wiki.snowdrift.coop/resources/meetings/2017/2017-08-11-meeting#next-steps-3>
Assigned: wolftune

## Describe a process for how we’ll use CiviCRM to enter and tag prospective first projects
Assigned: Salt

## Alpha announce / blog post
- NEXT STEPS: go over alpha milestone list on taiga, clarify prereqs (analytics, usability testing, etc.)
Assigned: wolftune writes blog post

## Recruit projects
- NEXT STEPS: consolidate our list of potential partner projects
Assigned: wolftune and/or Salt
```