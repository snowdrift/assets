Thank you so much for sticking with us and making your first crowdmatching donation of $8.20! It should now be shown on your dashboard at https://snowdrift.coop/dashboard

Do you have any feedback about your experience with this donation?

Please let me know if you can volunteer time to help us get fully launched, so we can enable crowdmatching for other projects. Also, if you can engage on the forum, Matrix, or otherwise, that will help by bringing energy to the project.

- Aaron
