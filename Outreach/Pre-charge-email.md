SUBJECT: (Action requested) Snowdrift.coop running first crowdmatching donations! 

Hi,

You are one of 161 active patrons at Snowdrift.coop, and we are finally starting the first round of crowdmatching charges!

I imagine you might be surprised to hear from us after such a long time.

The shortest summary is: We have not given up. We are still moving ahead. More about the status of the project later. Because the delay has been so long, we want to confirm your participation and your pending donation (a few dollars) before we charge your credit card.

Are you:

- Willing? Please (a) review your crowdmatch progress and pending donation at https://snowdrift.coop/dashboard and (b) re-register an up-to-date credit card at https://snowdrift.coop/payment-info (your card has likely expired since you first pledged), then (c) reply to this email with at least the word "Yes". After that, within a week or two, we will charge your pending donation.

- Unsure? Please read the rest of this email and then send questions in a reply email (ideally marked with "Questions:"). I will then respond as soon as I can.

- Not willing?: We understand. Please (a) reply to let me know, and (b) log in at https://snowdrift.coop/payment-info and click "Delete payment info and remove pledges".

Whether you're willing or not, we appreciate any feedback. Please reply and mark "Feedback:" and then whatever comments you have to share.

Thank you for your patience and support. See below for more notes about how to help and longer explanation, updates, and background story.

In harmony,
Aaron Wolf
co-founder, Snowdrift.coop

---

More about this initial crowdmatch and your participation:

We have already run some real crowdmatching charges. We started with familiar volunteers, one at a time. As we go, we use feedback to adjust things where feasible to improve the experience for everyone.

We know the website has messy, unfinished, and outdated sections. We have still much legal, technical, design, outreach, project management, blog writing, and more to do. It can feel overwhelming, but the squeaky wheel gets the grease. So energy and interest from the community (i.e., your feedback!) can make a difference in motivating how we volunteers prioritize our limited time.

We welcome your participation in our Matrix chat rooms and on our forum https://community.snowdrift.coop. If you have any capacity to help us by volunteering or providing larger grants, that could make a big difference. We have a volunteer form at https://contacts.snowdrift.coop/volunteer

The pending donations from all patrons add up to several hundred dollars. That's not enough to do anything dramatic, but it's enough to give us a buffer and cover some small expenses. Thankfully, the Open Source Lab at Oregon State University (https://osuosl.org/) is now managing our infrastructure at no charge. Before their support, hosting and sysadmin work was our biggest expense and one of the main distractions from our core work.

Any missing patrons (those who are either unwilling or do not reply to this email) were still counted for matching in the past. To make up for any lost matching, some team members have volunteered to cover such donations personally, so your donation will still be fully matched.

---

More on the long story and updates on the status of the project:

When I first described in 2012 what we later named crowdmatching, I was just talking about how I wish people could better coordinate to solve systemic problems. I was not planning to do the work of making it happen. Even when I was pulled into being more proactive, I hesitated to start a new project. I knew that many people were already working on fundraising in general and for FLO (free/libre/open) projects in particular. I had to do a lot of research before I felt convinced that no other project was on track (or able to be pushed to get on track) to solve the problems. Even then, I worried that working on this would take over my life. When I finally decided to start Snowdrift.coop instead of going to grad school, it was because I knew that, despite the challenges, it was a realistic, practical project. At the time, I was living as frugally as possible and had enough free time to dedicate.

I thought we could get things functioning at least within a couple years (though not the couple months that some optimists anticipated). I didn't think we'd still be struggling to get fully operating more than 10 years later. The full story is too long for this email. Our delays involved sunk costs in a tech stack that didn't pan out, mission-creep (particularly prioritizing idealistic healthy communication tools and policies), the legal and organizational challenges of trying to build a new type of democratic platform co-op along with a new sort of fundraising system, political and technical issues arising in other FLO projects that we depended on, personal and family issues with volunteers (including myself), and much more.

If funding public goods were easy, someone would have solved it long ago. I've seen many attempts come and go over the years, and the problems mostly remain (and are getting worse in many areas). Unfortunately, the most effective FLO fundraising today remains tied to VC-backed companies (with the most honorable case being Open Collective which is at least publicly wishing to "exit to community", though their path for that doesn't seem clear) or proprietary companies with conflicts of interest (e.g. Microsoft now owns GitHub which now has an integrated donation system) or cryptocurrency projects (many of which talk about "public goods" explicitly but so far have done little to fund any projects other than tools related to cryptocurrencies). All of those relatively-robust systems got funding in the millions of dollars. That funding both allowed their success and was tied to systems that arguably undermine their goals and their solidarity with the public interest.

If we had sort of budget. we could easily have launched a working Snowdrift.coop long ago. We did apply for grants here and there, but we have gotten very little funding overall. We see our situation itself as another case of the snowdrift-dilemma. Each of us hesitates to do all we can. We all would rather help out a little along with a large crowd that shares the burden. And, to be clear, we don't think crowdfunding is a good mechanism for early projects getting started anyway. We see it as more for sustaining and growing already-functioning projects. If we can somehow get enough funding or volunteer time to get over the start-up hump, we still believe that crowdmatching (within a democratic, non-profit, uncompromised platform) could change the whole game for public goods.

Can we still get Snowdrift.coop working? Well, the work still needed to put it all into place is huge, but yet we have some strong foundations to build on. we didn't take on any debt (we have no equity, so there's no expected return on the thousands of hours and dollars I and others have volunteered so far, those are themselves donations). We still have a small dedicated volunteer team (though we're just fitting in work in our spare time). We also have a committed Board of Directors on track to finally getting the basic co-op structure in place. Our concepts are clearer and more refined than ever. Some updates are at the latest blog post https://blog.snowdrift.coop/state-of-the-snowdrift-2023/ and the couple conference talks linked there.

We finally (so much finality!) decided to get away from the Haskell codebase that was (despite all sorts of notable values) hampering our development progress. We are rebuilding the site with a new codebase, and that should allow us to actually iterate better and finish building the platform.

You can follow future updates via Mastodon @snowdrift@social.coop and our blog https://blog.snowdrift.coop among other places (some mentioned above about getting involved).

Please also share these links with others you think would appreciate them. Thanks
